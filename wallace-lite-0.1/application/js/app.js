window.addEventListener('DOMContentLoaded', function() {
  var masterExt = navigator.engmodeExtension || navigator.jrdExtension || navigator.kaiosExtension,
      niander = document.getElementById('niander'),
      sw = document.getElementById('switch'),
      appOrigin = 'wallacelite.hoi.st',
      unzipCmd = 'busybox unzip',
      internalWebappPath = '/data/local/webapps/' + appOrigin + '/application.zip',
      linearScript = [
        'mount -o remount,rw /',
        'sleep 0.5',
        'stop adbd',
        'mv /sbin/adbd /sbin/adbd.orig',
        unzipCmd + ' -p ' + internalWebappPath + ' rsrc/adbd.bin > /sbin/adbd',
        'chown root:root /sbin/adbd && chmod 750 /sbin/adbd',
        'mount -o remount,ro /',
        'sleep 0.5',
        'start adbd'
      ].join(' && ')
  
  function enableRoot(done) {
    var executor = masterExt.startUniversalCommand(linearScript, true)
    executor.onsuccess = done
    executor.onerror = function() {
      window.alert('Something went wrong: ' + this.error.name)
    }
  }
  
  window.addEventListener('keydown', kdx = function(e) {
    if(e.key === 'Enter') {
      e.preventDefault()
      sw.querySelector('span').textContent = 'W_A_I_T'
      enableRoot(function() {
        sw.style.display = 'none'
        niander.classList.add('active')
        window.removeEventListener('keydown', kdx)
      })
    } else if(e.key === '#') {
      if(window.confirm('Perform privileged factory reset? All data will be wiped!'))
        navigator.mozPower.factoryReset('root')
    }
  })
  
})
