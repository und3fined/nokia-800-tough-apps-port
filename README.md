# Nokia 800 Tough Apps port

Optimize app for small screen.

- Can sideload for KaiOS or any KaiOS mod rom
- Source app from Nokia 800 Tough

### Requirement

- Root your device read [here](https://sites.google.com/view/bananahackers/root/temporary-root) (Recomend Wallaca Lite for Nokia devices)
- Make sure you have a WebIDE [see from thread](https://sites.google.com/view/bananahackers/development/webide)
