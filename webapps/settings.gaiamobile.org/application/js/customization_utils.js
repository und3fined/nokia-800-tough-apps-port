/* -*- Mode: js; js-indent-level: 2; indent-tabs-mode: nil -*- */
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */
/* global SettingsListener, SettingsSoftkey, Toaster,
   SupportedNetworkTypeHelper, LazyLoader, DsdsSettings */


const SHOW = 0;
const HIDE = 1;
const GRAYOUT = 2;

const CUSTOMIZATION_VALUES = [SHOW, HIDE, GRAYOUT];

const KEY_HREF_MAP = {
  'wifi.settings.ui': '#wifi',
  'bluetooth.settings.ui' : '#bluetooth',
  'geolocation.settings.ui':'#geolocation'
};

const KEY_LI_MAP = {
  'wifi.settings.ui' : 'connectivity-wifi',
  'bluetooth.settings.ui' : 'connectivity-bluetooth',
  'geolocation.settings.ui' : 'geolocation-settings',
  'data.settings.ui': 'liItem-dataConnection',
  'data.roaming.settings.ui': 'liItem-dataRoaming',
  'screen.timeout.settings.ui': 'screen-timeout',
  'pocketmode.autolock.settings.ui': 'auto-lock',
  'debug.performance_data.settings.ui': 'liItem-dataShared',
  'callforward.settings.ui': 'menuItem-callForwarding'
};

const KEY_DEVICE_SUPPORT = {
  'wifi.settings.ui' : 'wifi',
  'bluetooth.settings.ui' : 'bt',
  'geolocation.settings.ui' : 'gps'
};
const mozSettings = window.navigator.mozSettings;

function updateSettingsUI(key, value) {
  var element = document.getElementById(KEY_LI_MAP[key]);
  if (!element) {
    return;
  }

  var itemKey = KEY_DEVICE_SUPPORT[key];
  if (itemKey && (DeviceFeature.getValue(itemKey) !== 'true')) {
    return;
  }

  if (value === HIDE /*hide*/) {
    element.hidden = true;
    element.classList.remove('focus');
  } else if (value === GRAYOUT /*'grayout'*/) {
    element.hidden = false;
    element.classList.add('none-select');
    element.setAttribute('aria-disabled', true);
    if (KEY_HREF_MAP[key]) {
      var hrefItem = element.querySelector('a');
      hrefItem.removeAttribute('href');
    }
    if (element.querySelector('input')) {
      element.querySelector('input').disabled = true;
    }
  } else if (value === SHOW /*show*/ ) {
    element.hidden = false;
    element.classList.remove('none-select');
    element.removeAttribute('aria-disabled');
    if (KEY_HREF_MAP[key]) {
      var hrefItem = element.querySelector('a');
      hrefItem.setAttribute('href', KEY_HREF_MAP[key]);
    } else if (key === 'callforward.settings.ui') {
      var hrefItem = element.querySelector('a');
      /* << [CNT-924] BDC Matf disable the video call forward setting beacuse CNT not support VT call */
      /*if (DeviceFeature.getValue('vilte') === 'true') {
        hrefItem.setAttribute('href', '#call-cfsettings-list');
      } else {
        hrefItem.setAttribute('href', '#call-cfSettings');
      }*/
       hrefItem.setAttribute('href', '#call-cfSettings');
       /* >> [CNT-924]BDC Matf disable the video call forward setting beacuse CNT not support VT call */
      
    }
    if (element.querySelector('input')) {
      element.querySelector('input').disabled = false;
    }
  }
}

const ROOT_SETTINGS_UI_LIST = [
  'wifi.settings.ui',
  'bluetooth.settings.ui',
  'geolocation.settings.ui',
  'wifi-hotspot.settings.ui',
  'tethering.usb.settings.ui'
];

function updateForSettings(evt) {
  var key = evt.settingName;
  var value = evt.settingValue;
  if (key === 'wifi-hotspot.settings.ui' ||
    key === 'tethering.usb.settings.ui') {
    _updateHotspotDisplay();
  } else {
    updateSettingsUI(key, value);
  }
  window.dispatchEvent(new CustomEvent('refresh'));
}

function getSetting(settingKey) {
  return new Promise(function (resolve, reject) {
    var transaction = mozSettings.createLock();
    var req = transaction.get(settingKey);
    req.onsuccess = function () {
      resolve(req.result[settingKey]);
    };
    req.onerror = function () {
      resolve(false);
    };
  });
}

function initUIBySettings(list) {
  var promiseList = [];
  list.forEach(function (key) {
    promiseList.push(getSetting(key));
  });
  Promise.all(promiseList).then(function (values) {
    DeviceFeature.ready(() => {
      for (var i = 0; i < values.length; i++) {
      updateSettingsUI(list[i], values[i]);
      }
      window.dispatchEvent(new CustomEvent('refresh'));
    });
  });
}

function addListenerForCustomization(list) {
  list.forEach(function (key) {
    mozSettings.addObserver(key, updateForSettings);
  });
}

function removeListenerForCustomization(list) {
  list.forEach(function (key) {
    mozSettings.removeObserver(key, updateForSettings);
  })
}

function _updateHotspotDisplay() {
  let p1 = getSetting('wifi-hotspot.settings.ui');
  let p2 = getSetting('tethering.usb.settings.ui');
  let p3 = getSetting('ril.data.enabled');
  let p4 = getSetting('tethering.support');
  Promise.all([p1, p2, p3, p4]).then((values) => {
    let isSupport = values[3];
    if (!isSupport) {
      return;
    }

    DeviceFeature.ready(() => {
      let isSupportWifi = DeviceFeature.getValue('wifi');
      let hotspotItem = document.getElementById('internet-sharing');
      if (isSupportWifi === 'true' && values[0] === HIDE && values[1] === HIDE ||
        isSupportWifi !== 'true' && values[1] === HIDE) {
        hotspotItem.hidden = true;
      } else {
        hotspotItem.hidden = false;
        if (values[2]) {
          hotspotItem.classList.remove('none-select');
          hotspotItem.removeAttribute('aria-disabled');

          let hrefItem = hotspotItem.querySelector('a');
          hrefItem.setAttribute('href', '#hotspot');
        } else {
          hotspotItem.setAttribute('aria-disabled', true);
          hotspotItem.classList.add('none-select');

          let hrefItem = hotspotItem.querySelector('a');
          hrefItem.removeAttribute('href');
        }
      }
      window.dispatchEvent(new CustomEvent('refresh'));
    });
  });
}

