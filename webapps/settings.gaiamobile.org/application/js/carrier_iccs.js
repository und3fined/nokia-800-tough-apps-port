/* -*- Mode: js; js-indent-level: 2; indent-tabs-mode: nil -*- */
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */



/**
 * Singleton object that helps to populate and manage the 'Select a SIM card'
 * panel in the cell and data settings panel.
 */
var IccHandlerForCarrierSettings = (function(window, document, undefined) {
  var _settings = window.navigator.mozSettings;
  var _iccManager = window.navigator.mozIccManager;
  var _mobileConnections = null;


  /** Array of menu item ids. */
  var _menuItemIds = ['menuItem-carrier-sim1', 'menuItem-carrier-sim2'];
  var _apnItemIds = ['menuItem-apn-sim1', 'menuItem-apn-sim2'];

  /** Array of menu item descriptions. */
  var _menuItemDescriptions = [];

  /**
   * Init function.
   */
  function ihfcs_init() {
    _mobileConnections = window.navigator.mozMobileConnections;
    if (!_settings || !_mobileConnections || !_iccManager) {
      return;
    }
    if (DsdsSettings.getNumberOfIccSlots() === 1) {
      return;
    }

    function addListeners(iccId) {
      var eventHandler = ihfcs_showICCCardDetails.bind(null, iccId);
      var iccCard = _iccManager.getIccById(iccId);
      if (!iccCard) {
        return;
      }
      iccCard.addEventListener('cardstatechange', eventHandler);
      var mobileConnection = ihfcs_getMobileConnectionFromIccId(iccId);
      if (!mobileConnection) {
        return;
      }
      mobileConnection.addEventListener('datachange', eventHandler);
      mobileConnection.addEventListener('radiostatechange', eventHandler);
    }

    _menuItemIds.forEach(function forEachFunction(id) {
      var selector = document.getElementById(id);
      _menuItemDescriptions[_menuItemIds.indexOf(id)] =
        selector.querySelector('small');
    });

    _menuItemIds.forEach(function forEachFunction(id) {
      var selector = document.getElementById(id);
      var element = selector.querySelector('a');
      element.addEventListener('click', function eventListenerHandler() {
        DsdsSettings.setIccCardIndexForCellAndDataSettings(
          _menuItemIds.indexOf(id)
        );
      });
    });

    _apnItemIds.forEach(function forEachFunction(id) {
      var selector = document.getElementById(id);
      var element = selector.querySelector('a');
      element.addEventListener('click', function eventListenerHandler() {
        DsdsSettings.setIccCardIndexForCellAndDataSettings(
          _apnItemIds.indexOf(id)
        );
      });
    });

    var numberOfIccCards = _mobileConnections.length;
    for (var i = 0; i < numberOfIccCards; i++) {
      var mobileConnection = _mobileConnections[i];
      if (!mobileConnection.iccId) {
        // TODO: this could mean there is no ICC card or the ICC card is
        // locked. If locked we would need to figure out how to check the
        // current card state.
        ihfcs_disableItems(_menuItemIds[i], true);
        ihfcs_disableItems(_apnItemIds[i], true);
      }
      ihfcs_showICCCardDetails(mobileConnection.iccId);
      addListeners(mobileConnection.iccId);
    }

    _iccManager.addEventListener('iccdetected',
      function iccDetectedHandler(evt) {
        var iccId = evt.iccId;
        ihfcs_showICCCardDetails(iccId);
        addListeners(iccId);
    });

    _iccManager.addEventListener('iccundetected',
      function iccUndetectedHandler(evt) {
        var iccId = evt.iccId;
        var eventHandler = ihfcs_showICCCardDetails.bind(null, iccId);
        var mobileConnection = ihfcs_getMobileConnectionFromIccId(iccId);
        if (!mobileConnection) {
          return;
        }
        mobileConnection.removeEventListener('datachange', eventHandler);
        mobileConnection.removeEventListener('radiostatechange', eventHandler);
    });
  }

  /**
   * Show some details (card state or carrier) of the ICC card.
   *
   * @param {String} iccId ICC id.
   */
  function ihfcs_showICCCardDetails(iccId) {
    var iccCardIndex = ihfcs_getIccCardIndex(iccId);
    var desc = _menuItemDescriptions[iccCardIndex];

    desc.style.fontStyle = 'italic';

    var mobileConnection = ihfcs_getMobileConnectionFromIccId(iccId);
    if (!mobileConnection) {
      desc.textContent = '';
      desc.removeAttribute('data-l10n-id');
      ihfcs_disableItems(_menuItemIds[iccCardIndex], true);
      ihfcs_disableItems(_apnItemIds[iccCardIndex], true);
      return;
    }

    if (mobileConnection.radioState !== 'enabled') {
      // Airplane is enabled. Well, radioState property could be changing but
      // let's disable the items during the transitions also.
      desc.setAttribute('data-l10n-id', _getCardDesription('null'));
      ihfcs_disableItems(_menuItemIds[iccCardIndex], true);
      ihfcs_disableItems(_apnItemIds[iccCardIndex], true);
      return;
    }
    if (mobileConnection.radioState === 'enabled') {
      desc.textContent = '';
      desc.removeAttribute('data-l10n-id');
      ihfcs_disableItems(_menuItemIds[iccCardIndex], false);
      ihfcs_disableItems(_apnItemIds[iccCardIndex], false);
    }

    var iccCard = _iccManager.getIccById(iccId);
    if (!iccCard) {
      desc.textContent = '';
      desc.setAttribute('data-l10n-id', _getCardDesription('absent'));
      ihfcs_disableItems(_menuItemIds[iccCardIndex], true);
      ihfcs_disableItems(_apnItemIds[iccCardIndex], true);
      return;
    }

    var cardState = iccCard.cardState;
    if (cardState !== 'ready') {
      desc.setAttribute('data-l10n-id',
        _getCardDesription(cardState || 'null'));
      ihfcs_disableItems(_menuItemIds[iccCardIndex], true);
      ihfcs_disableItems(_apnItemIds[iccCardIndex], true);
      return;
    }

    desc.style.fontStyle = 'normal';

    _settings.createLock().get('custom.simcards.name').then((result) => {
      var customedSimName = result['custom.simcards.name'];
      if (customedSimName && customedSimName[iccId]) {
        desc.textContent = customedSimName[iccId];
      } else {
        var operatorInfo = MobileOperator.userFacingInfo(mobileConnection);
        if (operatorInfo.operator) {
        desc.textContent = operatorInfo.operator;
        }
      }
    });

    ihfcs_disableItems(_menuItemIds[iccCardIndex], false);
    ihfcs_disableItems(_apnItemIds[iccCardIndex], false);
  }

  /**
   * Disable the items listed in the panel in order to avoid user interaction.
   *
   * @param {String} id Element id from the element to disable.
   * @param {Boolean} disable This flag tells the function what to do.
   */
  function ihfcs_disableItems(id, disable) {
    var element = document.getElementById(id);
    var hrefItem = element.querySelector('a');
    if (disable) {
      element.setAttribute('aria-disabled', true);
      element.classList.add('none-select');
      hrefItem.removeAttribute('href');
      element.disabled = true;
    } else {
      if (id === 'menuItem-carrier-sim1' || id === 'menuItem-carrier-sim2') {
        hrefItem.setAttribute('href', '#carrier-operatorSettings');
      } else {
        hrefItem.setAttribute('href', '#apn-settings');
      }
      element.removeAttribute('aria-disabled');
      element.classList.remove('none-select');
      element.disabled = false;
    }
  }

  /**
   * Helper function. Return the index of the ICC card given the ICC code in the
   * ICC card.
   *
   * @param {String} iccId The iccId code form the ICC card.
   *
   * @return {Numeric} The index.
   */
  function ihfcs_getIccCardIndex(iccId) {
    for (var i = 0; i < _mobileConnections.length; i++) {
      if (_mobileConnections[i].iccId === iccId) {
        return i;
      }
    }
    return -1;
  }

  /**
   * Helper function. Return the mozMobileConnection for the ICC card given the
   * ICC code in the ICC card.
   *
   * @param {String} iccId The iccId code form the ICC card.
   *
   * @return {mozMobileConnection} mozMobileConnection object.
   */
  function ihfcs_getMobileConnectionFromIccId(iccId) {
    for (var i = 0; i < _mobileConnections.length; i++) {
      if (_mobileConnections[i].iccId === iccId) {
        return _mobileConnections[i];
      }
    }
    return null;
  }

  return {
    init: ihfcs_init
  };
})(this, document);
