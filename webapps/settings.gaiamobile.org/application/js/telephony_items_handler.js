/* -*- Mode: js; js-indent-level: 2; indent-tabs-mode: nil -*- */
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */


/**
 * Singleton object that helps to enable/disable and to show card state
 * information for telephony-related items from the root in the setting app.
 */
var TelephonyItemsHandler = (function(window, document, undefined) {
  var AirplaneModeHelper = require('shared/airplane_mode_helper');
  var DATA_TYPE_SETTING = 'operatorResources.data.icon';

  var dataTypeMapping = {
    'lte' : '4G LTE',
    'ehrpd': '4G CDMA',
    'hspa+': '3.5G HSPA+',
    'hsdpa': '3.5G HSDPA',
    'hsupa': '3.5G HSDPA',
    'hspa' : '3.5G HSDPA',
    'evdo0': '3G CDMA',
    'evdoa': '3G CDMA',
    'evdob': '3G CDMA',
    '1xrtt': '2G CDMA',
    'umts' : '3G UMTS',
    'edge' : '2G EDGE',
    'is95a': '2G CDMA',
    'is95b': '2G CDMA',
    'gprs' : '2G GPRS'
  };

  var CARD_STATE_MAPPING = {
    'pinRequired' : 'simCardLockedMsg',
    'pukRequired' : 'simCardLockedMsg',
    'permanentBlocked': 'simCardBlockedMsg',
    'networkLocked' : 'simLockedPhone',
    'serviceProviderLocked' : 'simLockedPhone',
    'corporateLocked' : 'simLockedPhone',
    'network1Locked' : 'simLockedPhone',
    'network2Locked' : 'simLockedPhone',
    'hrpdNetworkLocked' : 'simLockedPhone',
    'ruimCorporateLocked' : 'simLockedPhone',
    'ruimServiceProviderLocked' : 'simLockedPhone',
    'unknown' : 'unknownSimCardState',
    'illegal' : 'simCardIllegal',
    'absent' : 'noSimCard',
    'null' : 'simCardNotReady',
    'ready': ''
  };

  var HREF_MAPPING = {
    'call-settings': '#call',
    'simCardManager-settings': '#sim-manager',
    'data-connectivity': '#carrier',
    'volte-settings': '#volte-vowifi',
    'internet-sharing': '#hotspot',
    'cell-broadcast-entry': '#cell-broadcast-message',
    'simSecurity-settings': '#simpin'
  };

  function tih_updateDataTypeMapping() {
    var req;
    try {
      req = navigator.mozSettings.createLock().get(DATA_TYPE_SETTING) || {};
      req.onsuccess = function() {
        var dataTypeValues = req.result[DATA_TYPE_SETTING] || {};
        for (var key in dataTypeValues) {
          if (dataTypeMapping[key]) {
            dataTypeMapping[key] = dataTypeValues[key];
          }
        }
      };
      req.onerror = function() {
        console.error('Error loading ' + DATA_TYPE_SETTING + ' settings. ' +
                      req.error && req.error.name);
      };
    } catch (e) {
      console.error('Error loading ' + DATA_TYPE_SETTING + ' settings. ' + e);
    }
  };

  var _iccManager;
  var _mobileConnections;
  var _;
  var dataEnabled;

  /**
   * Init function.
   */
  function tih_init() {
    _iccManager = window.navigator.mozIccManager;
    _mobileConnections = window.navigator.mozMobileConnections;
    if (!_mobileConnections || !_iccManager) {
      return;
    }
    SettingsListener.observe('ril.data.enabled', false, function (enabled) {
      dataEnabled = enabled;
      tih_checkDataState();
    });
  }

  /**
   * Enable or disable the items according the state of the ICC card and the
   * airplane mode status. It also show a short description about the ICC card
   * status and the carrier name and the data connection type.
   */
  function tih_handleItems() {
  /*<<[BTS-2175] Add KaiOS patch 65182*/
    /*var validCardCount = 0;
    var _currentCapability = null;
    var _imsHandler;
    var iccId = null;*/
    
    let validCardCount = 0;
    let _currentCapability = null;
    let _imsHandler;
    let iccId = null;
    let itemIds = [];
    /*>>[BTS-2175] Add KaiOS patch 65182*/
    for (var i = 0; i < _mobileConnections.length; i++) {
      _imsHandler = _mobileConnections[i].imsHandler;
      if (_imsHandler) {
        _currentCapability = _imsHandler.capability || _currentCapability;
      }
    }
   /*<<[BTS-2175] Add KaiOS patch 65182*/
    if (_mobileConnections) {
      [].forEach.call(_mobileConnections, (simcard, cardIndex) => {
        iccId = simcard.iccId;
        var icc = _iccManager.getIccById(iccId);
        if (icc !== null) {
          validCardCount++;
        }
      });
    }

    let enabled = (AirplaneModeHelper.getStatus() !== 'disabled' || false);
    /*>>[BTS-2175] Add KaiOS patch 65182*/
    if (_currentCapability === 'voice-over-wifi' ||
        _currentCapability === 'video-over-wifi') {
       itemIds = [
        'data-connectivity',
        'simCardManager-settings',
        'simSecurity-settings'
      ];
      /*<<[BTS-2175] Add KaiOS patch 65182*/
        let setItemHref = [
        'call-settings',
        'cell-broadcast-entry'
      ];
      tih_disableItems(!enabled, setItemHref);
      /*>>[BTS-2175] Add KaiOS patch 65182*/
    } else {
       itemIds = [
        'call-settings',
        'data-connectivity',
        'simCardManager-settings',
        'cell-broadcast-entry',
        'simSecurity-settings'
      ];
    }
/*<<[BTS-2175] Add KaiOS patch 65182*/
/*
    if (_mobileConnections) {
      [].forEach.call(_mobileConnections, (simcard, cardIndex) => {
        iccId = simcard.iccId;
        var icc = _iccManager.getIccById(iccId);
        if (icc !== null) {
          validCardCount++;
        }
      });
    }

    var enabled = (AirplaneModeHelper.getStatus() !== 'disabled' || false);
    */
    /*>>[BTS-2175] Add KaiOS patch 65182*/
    tih_disableItems(enabled, itemIds);

    if (enabled) {
      return;
    }

    if (_mobileConnections) {
      var disabled = (validCardCount === 0 || false);
      if (_mobileConnections.length === 1) {
        // Single ICC card device.
        // Below as features can't be available while there is no SIM card.
        itemIds = [
          'call-settings',
          'data-connectivity',
          'volte-settings',
          'cell-broadcast-entry'
        ];

        tih_disableItems(disabled, itemIds);
        if (disabled) {
          // There is no ICC card.
          tih_showICCCardDetails(CARD_STATE_MAPPING['absent']);
          return;
        }

        var iccCard = _iccManager.getIccById(_mobileConnections[0].iccId);
        var cardState = iccCard.cardState;
        var isReady = (cardState === 'ready' || false);
        tih_disableItems(!isReady, itemIds);
        if (isReady) {
          tih_showICCCardDetails('');
        } else {
          tih_showICCCardDetails(CARD_STATE_MAPPING[cardState]);
        }
      } else {
        // Multi ICC card device.
        itemIds = [
          'simCardManager-settings',
          'call-settings',
          'data-connectivity',
          'volte-settings',
          'cell-broadcast-entry'
        ];
        // Enable/disable SIM manager menu item
        tih_disableItems(disabled, itemIds);

        itemIds = [
          'call-settings',
          'cell-broadcast-entry'
        ];

        if (disabled) {
          // There is no ICC cards.
          tih_showICCCardDetails(CARD_STATE_MAPPING['absent']);
        } else {
          // There is ICC card.
          tih_showICCCardDetails('');
          tih_removeItemHref(itemIds);
        }
      }
    } else {
      // hide telephony panels
      var elements = ['call-settings',
                      'data-connectivity',
                      'simSecurity-settings',
                      'simCardManager-settings',
                      'volte-settings',
                      'cell-broadcast-entry'
      ];
      elements.forEach((id) => {
        document.getElementById(id).hidden = true;
      });
    }
  }


  function tih_checkDataState() {
    var validCard = 0;
    if (_mobileConnections) {
      [].forEach.call(_mobileConnections, (simcard, cardIndex) => {
        var iccId = simcard.iccId;
        var icc = _iccManager.getIccById(iccId);
        if (icc !== null) {
          validCard++;
        }
      });
      var disabled = (validCard === 0 || false);
      var itemIds = [
        'internet-sharing'
      ];
      tih_disableItems(!dataEnabled || disabled , itemIds);
    }

  }

  /**
   * Show some details (card state) of the ICC card.
   *
   * @param {String} details What to show as ICC card details.
   */
  function tih_showICCCardDetails(details) {
    var itemIds = [
      'call-desc',
      'internetSharing-desc',
      'cell-broadcast-desc'
    ];

    for (var id = 0; id < itemIds.length; id++) {
      var desc = document.getElementById(itemIds[id]);
      if (!desc) {
        continue;
      }
      desc.style.fontStyle = 'italic';

      if (details !== '') {
        desc.setAttribute('data-l10n-id', details);
      } else {
        desc.removeAttribute('data-l10n-id');
        desc.textContent = '';
      }
    }
  }

  /**
   * Disable or enable a set of menu items.
   *
   * @param {Boolean} disable Flag about what to do.
   * @param {Array} itemIds Menu items id to enable/disable.
   */
  function tih_disableItems(disabled, itemIds) {
    for (var id = 0; id < itemIds.length; id++) {
      var item = document.getElementById(itemIds[id]);
      var hrefItem = item.querySelector('a');
      if (!item) {
        continue;
      }
      if (disabled) {
        hrefItem.removeAttribute('href');
        item.setAttribute('aria-disabled', true);
        item.classList.add('none-select');
      } else {
        item.removeAttribute('aria-disabled');
        item.classList.remove('none-select');
        hrefItem.setAttribute('href', HREF_MAPPING[itemIds[id]]);
      }
    }
  }

  /**
    * Remove the href link to show dual card action menu
    */
  function tih_removeItemHref(itemIds) {
    itemIds.forEach((id) => {
      var item = document.getElementById(id);
      item.removeAttribute('aria-disabled');
      item.classList.remove('none-select');
      var hrefItem = item.querySelector('a');
      hrefItem.removeAttribute('href');
    });
  }

  // Public API.
  return {
    init: tih_init,
    handleItems: function() {
      AirplaneModeHelper.ready(function() {
        tih_handleItems();
      });
    }
  };
})(this, document);
