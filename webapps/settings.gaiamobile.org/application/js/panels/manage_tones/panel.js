define(['require','modules/settings_panel'],function(require) {
  
  var SettingsPanel = require('modules/settings_panel');
  return function ctor_manage_tones_panel() {
    function handleKeyDown(e) {
      switch (e.key) {
        case 'Enter':
          var focusedElement = document.querySelector('#manageTones .focus');
          if (focusedElement.id === 'system-ringtones' ||
            focusedElement.id === 'system-alerts' ||
            focusedElement.id === 'my-ringtones') {
            var toneType = focusedElement.children[0].getAttribute('data-type');
            var activity = new MozActivity({
              name: 'configure',
              data: {
                target: 'ringtone',
                toneType: toneType
              }
            });
          }
          break;
      }
    }

    return SettingsPanel({
      onInit: function(panel) {
        this.params = {
          menuClassName: 'menu-button',
          header: {
            l10nId: 'message'
          },
          items: [{
            name: 'Select',
            l10nId: 'select',
            priority: 2,
            method: function() {}
          }]
        };
        SettingsSoftkey.init(this.params);

        DeviceFeature.ready(() => {
          if (DeviceFeature.getValue('lowMemory') === 'true') {
            let el = document.getElementById('my-ringtones');
            el && el.classList.add('hidden');
            // el && el.parentNode.removeChild(el);
          }
        });
      },
      onBeforeShow: function mt_onBeforeShow() {
        SettingsSoftkey.show();
        window.addEventListener('keydown', handleKeyDown);
      },
      onBeforeHide: function mt_onBeforeHide() {
        SettingsSoftkey.hide();
        window.removeEventListener('keydown', handleKeyDown);
      }
    });
  };
});
