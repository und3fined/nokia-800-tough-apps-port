define(['require','modules/settings_panel','modules/settings_service'],function(require){
  

  var SettingsPanel=require('modules/settings_panel');
  var SettingsService=require('modules/settings_service');
  return function ctor_soscall(){
    var elements,soscallEnabled,sosmessageOn;

    function _saveSoscall(enabled){
      if(soscallEnabled==enabled){
        return;
      }
      soscallEnabled=enabled;
      var settings = {};
      settings['iceinformation.soscallon']=soscallEnabled;
      navigator.mozSettings.createLock().set(settings);
      if(soscallEnabled==true){
        elements.sosMessage.hidden=false;
      }else{
        elements.sosMessage.hidden=true;
        SettingsService.navigate('accessibilitymode');
      }
      var evt=new CustomEvent('panelready',{
        detail:{
          current:Settings.currentPanel,
          // needFocused: needFocused
        }
      });
      window.dispatchEvent(evt);
      showToast('changessaved');
    }

    return SettingsPanel({
      onInit:function(panel){
        elements={
          panel:panel,
          soscallBtn:[].slice.apply(panel.querySelectorAll('input[name="soscall-enabled"]')),
          sosMessage:panel.querySelector('.sosmessage'),
          sosmessageItem:panel.querySelector('#sosmessage-desc')
        };
      },
      onBeforeShow:function(){
        let _settingLock = navigator.mozSettings.createLock();
        let p0 = _settingLock.get('iceinformation.soscallon');
        let p1 = _settingLock.get('iceinformation.sosmessageon');

        Promise.all([p0, p1]).then((values) => {
          soscallEnabled = values[0]['iceinformation.soscallon'];
          if(soscallEnabled==true){
            elements.soscallBtn[0].checked=true;
            elements.sosMessage.hidden=false;
          }else{
            elements.soscallBtn[1].checked=true;
            elements.sosMessage.hidden=true;
          }
          sosmessageOn = values[1]['iceinformation.sosmessageon'];
          var value='off';
          if(sosmessageOn==true){
            value='on';
          }
          elements.sosmessageItem.setAttribute('data-l10n-id',value);
          elements.sosmessageItem.textContent=value;
          // let sosCall be ready to be focused
          var evt=new CustomEvent('panelready',{
            detail:{
              current:Settings.currentPanel,
              // needFocused: needFocused
            }
          });
          window.dispatchEvent(evt);
        });
        this._initSoftKey();
      },
      onShow:function(){
      },
      onBeforeHide:function(){
        SettingsSoftkey.hide();
      },


      _initSoftKey:function(){
        var cancel={
          name:'Cancel',
          l10nId:'cancel',
          priority:1,
          method:function(){
            SettingsService.navigate('accessibilitymode');
          }
        };

        var select={
          name:'Select',
          l10nId:'select',
          priority:2,
          method:function(){
            if(Settings.currentPanel==='#soscall'){
              if(elements.panel.querySelector('li.focus input')){
                if(elements.panel.querySelector('.focus.soscall-enabled')){
                  _saveSoscall(true);}
                else if(elements.panel.querySelector('.focus.soscall-disabled')){
                  _saveSoscall(false);
                }
              }
            }
          }
        };

        var softkeyParams={
          menuClassName:'menu-button',
          header:{
            l10nId:'message'
          },
          items:[]
        };

        softkeyParams.items.push(cancel);
        softkeyParams.items.push(select);
        SettingsSoftkey.init(softkeyParams);
        SettingsSoftkey.show();
      },
    });
  }
});
