
define('panels/accessibility_mode/panel',['require','modules/settings_panel','shared/settings_listener','modules/settings_service'],function(require){
  
  var SettingsPanel=require('modules/settings_panel');
  var SettingsListener=require('shared/settings_listener');
  var SettingsService=require('modules/settings_service');
  return function ctor_accessibilitymode(){
    var elements,accessibilitymodeEnabled,soscallOn;

    function _saveAccessibilityMode(enabled){
      if(accessibilitymodeEnabled==enabled){
        return;
      }
      accessibilitymodeEnabled=enabled;
      var settings={};
      settings['accessibilitymode.enabled']=accessibilitymodeEnabled;
      if(accessibilitymodeEnabled==true){
        elements.sosCall.hidden=false;
        elements.soscallItem.setAttribute('data-l10n-id','on');
        elements.soscallItem.textContent='on';
        elements.accessibilitymodeBtn[0].checked=true;
        elements.accessibilitymodeBtn[0].value=true;
        settings['audio.volume.alarm']=15;
        settings['audio.volume.notification']=15;
        //settings['audio.volume.content']=15; 
        settings['accessibility.large_text']=true;
        settings['phone.ring.keypad']=true;
        settings['iceinformation.soscallon']=true;
        settings['iceinformation.sosmessageon']=false;
        settings['iceinformation.sosposition']=false;
        navigator.mozSettings.createLock().set(settings);
      }else{
        elements.accessibilitymodeBtn[1].checked=true;
        elements.accessibilitymodeBtn[1].value=true;
        settings['audio.volume.alarm']=10;//BTS-1787
        settings['audio.volume.notification']=10;//BTS-1787
        //settings['audio.volume.content']=8;
        settings['accessibility.large_text']=false;
        settings['phone.ring.keypad']=false;
        settings['iceinformation.soscallon']=false;
        settings['iceinformation.sosmessageon']=false;
        settings['iceinformation.sosposition']=false;
        navigator.mozSettings.createLock().set(settings);
        elements.sosCall.hidden=true;
        SettingsService.navigate('root');
      }
      var evt=new CustomEvent('panelready',{
        detail:{
          current:Settings.currentPanel,
          // needFocused: needFocused
        }
      });
      window.dispatchEvent(evt);
      showToast('changessaved');
    }

    function _showConfirmDialog(){
      var dialogConfig={
        title:{
          id:'confirmation',
          args:{}
        },
        body:{
          id:elements.panel.querySelector('.focus.accessibilitymode-enabled')?'qtn_sm_dial_attention_high_vol':'qtn_sm_dial_turn_off_mode',
          args:{}
        },
        cancel:{
          name:'Cancel',
          l10nId:'cancel',
          priority:1,
          callback:function(){
            elements.accessibilitymodeBtn[0].checked=(accessibilitymodeEnabled==true);
            elements.accessibilitymodeBtn[1].checked=!(accessibilitymodeEnabled==true);
            dialog.destroy();
          }
        },
        confirm:{
          name:'Yes',
          l10nId:'yes',
          priority:3,
          callback:function(){
            elements.panel.querySelector('.focus.accessibilitymode-enabled')?_saveAccessibilityMode(true):_saveAccessibilityMode(false);
            dialog.destroy();
          }
        },
      };

      // The info is composed by 2 strings in spec
      if (elements.panel.querySelector('.focus.accessibilitymode-enabled')) {
        var msg1 = navigator.mozL10n.get('qtn_sm_dial_attention_high_vol');
        var msg2 = navigator.mozL10n.get('qtn_sm_dial_enter_mode');
        dialogConfig.body = {text: msg1 + '\n' + msg2};
      }

      var dialog=new ConfirmDialogHelper(dialogConfig);
      dialog.show(document.getElementById('app-confirmation-dialog'));
    }

    return SettingsPanel({
      onInit:function(panel){
        elements={
          panel:panel,
          accessibilitymodeBtn:[].slice.apply(panel.querySelectorAll('input[name="accessibilitymode-enabled"]')),
          sosCall:panel.querySelector('.soscall'),
          soscallItem:panel.querySelector('#soscall-desc')
        };
      },
      onBeforeShow:function(){
        let _settingLock = navigator.mozSettings.createLock();
        let p1 = _settingLock.get('accessibilitymode.enabled');
        let p2 = _settingLock.get('iceinformation.soscallon');

        Promise.all([p1, p2]).then((values) => {
          accessibilitymodeEnabled = values[0]['accessibilitymode.enabled'];
          accessibilitymodeEnabled==true?elements.accessibilitymodeBtn[0].checked=true:elements.accessibilitymodeBtn[1].checked=true;

          soscallOn = values[1]['iceinformation.soscallon'];
          if (soscallOn == null) {
            // default to enable soscallOn
            let settings={};
            settings['iceinformation.soscallon']=true;
            navigator.mozSettings.createLock().set(settings);
            soscallOn = true;
          }

          var value='off';
          if(soscallOn==true){
            value='on';
          }
          elements.soscallItem.setAttribute('data-l10n-id',value);
          elements.soscallItem.textContent=value;
          if(accessibilitymodeEnabled==true){
            elements.sosCall.hidden=false;
          }else{
            elements.sosCall.hidden=true;
          }

          // let sosCall be ready to be focused
          var evt=new CustomEvent('panelready',{
            detail:{
              current:Settings.currentPanel,
              // needFocused: needFocused
            }
          });
          window.dispatchEvent(evt);

        });
        this._initSoftKey();
      },
      onShow:function(){
      },
      onBeforeHide:function(){
        SettingsSoftkey.hide();
      },

      _initSoftKey:function(){
        var cancel={
          name:'Cancel',
          l10nId:'cancel',
          priority:1,
          method:function(){
            SettingsService.navigate('root');
          }
        };

        var select={
          name:'Select',
          l10nId:'select',
          priority:2,
          method:function(){
            if(Settings.currentPanel==='#accessibilitymode'){
              if(elements.panel.querySelector('li.focus input')){
                if((elements.panel.querySelector('.focus.accessibilitymode-enabled')&&accessibilitymodeEnabled==true)||
                  (elements.panel.querySelector('.focus.accessibilitymode-disabled')&&accessibilitymodeEnabled==false)
                ){
                  return;
                }
                _showConfirmDialog();
              }
            }
          }
        };

        var softkeyParams={
          menuClassName:'menu-button',
          header:{
            l10nId:'message'
          },
          items:[]
        };

        softkeyParams.items.push(cancel);
        softkeyParams.items.push(select);
        SettingsSoftkey.init(softkeyParams);
        SettingsSoftkey.show();
      },
    });
  }
});
