/* global SettingsSoftkey */
/**
 * Used to show Emergency alert panel
 */
define(['require','modules/settings_panel'],function(require) {
  

  var SettingsPanel = require('modules/settings_panel');

  return function ctor_emergency_alert_panel() {
    var elements = {};
    var settingElements = ["cmas.extreme.enabled", "cmas.severe.enabled",
      "cmas.amber.enabled", "cmas.monthlytest.enabled"];
    var listElements = document.querySelectorAll('#emergency-alert li');
    var softkeyParams = {
      menuClassName: 'menu-button',
      header: {
        l10nId: 'message'
      },
      items: [{
        name: 'Deselect',
        l10nId: 'deselect',
        priority: 2,
        method: function() {}
      }]
    };
    var params = {
      menuClassName: 'menu-button',
      header: {
        l10nId: 'message'
      },
      items: [{
        name: 'Select',
        l10nId: 'select',
        priority: 2
      }]
    };

    function _updateSoftkey() {
      var focusedElement = document.querySelector('#emergency-alert .focus');
      if (focusedElement && focusedElement.classList &&
          focusedElement.classList.contains('none-select')) {
        SettingsSoftkey.hide();
        return;
      }
      if (focusedElement && focusedElement.querySelector('input') &&
          focusedElement.querySelector('input').checked) {
        SettingsSoftkey.init(softkeyParams);
      } else {
        SettingsSoftkey.init(params);
      }
      SettingsSoftkey.show();
    }

    function _initAlertSettingListener() {
      var i = settingElements.length - 1;
      for (i; i >= 0; i--) {
        SettingsListener.observe(settingElements[i], true, _updateSoftkey);
      }
    }

    function _removeAlertSettingListener() {
      var i = settingElements.length - 1;
      for (i; i >= 0; i--) {
        SettingsListener.unobserve(settingElements[i] , _updateSoftkey());
      }
    }

    function _keyDownHandler(evt) {
      switch (evt.key) {
        case 'Enter':
          if ('alert-inbox' === evt.target.id) {
            try {
              new MozActivity({
                name: 'alert_inbox'
              });
            } catch (e) {
              console.error('Failed to create an alert_inbox activity: ' + e);
            }
          } else if ('ringtone-preview' === evt.target.id) {
            try {
              new MozActivity({
                name: 'ringtone_preview'
              });
            } catch (e) {
              console.error('Failed to create an alert_inbox activity: ' + e);
            }
          }
          break;
        default:
          break;
      }
    }

     // Task5758730-chengyanzhang@t2mobile.com-for add exercise alert message-begin
    function _disableSomeMenu() {// bug4000-chengyanzhang@t2mobile.com-modify
      navigator.customization.getValue("def.operator.name").then((operator) => {
        console.log('Emergency-alert customization operator: ' + operator);
        if (operator === undefined) {
          return;
        }
        //For test
        //operator='NL';
        
        if (operator !== 'IL' && operator !== 'UAE') {
          var exerciseAlertMenu = document.getElementById('exercise-alert-menu');
          // exerciseAlertMenu.setAttribute('hidden', true);
          exerciseAlertMenu.classList.add('hidden');
          exerciseAlertMenu.classList.remove('navigable');
        }
        // bug4018-wen.jun.jiang@fih-foxconn.com-begin
        if (operator == 'UAE') {
          console.log('Emergency-alert customization operator is UAE');
          //var AlertNotificationMenu = document.getElementById('alert-notification');
          //AlertNotificationMenu.classList.add('hidden');
          //AlertNotificationMenu.classList.remove('navigable');
          var nationalEmergencyAlertUaeMenu = document.getElementById('national-emergency-alert-uae-menu');
          var emergencyAlertUaeMenu = document.getElementById('emergency-alert-uae-menu');
          var warningAlertUaeMenu = document.getElementById('warning-alert-uae-menu');
          var testAlertUaeMenu = document.getElementById('test-alert-uae-menu');
          var exerciseUaeMenu = document.getElementById('exercise-uae-menu');
          nationalEmergencyAlertUaeMenu.removeAttribute('hidden');
          emergencyAlertUaeMenu.removeAttribute('hidden');
          warningAlertUaeMenu.removeAttribute('hidden');
          testAlertUaeMenu.removeAttribute('hidden');
          //exerciseUaeMenu.removeAttribute('hidden');

          var cmasPresidentialMenu = document.getElementById('cmas-presidential-menu');
          var cmasExtremeMenu = document.getElementById('cmas-extreme-menu');
          var cmasSevereMenu = document.getElementById('cmas-severe-menu');
          var cmasAmberMenu = document.getElementById('cmas-amber-menu');
          var monthlytestMenu = document.getElementById('required-monthly-test-menu');
          var exerciseMenu = document.getElementById('exercise-alert-menu');
          cmasPresidentialMenu.classList.add('hidden');
          cmasExtremeMenu.classList.add('hidden');
          cmasSevereMenu.classList.add('hidden');
          cmasAmberMenu.classList.add('hidden');
          monthlytestMenu.classList.add('hidden');
          exerciseMenu.classList.add('hidden');
        } 

        if (operator == 'IL') {
          console.log('Emergency-alert customization operator is IL');
          var extremeAlertIlMenu = document.getElementById('extreme-alert-il-menu');
          var warningNotificationIlMenu = document.getElementById('warning-notification-il-menu');
          var informationalIlMenu = document.getElementById('informational-il-menu');
          var testAlertIlMenu = document.getElementById('test-alert-il-menu');
          var exerciseIlMenu = document.getElementById('exercise-il-menu');
          var assistanceIlMenu = document.getElementById('assistance-il-menu');
          extremeAlertIlMenu.removeAttribute('hidden');
          warningNotificationIlMenu.removeAttribute('hidden');
          informationalIlMenu.removeAttribute('hidden');
          testAlertIlMenu.removeAttribute('hidden');
          exerciseIlMenu.removeAttribute('hidden');
          assistanceIlMenu.removeAttribute('hidden');

          var cmasPresidentialMenu = document.getElementById('cmas-presidential-menu');
          var cmasExtremeMenu = document.getElementById('cmas-extreme-menu');
          var cmasSevereMenu = document.getElementById('cmas-severe-menu');
          var cmasAmberMenu = document.getElementById('cmas-amber-menu');
          var monthlytestMenu = document.getElementById('required-monthly-test-menu');
          var exerciseMenu = document.getElementById('exercise-alert-menu');
          cmasPresidentialMenu.classList.add('hidden');
          cmasExtremeMenu.classList.add('hidden');
          cmasSevereMenu.classList.add('hidden');
          cmasAmberMenu.classList.add('hidden');
          monthlytestMenu.classList.add('hidden');
          exerciseMenu.classList.add('hidden');
        }
       

        if (operator == 'LT') {
          console.log('Emergency-alert customization operator is LT');
          var nationalEmergencyAlertLtMenu = document.getElementById('national-emergency-alert-lt-menu');
          var emergencyAlertLtMenu = document.getElementById('emergency-alert-lt-menu');
          var warningAlertLtMenu = document.getElementById('warning-alert-lt-menu');
          var testAlertLtMenu = document.getElementById('test-alert-lt-menu');
          nationalEmergencyAlertLtMenu.removeAttribute('hidden');
          emergencyAlertLtMenu.removeAttribute('hidden');
          warningAlertLtMenu.removeAttribute('hidden');
          testAlertLtMenu.removeAttribute('hidden');

          var cmasPresidentialMenu = document.getElementById('cmas-presidential-menu');
          var cmasExtremeMenu = document.getElementById('cmas-extreme-menu');
          var cmasSevereMenu = document.getElementById('cmas-severe-menu');
          var monthlytestMenu = document.getElementById('required-monthly-test-menu');
          var exerciseMenu = document.getElementById('exercise-alert-menu');
          cmasPresidentialMenu.classList.add('hidden');
          cmasExtremeMenu.classList.add('hidden');
          cmasSevereMenu.classList.add('hidden');
          monthlytestMenu.classList.add('hidden');
          exerciseMenu.classList.add('hidden');
        }

        if (operator == 'NL') {
          console.log('Emergency-alert customization operator is NL');
          var alertNlMenu = document.getElementById('alert-nl-menu');
          alertNlMenu.removeAttribute('hidden');
          var cmasPresidentialMenu = document.getElementById('cmas-presidential-menu');
          var cmasExtremeMenu = document.getElementById('cmas-extreme-menu');
          var cmasSevereMenu = document.getElementById('cmas-severe-menu');
          var cmasAmberMenu = document.getElementById('cmas-amber-menu');
          var monthlytestMenu = document.getElementById('required-monthly-test-menu');
          var exerciseMenu = document.getElementById('exercise-alert-menu');
          cmasPresidentialMenu.classList.add('hidden');
          cmasExtremeMenu.classList.add('hidden');
          cmasSevereMenu.classList.add('hidden');
          cmasAmberMenu.classList.add('hidden');
          monthlytestMenu.classList.add('hidden');
          exerciseMenu.classList.add('hidden');
        }


        // bug4018-wen.jun.jiang@fih-foxconn.com-end

        // bug4000-chengyanzhang@t2mobile.com-begin
        // hidden some default menu and display cmas menu for tw
        if (operator === 'TW') {
          var emergencyAlertTWMenu = document.getElementById('emergency-alert-tw-menu');
          var emergencyMessageTWMenu = document.getElementById('emergency-message-tw-menu');
          emergencyAlertTWMenu.removeAttribute('hidden');
          emergencyMessageTWMenu.removeAttribute('hidden');

          var cmasExtremeMenu = document.getElementById('cmas-extreme-menu');
          var cmasSevereMenu = document.getElementById('cmas-severe-menu');
          var cmasAmberMenu = document.getElementById('cmas-amber-menu');
          var ringtonePreviewMenu = document.getElementById('ringtone-preview-menu');
          cmasExtremeMenu.classList.add('hidden');
          cmasSevereMenu.classList.add('hidden');
          cmasAmberMenu.classList.add('hidden');
          ringtonePreviewMenu.classList.add('hidden');
        }
        // bug4000-chengyanzhang@t2mobile.com-end

        // Added by yingsen.zhang@t2mobile.com 20180319 begin
        // Change Romania CMAS settings
        if (operator === 'RO') {
          var presidentialAlertRoMenu = document.getElementById('presidential-alert-ro-menu');
          var extremeRoMenu = document.getElementById('extreme-alert-ro-menu');
          var severeRoMenu = document.getElementById('severe-alert-ro-menu');
          var amberRoMenu = document.getElementById('amber-alert-ro-menu');
          var exerciseRoMenu = document.getElementById('exercise-alert-ro-menu');
          presidentialAlertRoMenu.removeAttribute('hidden');
          extremeRoMenu.removeAttribute('hidden');
          severeRoMenu.removeAttribute('hidden');
          amberRoMenu.removeAttribute('hidden');
          exerciseRoMenu.removeAttribute('hidden');

          //var emergencyAlertTWMenu = document.getElementById('emergency-alert-tw-menu');
          //var emergencyMessageTWMenu = document.getElementById('emergency-message-tw-menu');
          //emergencyAlertTWMenu.classList.add('hidden');
          //emergencyMessageTWMenu.classList.add('hidden');

          //var exerciseAlertMenu = document.getElementById('exercise-alert-menu');
          //var requiredMonthlyTestMenu = document.getElementById('required-monthly-test-menu');
          //exerciseAlertMenu.classList.add('hidden');
          //requiredMonthlyTestMenu.classList.add('hidden');
          var cmasPresidentialMenu = document.getElementById('cmas-presidential-menu');
          var cmasExtremeMenu = document.getElementById('cmas-extreme-menu');
          var cmasSevereMenu = document.getElementById('cmas-severe-menu');
          var cmasAmberMenu = document.getElementById('cmas-amber-menu');
          var monthlytestMenu = document.getElementById('required-monthly-test-menu');
          var exerciseMenu = document.getElementById('exercise-alert-menu');
          cmasPresidentialMenu.classList.add('hidden');
          cmasExtremeMenu.classList.add('hidden');
          cmasSevereMenu.classList.add('hidden');
          cmasAmberMenu.classList.add('hidden');
          monthlytestMenu.classList.add('hidden');
          exerciseMenu.classList.add('hidden');
        }
        // Added by yingsen.zhang@t2mobile.com 20180319 end
        
        // JWJ For Korea --Begin
        if (operator === 'KR') {
          var emergencyAlertTWMenu = document.getElementById('emergency-alert-tw-menu');
          var emergencyMessageTWMenu = document.getElementById('emergency-message-tw-menu');
          emergencyAlertTWMenu.classList.add('hidden');
          emergencyMessageTWMenu.classList.add('hidden');

          var exerciseAlertMenu = document.getElementById('exercise-alert-menu');
          var requiredMonthlyTestMenu = document.getElementById('required-monthly-test-menu');
          exerciseAlertMenu.classList.add('hidden');
          requiredMonthlyTestMenu.classList.add('hidden');
        }
        // JWJ For Korea --End

       });
    }
    // Task5758730-chengyanzhang@t2mobile.com-for add exercise alert message-end

    function _updateAlertBodyDisplay(panel) {
      var alertBody = panel.querySelector('#receive-alert-body');
      var request = navigator.mozSettings.createLock().get('cmas.settings.show');
      request.onsuccess = () => {
        var val = request.result['cmas.settings.show'];
        if (val === 'undefined') {
          val = true;
        }
        if (alertBody.hidden === val) {
          alertBody.hidden = !val;
          NavigationMap.refresh();
        }
      };
      request.onerror = () => {
        console.error('ERROR: Can not get the receive alert setting.');
      };
    }

    return SettingsPanel({
      onInit: function(panel) {
        elements = [
          document.getElementById('alert-inbox'),
          document.getElementById('ringtone-preview')
        ];
        _updateAlertBodyDisplay(panel);
      },

      onBeforeShow: function() {
        _updateSoftkey();
        _disableSomeMenu();// Task5758730-chengyanzhang@t2mobile.com-for add exercise alert message-add
        elements.forEach((ele) => {
          ele.addEventListener('keydown', _keyDownHandler);
        });
        ListFocusHelper.addEventListener(listElements, _updateSoftkey);
        _initAlertSettingListener();
      },

      onBeforeHide: function() {
        ListFocusHelper.removeEventListener(listElements, _updateSoftkey);
        _removeAlertSettingListener();
        SettingsSoftkey.hide();
        elements.forEach((ele) => {
          ele.removeEventListener('keydown', _keyDownHandler);
        })
      }
    });
  };
});
