/**
 * Used to show Personalization/Sound/other-sounds panel
 */
define(['require','modules/settings_panel','shared/settings_listener'],function(require) {
  

  var SettingsPanel = require('modules/settings_panel');
  var SettingsListener = require('shared/settings_listener');

  return function createOtherSoundsPanel () {
    function _initSoftkey() {
      var params = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };

      SettingsSoftkey.init(params);
      SettingsSoftkey.show();
    }

    return SettingsPanel({
      onInit: function(panel) {
        this.otherSoundsElements = {
          dialpad: document.getElementById('dial-pad'),
          camera: document.getElementById('camera'),
          sentMessage: document.getElementById('sent-message')
        };

        SettingsListener.observe('phone.ring.keypad', true, value => {
          this.otherSoundsElements.dialpad.value = value;
        });

        SettingsListener.observe('camera.sound.enabled', true, value => {
          this.otherSoundsElements.camera.value = value;
        });

        SettingsListener.observe('message.sent-sound.enabled', true, value => {
          this.otherSoundsElements.sentMessage.value = value;
        });
      },

      onBeforeShow: function() {
        _initSoftkey();
      },

      onBeforeHide: function () {
        SettingsSoftkey.hide();
      }
    });
  };
});
