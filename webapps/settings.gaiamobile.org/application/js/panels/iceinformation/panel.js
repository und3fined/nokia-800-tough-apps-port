
define('panels/iceinformation/panel',['require','modules/settings_panel','modules/settings_service'],function(require){
  

  var SettingsPanel=require('modules/settings_panel');
  var SettingsService=require('modules/settings_service');

  return function ctor_iceinformation_settings(){
    var elements;

    function _submit(panel){
      dump("JWJ: _submit");
      localStorage.firstname=elements.firstName.value;
      localStorage.lastname=elements.lastName.value;
      localStorage.condition=elements.condition.value;
      localStorage.allergies=elements.allergies.value;
      localStorage.vaccination=elements.vaccination.value;
      localStorage.medication=elements.medication.value;
      localStorage.birthday=elements.birthday.value;
      localStorage.height=elements.height.value;
      localStorage.weight=elements.weight.value;
      localStorage.address=elements.address.value;
      var settings = {};
      settings['iceinformation.user'] = '';
      settings['iceinformation.user'] = (localStorage.firstname != '' && localStorage.firstname != undefined)? (settings['iceinformation.user']+elements.firstNameTab.textContent+localStorage.firstname+"; ") : settings['iceinformation.user'];
      settings['iceinformation.user'] = (localStorage.lastname != '' && localStorage.lastname != undefined)? (settings['iceinformation.user']+elements.lastNameTab.textContent+localStorage.lastname+"; ") : settings['iceinformation.user'];
      settings['iceinformation.user'] = (localStorage.condition != '' && localStorage.condition != undefined)? (settings['iceinformation.user']+elements.conditionTab.textContent+localStorage.condition+"; ") : settings['iceinformation.user'];
      settings['iceinformation.user'] = (localStorage.allergies != '' && localStorage.allergies != undefined)? (settings['iceinformation.user']+elements.allergiesTab.textContent+localStorage.allergies+"; ") : settings['iceinformation.user'];
      settings['iceinformation.user'] = (localStorage.vaccination != '' && localStorage.vaccination != undefined)? (settings['iceinformation.user']+elements.vaccinationTab.textContent+localStorage.vaccination+"; ") : settings['iceinformation.user'];
      settings['iceinformation.user'] = (localStorage.medication != '' && localStorage.medication != undefined)? (settings['iceinformation.user']+elements.medicationTab.textContent+localStorage.medication+"; ") : settings['iceinformation.user'];
      settings['iceinformation.user'] = (localStorage.birthday != '' && localStorage.birthday != undefined)? (settings['iceinformation.user']+elements.birthdayTab.textContent+localStorage.birthday+"; ") : settings['iceinformation.user'];
      settings['iceinformation.user'] = (localStorage.height != '' && localStorage.height != undefined)? (settings['iceinformation.user']+elements.heightTab.textContent+localStorage.height+"; ") : settings['iceinformation.user'];
      settings['iceinformation.user'] = (localStorage.weight != '' && localStorage.weight != undefined)? (settings['iceinformation.user']+elements.weightTab.textContent+localStorage.weight+"; ") : settings['iceinformation.user'];
      settings['iceinformation.user'] = (localStorage.address != '' && localStorage.address != undefined)? (settings['iceinformation.user']+elements.addressTab.textContent+localStorage.address+";") : settings['iceinformation.user'];
      navigator.mozSettings.createLock().set(settings);
      showToast('qtn_sm_toast_info_saved');
      if (panel.dataset.ftu) {
        ActivityHandler.postResult();
      }
      else {
        SettingsService.navigate('iceinformation-user');
      }
    }

    function _onKeydownHandler(evt){
      switch(evt.key){
        case'ArrowUp':
        case'ArrowDown':
          var input=document.querySelector('li.focus input');
          input&&input.focus();
          _updateSoftkey();
          break;
        default:
      }
    }

    function _canSaved(){
      var firstNameLength=elements.firstName.value.length;
      var lastNameLength=elements.lastName.value.length;
      var conditionLength=elements.condition.value.length;
      var allergiesLength=elements.allergies.value.length;
      var vaccinationLength = elements.vaccination.value.length;
      var medicationLength = elements.medication.value.length;
      var addressLength = elements.address.value.length;
      var birthdayLength = elements.birthday.value.length;
      var heightLength = elements.height.value.length;
      var weightLength = elements.weight.value.length;
      return(firstNameLength>0&&firstNameLength< 32)||
        (lastNameLength>0&&lastNameLength< 32)||
        (conditionLength>0&&conditionLength< 32)||
        (allergiesLength > 0 && allergiesLength < 32) ||
        (vaccinationLength > 0 && vaccinationLength < 32) ||
        (medicationLength > 0 && medicationLength < 32) ||
        (birthdayLength > 0 && birthdayLength < 32) ||
        (heightLength > 0 && heightLength < 32) ||
        (weightLength > 0 && weightLength < 32) ||
        (addressLength > 0 && addressLength < 32);
    }

    function _updateSoftkey(){

      var cancel={
        name:'Cancel',
        l10nId:'cancel',
        priority:1,
        method:function(){
          //JWJ: 2019-04-30 Bug fixing BTS-1223 --Start
          //dump("JWJ:Cancel iceinformation");
          targetPanel = document.getElementById('iceinformation');
          if(targetPanel.dataset.ftu){
              //dump("JWJ:Cancel from FTU, exist settings directly");
              window.close();
              return;
          }
          //JWJ: 2019-04-30 Bug fixing BTS-1223 --End
          //SettingsService.navigate('root');
          SettingsService.navigate('iceinformation-user');
        }
      };

      var save={
        name:'Save',
        l10nId:'save',
        priority:2,
        method:function(){
          //JWJ 2019-05-02 Bug fixing BTS-1269 --Start
          //elements.submitBtn.click();
            var canSaved=_canSaved();
            if(_canSaved()){
            targetPanel = document.getElementById('iceinformation');
            _submit(targetPanel);
            }
          //JWJ 2019-05-02 Bug fixing BTS-1269 --End

        }
      };

      var softkeyParams={
        header:{
          l10nId:'options'
        },
        priority: 3,
        items:[]
      };
      var canSaved=_canSaved();

      if(canSaved){
        softkeyParams.items.push(cancel);
        softkeyParams.items.push(save);

      }else{
        softkeyParams.items.push(cancel);
      }
             let softkeyOption = [
         {
         name: 'Create',
         l10nId: 'qtn_oobe_opt_crt_ICE_contact',
         priority: 5,
         method: () => {
             let activity = new MozActivity({ name: 'ice', data: { type: 'webcontacts/contact' } });
             activity.onsuccess = () =>{
               console.log("Call Create ICE completed");
             }
             activity.onerror = () => {
               console.warn('Activity error', activity.error.name);
             };
           }
         }
       ];
       softkeyParams.items.push.apply(softkeyParams.items, softkeyOption);
       SettingsSoftkey.init(softkeyParams);
       SettingsSoftkey.show();
    }

    function _updateCursorPos(){
      var cursorPosForFirstName=elements.firstName.value.length;
      var cursorPosForLastName=elements.lastName.value.length;
      var cursorPosForCondition=elements.condition.value.length;
      var cursorPosForAllergies=elements.allergies.value.length;
      var cursorPosForVaccination = elements.vaccination.value.length;
      var cursorPosForMedication = elements.medication.value.length;
      var cursorPosForBirthday = elements.birthday.value.length;
      var cursorPosForHeight = elements.height.value.length;
      var cursorPosForWeight = elements.weight.value.length;
      var cursorPosForAddress = elements.address.value.length;
      elements.firstName.setSelectionRange(
        cursorPosForFirstName,cursorPosForFirstName);
      elements.lastName.setSelectionRange(
        cursorPosForLastName,cursorPosForLastName);
      elements.condition.setSelectionRange(
        cursorPosForCondition,cursorPosForCondition);
      elements.allergies.setSelectionRange(
        cursorPosForAllergies,cursorPosForAllergies);
      elements.vaccination.setSelectionRange(
        cursorPosForVaccination, cursorPosForVaccination);
      elements.medication.setSelectionRange(
        cursorPosForMedication, cursorPosForMedication);
      elements.birthday.setSelectionRange(
        cursorPosForBirthday, cursorPosForBirthday);
      elements.height.setSelectionRange(
        cursorPosForHeight, cursorPosForHeight);
      elements.weight.setSelectionRange(
        cursorPosForWeight, cursorPosForWeight);
      elements.address.setSelectionRange(
        cursorPosForAddress, cursorPosForAddress);
        }

    return SettingsPanel({
      onInit:function(panel){
        elements={
          panel:panel,
          firstNameTab:panel.querySelector('#first-name'),
          lastNameTab:panel.querySelector('#last-name'),
          conditionTab:panel.querySelector('#condition'),
          allergiesTab:panel.querySelector('#allergies'),
        medicationTab: panel.querySelector('#medication'),
        vaccinationTab: panel.querySelector('#vaccination'),
        birthdayTab: panel.querySelector('#birthday'),
        heightTab: panel.querySelector('#height'),
        weightTab: panel.querySelector('#weight'),
        addressTab: panel.querySelector('#address'),
          submitBtn:panel.querySelector('button.save-iceinformation'),
          firstName:panel.querySelector('#device-iceinformation-firstname'),
          lastName:panel.querySelector('#device-iceinformation-lastname'),
          condition:panel.querySelector('#device-iceinformation-condition'),
        vaccination: panel.querySelector('#device-iceinformation-vaccination'),
        medication: panel.querySelector('#device-iceinformation-medication'),
        allergies: panel.querySelector('#device-iceinformation-allergies'),
        birthday: panel.querySelector('#device-iceinformation-birthday'),
        height: panel.querySelector('#device-iceinformation-height'),
        weight: panel.querySelector('#device-iceinformation-weight'),
        address: panel.querySelector('#device-iceinformation-address')};
      },

      _initPlaceholder: function(){
        dump("iceinfo _initPlaceholder");
        elements.firstName.setAttribute('placeholder', navigator.mozL10n.get('qtn_oobe_hin_first_phone_name'));
        elements.lastName.setAttribute('placeholder', navigator.mozL10n.get('qtn_oobe_hin_last_phone_name'));
        elements.condition.setAttribute('placeholder', navigator.mozL10n.get('qtn_oobe_hin_medical_conditions'));
        elements.allergies.setAttribute('placeholder', navigator.mozL10n.get('qtn_oobe_hin_known_allergies'));
        elements.vaccination.setAttribute('placeholder', navigator.mozL10n.get('qtn_oobe_hin_relevant'));
        elements.medication.setAttribute('placeholder', navigator.mozL10n.get('qtn_oobe_hin_medical_conditions'));
        elements.birthday.setAttribute('placeholder', navigator.mozL10n.get('qtn_oobe_hin_birth_date'));
        elements.height.setAttribute('placeholder', navigator.mozL10n.get('qtn_oobe_hin_phone_height'));
        elements.weight.setAttribute('placeholder', navigator.mozL10n.get('qtn_oobe_hin_phone_weight'));
        elements.address.setAttribute('placeholder', navigator.mozL10n.get('qtn_oobe_hin_phone_address'));
      },

      onBeforeShow:function(panel){
        window.addEventListener('keydown',_onKeydownHandler);
        elements.firstName.oninput=_updateSoftkey;
        elements.lastName.oninput=_updateSoftkey;
        elements.condition.oninput=_updateSoftkey;
        elements.allergies.oninput=_updateSoftkey;
        elements.vaccination.oninput = _updateSoftkey;
        elements.medication.oninput = _updateSoftkey;
        elements.birthday.oninput = _updateSoftkey;
        elements.height.oninput = _updateSoftkey;
        elements.weight.oninput = _updateSoftkey;
        elements.address.oninput = _updateSoftkey;
        if(localStorage.firstname==undefined){
          localStorage.firstname='';
        }
        if(localStorage.condition==undefined){
          localStorage.condition='';
        }
        if(localStorage.lastname==undefined){
          localStorage.lastname='';
        }
        if(localStorage.allergies==undefined){
          localStorage.allergies='';
        }
        if(localStorage.vaccination==undefined){
        localStorage.vaccination='';
        }
        if(localStorage.medication==undefined){
        localStorage.medication='';
        }
        if(localStorage.birthday==undefined){
        localStorage.birthday='';
        }
        if(localStorage.height==undefined){
        localStorage.height='';
        }
        if(localStorage.weight==undefined){
        localStorage.weight='';
        }
        if(localStorage.address==undefined){
        localStorage.address='';
        }
        elements.firstName.value=localStorage.firstname;
        elements.lastName.value=localStorage.lastname;
        elements.condition.value=localStorage.condition;
        elements.allergies.value=localStorage.allergies;
        elements.vaccination.value=localStorage.vaccination;
        elements.medication.value=localStorage.medication;
        elements.birthday.value=localStorage.birthday;
        elements.height.value=localStorage.height;
        elements.weight.value=localStorage.weight;
        elements.address.value=localStorage.address;
        var inputLi=elements.panel.querySelectorAll('li input');
        for(var i=inputLi.length-1;i>=0;i--){
          inputLi[i].addEventListener('focus',_updateCursorPos);
        }

        this._initPlaceholder();
        //JWJ:2019-05-02 Bug fixing BTS-1269 --Start 
        // The button is not necessary since Save is already softkey
        //
        //elements.submitBtn.addEventListener('click',evt=>{
        //  this.onSubmit(panel);
        //});
        //JWJ 2019-05-02 Bug fixing BTS-1269 --End
      },
      //JWJ 2019-05-02 Bug fixing BTS-1269 --Start
      //onSubmit:function(panel){
      //  dump("JWJ onSubmit");
      //  var canSaved=_canSaved();
      //  if(_canSaved()){
      //    _submit(panel);
      //  }
      //},
      //JWJ 2019-05-02 Bug fixing BTS-1269 --End
      onBeforeHide:function(panel){
        window.removeEventListener('keydown',_onKeydownHandler);
        var inputLi=
          document.querySelectorAll('#iceinformation li input');
        for(var i=inputLi.length-1;i>=0;i--){
          inputLi[i].removeEventListener('focus',_updateCursorPos);
        }
        //JWJ 2019-05-02 Bug fixing BTS-1269 --Start
        //elements.submitBtn.removeEventListener('click',evt=>{
        //  this.onSubmit(panel);
        //});
        //JWJ 2019-05-02 Bug fixing BTS-1269 --End

        SettingsSoftkey.hide();
      },

      onShow:function(){
        elements.firstName.focus();
        _updateSoftkey();
      }
    });
  };
});
