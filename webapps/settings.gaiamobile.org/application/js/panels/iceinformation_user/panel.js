define(['require','modules/settings_panel','modules/settings_service'],function(require) {
  

  var SettingsPanel = require('modules/settings_panel');
  var SettingsService = require('modules/settings_service');


  return function ctor_iceinformation_user() {
    var elements = {};
    function _initSoftKey() {
      var softkeyParams = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Options',
          l10nId: 'options',
          priority: 3,
          method: function() {
            SettingsService.navigate('iceinformation-option');
          }
        }]
      };
      SettingsSoftkey.init(softkeyParams);
      SettingsSoftkey.show();
    }

     function _registerSoftkey() {
       let softkeyOption = [
         {
         name: 'Edit',
         l10nId: 'edit',
         priority: 5,
         method: () => {
           SettingsService.navigate('iceinformation');
           }
         },

         {
           name: 'Share',
           l10nId: 'qtn_sm_opt_share_via_SMS',
           priority: 6,
           method: () => {
             navigator.mozSettings.createLock().get('iceinformation.user').then((res) => {
               let iceinformationUser = res['iceinformation.user'];
               new MozActivity({
                 name: 'new',
                 data: {
                   type: 'websms/sms',
                   body: iceinformationUser
                 }
               });
             });
           }
         },

         {
         name: 'Delete',
         l10nId: 'delete',
         priority: 7,
         method: () => {
           _deleteConfirmDialog();
           }
         },

         {
         name: 'Create',
         l10nId: 'qtn_oobe_opt_crt_ICE_contact',
         priority: 8,
         method: () => {
             let activity = new MozActivity({ name: 'ice', data: { type: 'webcontacts/contact' } });
             activity.onsuccess = () =>{
               console.log("Call Create ICE completed");
             }
             activity.onerror = () => {
               console.warn('Activity error', activity.error.name);
             };
           }
         }
       ];
 
       let softkeyParams = {
         header: {l10nId: 'options'},
         items: []
       }
       softkeyParams.items.push.apply(softkeyParams.items, softkeyOption);
       SettingsSoftkey.init(softkeyParams);
       SettingsSoftkey.show();
     }


    function _deleteConfirmDialog(){
      var dialogConfig={
        title:{
          id:'confirmation',
          args:{}
        },
        body:{
          id:'qtn_sm_dial_delete_ICE_confirm',
          args:{}
        },
        cancel:{
          name:'Cancel',
          l10nId:'cancel',
          priority:1,
          callback:function(){
            dialog.destroy();
          }
        },
        confirm:{
          name:'Yes',
          l10nId:'yes',
          priority:3,
          callback:function(){
            localStorage.firstname='';
            localStorage.lastname='';
            localStorage.condition='';
            localStorage.allergies='';
        localStorage.vaccination='';
        localStorage.medication='';
        localStorage.birthday='';
        localStorage.height='';
        localStorage.weight='';
        localStorage.address='';
        _updateUI(); 
        var settings = {};
        settings['iceinformation.user']= '';
        navigator.mozSettings.createLock().set(settings);
            dialog.destroy();
          }
        },
      };

      var dialog=new ConfirmDialogHelper(dialogConfig);
      dialog.show(document.getElementById('app-confirmation-dialog'));
    }


        function _updateUI() {
        elements.firstName.textContent = localStorage.firstname;
        elements.lastName.textContent = localStorage.lastname;
        elements.condition.textContent = localStorage.condition;
        elements.allergies.textContent = localStorage.allergies;
        elements.vaccination.textContent = localStorage.vaccination;
        elements.medication.textContent = localStorage.medication;
        elements.birthday.textContent = localStorage.birthday;
        elements.height.textContent = localStorage.height;
        elements.weight.textContent = localStorage.weight;
        elements.address.textContent = localStorage.address;
        }

    return SettingsPanel({
      onInit: function(panel) {
        elements = {
          panel: panel,
          firstName: document.getElementById('firstname-desc'),
          lastName: document.getElementById('lastname-desc'),
          condition: document.getElementById('condition-desc'),
        allergies: document.getElementById('allergies-desc'),
        vaccination: document.getElementById('vaccination-desc'),
        medication: document.getElementById('medication-desc'),
        birthday: document.getElementById('birthday-desc'),
        height: document.getElementById('height-desc'),
        weight: document.getElementById('weight-desc'),
        address: document.getElementById('address-desc'),
        };
      },

      onBeforeShow: function() {
        _updateUI();
        //_initSoftKey();
        _registerSoftkey();
      },

      onBeforeHide: function() {
        SettingsSoftkey.hide();
      }
    });
  };
});
