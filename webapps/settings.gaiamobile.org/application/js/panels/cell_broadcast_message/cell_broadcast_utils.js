/* global SettingsSoftkey, DUMP, Settings*/
define(['require','shared/settings_listener'],function(require) {
  

  let SettingsListener = require('shared/settings_listener');
  /** GSM or CDMA map table **/
  const NETWORK_TYPE_GSM = 'gsm';
  const NETWORK_TYPE_CDMA = 'cdma';
  const NETWORK_TYPE_UNKNOWN = 'unknown';
  const CB_CHANNEL_TYPE_CUSTOM = 'custom';
  const CB_CHANNEL_TYPE_PRESET = 'preset';
  /* TODO: GSM NETWORK TYPE */
  const RADIO_TECH_GPRS = 'gprs';
  const RADIO_TECH_EDGE = 'edge';
  const RADIO_TECH_UMTS = 'umts';
  const RADIO_TECH_HSDPA = 'hsdpa';
  const RADIO_TECH_HSUPA = 'hsupa';
  const RADIO_TECH_HSPA = 'hspa';
  const RADIO_TECH_LTE = 'lte';
  const RADIO_TECH_HSPAP = 'hspa+';
  const RADIO_TECH_TD_SCDMA = 'tdscdma';
  const RADIO_TECH_GSM = 'gsm';
  /* TODO: CDMA NETWORK TYPE */
  const RADIO_TECH_IS95A = 'is95a';
  const RADIO_TECH_IS95B = 'is95b';
  const RADIO_TECH_1xRTT = '1xrtt';
  const RADIO_TECH_EVDO_0 = 'evdo0';
  const RADIO_TECH_EVDO_A = 'evdoa';
  const RADIO_TECH_EVDO_B = 'evdob';
  const RADIO_TECH_EHRPD = 'ehrpd';

  let CBS_SERACHLIST_KEY = 'ril.cellbroadcast.searchlist';
  let CBS_PRESET_KEY = 'ril.cellbroadcast.preset.channelslist';
  let CBS_CUSTOM_KEY = 'ril.cellbroadcast.custom.channelslist';

  let CellBroadcastUtilsHelper = (function ListFocusHelper() {
    let deselectSoftkeyParams = {
      menuClassName: 'menu-button',
      header: {
        l10nId: 'message'
      },
      items: [{
        name: 'Deselect',
        l10nId: 'deselect',
        priority: 2,
        method: function() {}
      }]
    };
    let selectSoftkeyParams = {
      menuClassName: 'menu-button',
      header: {
        l10nId: 'message'
      },
      items: [{
        name: 'Select',
        l10nId: 'select',
        priority: 2
      }]
    };

    let DEBUG = true;
    let FileName = '<!-- CBTAG:: cell_broadcast_utils.js -->';
    let readCellBroadcastListDone = false;

    function debug() {
      if (DEBUG) {
        console.log('[' + FileName + '] ' + Array.slice(arguments).concat());
      } else if (window.DUMP) {
        DUMP('[' + FileName + '] ' + Array.slice(arguments).concat());
      }
    }

    function deepCopy(obj) {
      let newObjct = {};
      if (obj instanceof Array) {
        newObjct = [];
      }

      for (let key in obj) {
        let val = obj[key];
        newObjct[key] = typeof val === 'object' ? deepCopy(val) : val;
      }
      return newObjct;
    }

    function dispatchEvent(name, options) {
      let evt = new CustomEvent(name, options);
      window.dispatchEvent(evt);
    }

    function getCurSimlot() {
      let serviceId = window.DsdsSettings.getIccCardIndexForCallSettings();
      return serviceId;
    }

    function setReadCellBroadcastListDone(done) {
      readCellBroadcastListDone = done;
    }

    function getReadCellBroadcastListDone() {
      return readCellBroadcastListDone;
    }

    function updateSoftkey(evt) {
      debug('updateSoftkey::type -> ' + evt.type);
      let focusedElement = document.querySelector('.focus');
      if (focusedElement && focusedElement.classList &&
        focusedElement.classList.contains('none-select')) {
        SettingsSoftkey.hide();
        return;
      }
      if (focusedElement && focusedElement.querySelector('input') &&
        focusedElement.querySelector('input').checked) {
        debug('updateSoftkey::deselectSoftkeyParams ');
        SettingsSoftkey.init(deselectSoftkeyParams);
      } else {
        debug('updateSoftkey::selectSoftkeyParams ');
        SettingsSoftkey.init(selectSoftkeyParams);
      }
      SettingsSoftkey.show();
    }

    function addFocusEventListener(elements, callback) {
      let i = elements.length - 1, domTarget = null;
      if (callback) {
        for (i; i >= 0; i--) {
          domTarget = elements[i].querySelector('input');
          if (domTarget) {
            domTarget.addEventListener('change', callback);
          }
          elements[i].addEventListener('focus', callback);
        }
      } else {
        for (i; i >= 0; i--) {
          domTarget = elements[i].querySelector('input');
          if (domTarget) {
            domTarget.addEventListener('change', updateSoftkey);
          }
          elements[i].addEventListener('focus', updateSoftkey);
        }
      }
    }

    function removeFocusEventListener(elements, callback) {
      let i = elements.length - 1, domTarget = null;
      if (callback) {
        for (i; i >= 0; i--) {
          domTarget = elements[i].querySelector('input');
          if (domTarget) {
            domTarget.removeEventListener('change', callback);
          }
          elements[i].removeEventListener('focus', callback);
        }
      } else {
        for (i; i >= 0; i--) {
          domTarget = elements[i].querySelector('input');
          if (domTarget) {
            domTarget.removeEventListener('change', updateSoftkey);
          }
          elements[i].removeEventListener('focus', updateSoftkey);
        }
      }
    }

    /**
     * For example as the follow:
     * [{'gsm' : "1, 2, 4-6", 'cdma' : "1, 50, 99"}, {'cdma' : "3, 6, 8-9"}]
     */
    function observeDbState() {
      SettingsListener.observe(CBS_CUSTOM_KEY, [], (cResult) => {
        debug('observeDbState::CBS_CUSTOM_KEY change... ');
        handleSettingsDbChanged();
      });
      SettingsListener.observe(CBS_PRESET_KEY, [], (pResult) => {
        debug('observeDbState::CBS_PRESET_KEY change... ');
        handleSettingsDbChanged();
      });
      return handleSettingsDbChanged();
    }

    function handleSettingsDbChanged() {
      return new Promise((resolve) => {
        let lset = [{}, {}];
        getSettingsDbData().then((sResult) => {
          debug('handleSettingsDbChanged::sResult -> ' +
            JSON.stringify(sResult));
          if (!Array.isArray(sResult.allChannels)) {
            return;
          }

          Array.prototype.forEach.call((sResult.allChannels), (channel) => {
            let index = channel.simLots;
            if (channel.enabled) {
              if (channel.isGSM) {
                if (lset[index]['gsm']) {
                  lset[index]['gsm'] =
                    lset[index]['gsm'] + ', ' + channel.channelId;
                } else {
                  lset[index]['gsm'] = channel.channelId;
                }
              } else {
                if (lset[index]['cdma']) {
                  lset[index]['cdma'] =
                    lset[index]['cdma'] + ', ' + channel.channelId;
                } else {
                  lset[index]['cdma'] = channel.channelId;
                }
              }
            }
          });
          let nset = Array.prototype.filter.call((lset), (list) => {
            return Object.keys(list).length !== 0;
          });
          debug('handleSettingsDbChanged lset -> ' + JSON.stringify(lset));
          debug('handleSettingsDbChanged nset -> ' + JSON.stringify(nset));
          setData(CBS_SERACHLIST_KEY, nset);
          resolve(nset);
        });
      });
    }
    function getSettingsDbData() {
      return new Promise((resolve) => {
        return getChannelsList(CBS_PRESET_KEY).then((presetResult) => {
          return getChannelsList(CBS_CUSTOM_KEY).then((customResult) => {
            let channelsList = presetResult.concat(customResult);
            let returnResult = {
              allChannels: channelsList,
              preset: presetResult,
              custom: customResult
            };
            resolve(returnResult);
          });
        });
      });
    }

    function getData(key) {
      return new Promise((resolve, reject) => {
        let request = SettingsListener.getSettingsLock().get(key);

        request.onsuccess = () => {
          let result = request.result[key];
          resolve(result);
        };

        request.onerror = () => {
          reject('ERROR: Get the '+ key +' value failure.');
        };
      });
    }

    function setData(key, value) {
      return new Promise((resolve, reject) => {
        let cset = {};
        cset[key] = value;
        let request = SettingsListener.getSettingsLock().set(cset);

        request.onsuccess = () => {
          resolve(cset);
        };

        request.onerror = () => {
          reject('ERROR: Set the '+ key +' value failure.');
        };
      });
    }

    // compare both object
    function compare(origin, target) {
      if (typeof target === 'object') {
        if (typeof origin !== 'object') {
          return false;
        }

        for (let key of Object.keys(target)) {
          if (!compare(origin[key], target[key])) {
            return false;
          }
        }
        return true;
      } else {
        return origin === target;
      }
    }

    function updateCbChanelsSet(oChannel, nChannel, customSet, presetSet) {
      debug('updateCbChanelsSet::oChannel -> ' + JSON.stringify(oChannel));
      debug('updateCbChanelsSet::nChannel -> ' + JSON.stringify(nChannel));
      let updateSet = false;
      let isCustomCb = (oChannel.channelType === CB_CHANNEL_TYPE_CUSTOM);
      let checkSameCb = (!compare(oChannel, nChannel) &&
        oChannel.channelId === nChannel.channelId &&
        oChannel.channelType === nChannel.channelType);
      let include =
        includeExistedChannelInPresetDB(oChannel, nChannel, presetSet);
      let allowUpdate = (isCustomCb && !include) || checkSameCb;

      debug('updateCbChanelsSet::allowUpdate -> ' + allowUpdate);
      let lset = isCustomCb ? customSet : presetSet;
      debug('updateCbChanelsSet::lset before -> ' + JSON.stringify(lset));
      let cIndex = Array.prototype.findIndex.call((lset), (els) => {
        return compare(els, oChannel);
      });
      let vIndex = Array.prototype.findIndex.call((lset), (els) => {
        return compare(els, nChannel);
      });

      debug('updateCbChanelsSet::cIndex -> ' + cIndex +
        ', vIndex -> ' + vIndex);
      if (cIndex !== -1 && allowUpdate) {
        updateSet = true;
        if (vIndex !== -1) {
          lset.splice(cIndex, 1);
        } else {
          lset.splice(cIndex, 1, nChannel);
        }
      }
      debug('updateCbChanelsSet::lset after-> ' + JSON.stringify(lset));
      return updateSet;
    }

    function includeExistedChannelInPresetDB(oChannel, nChannel, pset) {
      let include = Array.prototype.some.call((pset), (channel) => {
        if (nChannel.channelId && nChannel.channelId.indexOf('-') > 0) {
          let limitRange = nChannel.channelId.split('-');
          let minBound = parseInt(limitRange[0]);
          let maxBound = parseInt(limitRange[1]);

          if (channel.channelId.indexOf('-') > 0) {
            let olimitRange = channel.channelId.split('-');
            let ominBound = parseInt(olimitRange[0]);
            let omaxBound = parseInt(olimitRange[1]);

            /* TODO: For Example: 3-6, 1-8 */
            let filter1 = (minBound <= ominBound && maxBound >= omaxBound &&
              (nChannel.channelId !== ominBound + '-' + omaxBound));
            /* TODO: For Example: 3-6, 1-5 */
            let filter2 = (minBound <= ominBound &&
              maxBound >= ominBound && maxBound <= omaxBound &&
              (nChannel.channelId !== minBound + '-' + omaxBound));
            /* TODO: For Example: 3-6, 5-8 */
            let filter3 = (minBound >= ominBound &&
              minBound <= omaxBound && maxBound >= omaxBound &&
              (nChannel.channelId !== ominBound + '-' + maxBound));
            /* TODO: For Example: 3-6, 4-5/3-6 */
            let filter4 = (ominBound <= minBound && omaxBound >= maxBound);

            return filter1 || filter2 || filter3 || filter4;
          } else {
            /* TODO: For Example: 5, 3-6 */
            let oChannelId = parseInt(channel.channelId);
            return (oChannelId >= minBound && oChannelId <= maxBound);
          }
        } else {
          if (channel.channelId.indexOf('-') > 0) {
            let olimitRange = channel.channelId.split('-');
            let ominBound = parseInt(olimitRange[0]);
            let omaxBound = parseInt(olimitRange[1]);

            let nChannelId = parseInt(nChannel.channelId);
            /* TODO: For Example: 3-6, 5 */
            return (nChannelId >= ominBound && nChannelId <= omaxBound);
          } else {
            /* TODO: For Example: 5, 5 */
            return nChannel.channelId === channel.channelId;
          }
        }
      });

      return include;
    }

    function isUpdateCbChannelsDB(key, type, nChannel, oChannel) {
      debug('isUpdateCbChannelsDB::nChannel.channelId -> ' + nChannel.channelId);
      return new Promise((resolve) => {
        getSettingsDbData().then((sResult) => {
          debug('isUpdateCbChannelsDB::sResult -> ' + JSON.stringify(sResult));
          if (!Array.isArray(sResult.allChannels)) {
            return;
          }

          if (nChannel.channelId && nChannel.channelId.indexOf('-') > 0) {
            let isContainCb = false, isUpdateCbDb = false;
            let aset = [].concat(sResult.allChannels);
            let cset = [].concat(sResult.custom);
            let pset = [].concat(sResult.preset);
            let iterations = Array.prototype.filter.call((aset), (channel) => {
              return nChannel.simLots === channel.simLots;
            });
            Array.prototype.forEach.call((iterations), (channel) => {
              let limitRange = nChannel.channelId.split('-');
              let minBound = parseInt(limitRange[0]);
              let maxBound = parseInt(limitRange[1]);
              let filter = channel.channelId !== nChannel.channelId;

              debug('isUpdateCbChannelsDB::[nChannel, channel] 1 -> [ ' +
                nChannel.channelId + ', '　+ channel.channelId + ' ]');

              if (channel.channelId.indexOf('-') > 0) {
                let olimitRange = channel.channelId.split('-');
                let ominBound = parseInt(olimitRange[0]);
                let omaxBound = parseInt(olimitRange[1]);

                /* TODO: For Example: 3-6, 1-8 */
                let filter1 = (minBound <= ominBound && maxBound >= omaxBound &&
                  (nChannel.channelId !== ominBound + '-' + omaxBound));
                /* TODO: For Example: 3-6, 1-5 */
                let filter2 = (minBound <= ominBound &&
                  maxBound >= ominBound && maxBound <= omaxBound &&
                  (nChannel.channelId !== minBound + '-' + omaxBound));
                /* TODO: For Example: 3-6, 5-8 */
                let filter3 = (minBound >= ominBound &&
                  minBound <= omaxBound && maxBound >= omaxBound &&
                  (nChannel.channelId !== ominBound + '-' + maxBound));
                /* TODO: For Example: 3-6, 4-5/3-6 */
                let filter4 = (ominBound <= minBound && omaxBound >= maxBound);

                debug('isUpdateCbChannelsDB::filter -> [ ' + filter + ', ' +
                  filter1 + ', ' + filter2 + ', ' +
                  filter3 + ', ' + filter4 + ' ]');

                if (filter1 || filter2 || filter3) {
                  if (type === 'add') {
                    if (filter2) {
                      nChannel.channelId = minBound + '-' + omaxBound;
                    } else if (filter3) {
                      nChannel.channelId = ominBound + '-' + maxBound;
                    }
                  }

                  let change = updateCbChanelsSet(channel, nChannel, cset, pset);
                  if (filter) {
                    isUpdateCbDb = true;
                  }
                  debug('isUpdateCbChannelsDB::change -> ' + change);
                  isContainCb = !change;
                } else {
                  if (filter4) {
                    if (filter) {
                      debug('isUpdateCbChannelsDB::channelId is different.');
                      if (compare(oChannel, channel) && type === 'edit') {
                        updateCbChanelsSet(channel, nChannel, cset, pset);
                        return;
                      }
                      isContainCb = true;
                    } else if ('click' === type) {
                      debug('isUpdateCbChannelsDB::channelId is same.');
                      updateCbChanelsSet(channel, nChannel, cset, pset);
                    } else {
                      isContainCb = true;
                    }
                  }
                }
              } else {
                let oldChannelId = parseInt(channel.channelId);
                /* TODO: For Example: 3, 1-5 */
                if (oldChannelId >= minBound && oldChannelId <= maxBound) {
                  let include =
                    includeExistedChannelInPresetDB(channel, nChannel, pset);
                  debug('isUpdateCbChannelsDB::include 1 -> ' + include);
                  if (!include) {
                    let change =
                      updateCbChanelsSet(channel, nChannel, cset, pset);
                    if (filter) {
                      isUpdateCbDb = true;
                    }
                    debug('isUpdateCbChannelsDB::change 1 -> ' + change);
                    isContainCb = !change;
                  } else {
                    isContainCb = true;
                  }
                }
              }
            });

            aset = pset.concat(cset);
            debug('isUpdateCbChannelsDB::isContainCb 1 -> ' + isContainCb);
            let returnResult = {
              preset: pset,
              custom: cset,
              channel: isContainCb ? oChannel : nChannel,
              allChannels: aset,
              allow: !isContainCb,
              update: isUpdateCbDb,
            };
            resolve(returnResult);
          } else {
            let isContainCb = false, isUpdateCbDb = false;
            let aset = [].concat(sResult.allChannels);
            let cset = [].concat(sResult.custom);
            let pset = [].concat(sResult.preset);
            let iterations = Array.prototype.filter.call((aset), (channel) => {
              return nChannel.simLots === channel.simLots;
            });

            isContainCb = Array.prototype.some.call((iterations), (channel) => {
              debug('isUpdateCbChannelsDB::[nChannel, channel] 2 -> [ ' +
                nChannel.channelId + ', ' + channel.channelId + ' ]');
              let filter = !compare(nChannel, channel) &&
                channel.channelId === nChannel.channelId;
              debug('isUpdateCbChannelsDB::filter 2 -> ' + filter);
              if (channel.channelId.indexOf('-') > 0) {
                let olimitRange = channel.channelId.split('-');
                let ominBound = parseInt(olimitRange[0]);
                let omaxBound = parseInt(olimitRange[1]);
                let nlimitBound = parseInt(nChannel.channelId);

                /*TODO: For Example: 1-5, 3*/
                if (nlimitBound >= ominBound && nlimitBound <= omaxBound) {
                  //reject('This channel has exist, must not add/edit it');
                  if (compare(oChannel, channel) && type === 'edit') {
                    updateCbChanelsSet(channel, nChannel, cset, pset);
                    return;
                  }
                  return true;
                } else if (filter) {
                  updateCbChanelsSet(channel, nChannel, cset, pset);
                }
              } else {
                if (compare(channel, nChannel) ||
                  ('click' !== type && filter)) {
                  //reject('This channel has exist, must not add/edit it');
                  return true;
                } else if (filter) {
                  let change = updateCbChanelsSet(channel, nChannel, cset, pset);
                  debug('isUpdateCbChannelsDB::change 2 -> ' + change);
                  return !change;
                }
              }
            });
            aset = pset.concat(cset);
            debug('isUpdateCbChannelsDB::isContainCb 2 -> ' + isContainCb);
            let returnResult = {
              preset: pset,
              custom: cset,
              channel: isContainCb ? oChannel : nChannel,
              allChannels: aset,
              allow: !isContainCb,
              update: isUpdateCbDb,

            };
            resolve(returnResult);
          }
        });
      });
    }

    /**
    * <li role="menuitem" class="none-select">
    *   <label class="pack-checkbox">
    *     <input type="checkbox"
    *     name="ril.cellbroadcast.enabled" disabled="true">
    *     <span data-l10n-id="">CB channel</span>
    *     <small data-l10n-args="" data-l10n-id="channel-id"
    *     class="menu-item-desc">50</small>
    *     <label data-l10n-args="" data-l10n-id="cell-broadcast-desc"
     *     class="hidden menu-item-desc"></label>
    *   </label>
    * </li>
    *
    * */

    function genChannelTemplate(itemData) {
      let liItem = document.createElement('li');
      let lableItem = document.createElement('label');
      let inputItem = document.createElement('input');
      let spanItem = document.createElement('span');
      let smallItem = document.createElement('small');
      let bdiItem = document.createElement('bdi');

      liItem.setAttribute('role', 'menuitem');
      liItem.dataset.channelId = itemData.channelId;
      if (itemData.hasOwnProperty('disabled')) {
        if (itemData.disabled) {
          liItem.setAttribute('class', 'none-select');
          inputItem.setAttribute('disabled', 'true');
        }
      } else {
        itemData.disabled = false;
      }

      lableItem.setAttribute('class', 'pack-checkbox');
      inputItem.setAttribute('type', 'checkbox');
      if (!itemData.hasOwnProperty('enabled')) {
        itemData.enabled = true;
      }
      inputItem.checked = itemData.enabled;
      inputItem.setAttribute('checked', itemData.enabled);
      let channelName = itemData.channelName || 'CB channel';
      //spanItem.textContent = channelName + ' ' + itemData.channelId;
      spanItem.textContent = channelName;
      smallItem.setAttribute('class', 'menu-item-desc');
      smallItem.setAttribute('style', 'width: 80%');
      smallItem.setAttribute('data-l10n-id', 'channel-id');
      smallItem.setAttribute('data-l10n-args', JSON.stringify({
        channelId: itemData.channelId
      }));

      let cbChannel = repairCbChanel(itemData);
      bdiItem.setAttribute('class', 'hidden');
      bdiItem.setAttribute('data-l10n-id', 'cell-broadcast-desc');
      bdiItem.setAttribute('data-l10n-args', JSON.stringify({
        'channelType': cbChannel.channelType,
        'channelName': cbChannel.channelName,
        'channelId': cbChannel.channelId,
        'simLots': cbChannel.simLots,
        'isGSM': cbChannel.isGSM,
        'enabled': cbChannel.enabled,
        'disabled': cbChannel.disabled
      }));

      lableItem.appendChild(inputItem);
      lableItem.appendChild(spanItem);
      lableItem.appendChild(smallItem);
      lableItem.appendChild(bdiItem);
      liItem.appendChild(lableItem);

      return liItem;
    }

    function isGsm(radioTechnology) {
      return radioTechnology === RADIO_TECH_GPRS ||
        radioTechnology === RADIO_TECH_EDGE ||
        radioTechnology === RADIO_TECH_UMTS ||
        radioTechnology === RADIO_TECH_HSDPA ||
        radioTechnology === RADIO_TECH_HSUPA ||
        radioTechnology === RADIO_TECH_HSPA ||
        radioTechnology === RADIO_TECH_LTE ||
        radioTechnology === RADIO_TECH_HSPAP ||
        radioTechnology === RADIO_TECH_TD_SCDMA ||
        radioTechnology === RADIO_TECH_GSM;
    }

    function isCdma(radioTechnology) {
      return radioTechnology === RADIO_TECH_IS95A ||
        radioTechnology === RADIO_TECH_IS95B ||
        radioTechnology === RADIO_TECH_1xRTT ||
        radioTechnology === RADIO_TECH_EVDO_0 ||
        radioTechnology === RADIO_TECH_EVDO_A ||
        radioTechnology === RADIO_TECH_EVDO_B ||
        radioTechnology === RADIO_TECH_EHRPD;
    }

    function getNetworkType() {
      let serviceId = getCurSimlot();
      let mobileConnection = navigator.mozMobileConnections[serviceId];
      let networkType = mobileConnection.data.type;
      debug('getNetworkType::networkType -> ' + networkType);
      if (isGsm(networkType)) {
        return NETWORK_TYPE_GSM;
      } else if (isCdma(networkType)){
        return NETWORK_TYPE_CDMA;
      } else {
        return NETWORK_TYPE_UNKNOWN;
      }
    }

    function repairCbChanel(channel) {
      if (!channel) {
        return null;
      }

      let nType = getNetworkType();
      channel.isGSM = (nType === NETWORK_TYPE_GSM) ? true : false;
      let cbChannel = {};
      cbChannel.simLots =
        channel.hasOwnProperty('simLots') ? channel.simLots : getCurSimlot();
      cbChannel.channelType =
        channel.hasOwnProperty('channelType') ? channel.channelType : 'custom';
      cbChannel.channelId =
        channel.hasOwnProperty('channelId') ? channel.channelId : 0;
      cbChannel.channelName =
        channel.hasOwnProperty('channelName') ?
        channel.channelName : 'CB channel';
      cbChannel.isGSM =
        channel.hasOwnProperty('isGSM') ? channel.isGSM : true;
      cbChannel.enabled =
        channel.hasOwnProperty('enabled') ? channel.enabled : true;
      cbChannel.disabled =
        channel.hasOwnProperty('disabled') ? channel.disabled : false;

      return cbChannel;
    }

    function getChannelsList(key) {
      return Promise.resolve(getData(key));
    }

    function setChannelsList(key, type, oChannel, nChannel) {
      return new Promise((resolve) => {
        return isUpdateCbChannelsDB(key, type, nChannel, oChannel)
          .then((result) => {
          debug('setChannelsList::result -> ' + JSON.stringify(result));
          debug('setChannelsList::allow -> ' + result.allow);
          debug('setChannelsList::update -> ' + result.update);
          if (result.allow) {
            let CBS_TYPE = (key === CBS_PRESET_KEY) ?
              CB_CHANNEL_TYPE_PRESET : CB_CHANNEL_TYPE_CUSTOM;
            let matchChannelList = result[CBS_TYPE];
            let channelList = [].concat(matchChannelList);
            if (!result.update) {
              channelList.push(nChannel);
            }
            setData(key, channelList).then(() => {
              resolve(result);
            });
          } else {
            resolve(result);
          }
        });
      });
    }

    function modifyChannelsList(key, type, oChannel, nChannel) {
      return new Promise((resolve) => {
        return isUpdateCbChannelsDB(key, type, nChannel, oChannel)
          .then((result) => {
          debug('modifyChannelsList::result -> ' + JSON.stringify(result));
          debug('modifyChannelsList::allow -> ' + result.allow);
          debug('modifyChannelsList::update -> ' + result.update);
          if (result.allow) {
            let CBS_TYPE = (key === CBS_PRESET_KEY) ?
              CB_CHANNEL_TYPE_PRESET : CB_CHANNEL_TYPE_CUSTOM;
            let matchChannelList = result[CBS_TYPE];
            let channelList = [].concat(matchChannelList);
            if (!result.update) {
              Array.prototype.forEach.call((channelList),
                (curChannel, index) => {
                if (curChannel.channelId === oChannel.channelId) {
                  channelList.splice(index, 1, nChannel);
                }
              });
            }

            debug('modifyChannelsList::channelList -> ' +
              JSON.stringify(channelList));

            setData(key, channelList).then(() => {
              resolve(result);
            });
          } else {
            resolve(result);
          }
        });
      });
    }

    function createPresetChannelPanel(curPanleId) {
      return new Promise((resolve) => {
        return getChannelsList(CBS_PRESET_KEY).then((channels) => {
          debug('createPresetChannelPanel::preset channels -> ' +
            JSON.stringify(channels));
          if (!channels.length) {
            return resolve(channels);
          }

          let fResult = Array.prototype.filter.call((channels), (channel)=> {
            return channel.simLots === getCurSimlot();
          });

          if (!fResult.length) {
            return resolve(fResult);
          }

          Array.prototype.forEach.call((fResult), (channel, index)=> {
            createChannelPanel(curPanleId, channel);
            if (index === fResult.length - 1) {
              return resolve(fResult);
            }
          });
        });
      });
    }

    function createCustomChannelPanel(curPanleId) {
      return new Promise((resolve) => {
        return getChannelsList(CBS_CUSTOM_KEY).then((channels) => {
          debug('createCustomChannelPanel::custom channels -> ' +
            JSON.stringify(channels));

          if (!channels.length) {
            return resolve(channels);
          }

          let fResult = Array.prototype.filter.call((channels), (channel) => {
            return channel.simLots === getCurSimlot();
          });

          if (!fResult.length) {
            return resolve(fResult);
          }

          Array.prototype.forEach.call((fResult), (channel, index) => {
            createChannelPanel(curPanleId, channel);
            if (index === fResult.length - 1) {
              return resolve(fResult);
            }
          });
        });
      });
    }

    function displayCbChannels(curPanleId, callback) {
      clearChannelPanel(curPanleId);
      createPresetChannelPanel(curPanleId).then((presetReuslt) => {
        createCustomChannelPanel(curPanleId).then((customResult) => {
          let disChannels = presetReuslt.concat(customResult);
          let filterEmptyListRule = '#cell-channels-config > #empty-list';
          let emptyListNode = document.querySelector(filterEmptyListRule);
          debug('displayCbChannels::disChannels -> ' + disChannels.length);
          if (!disChannels.length) {
            emptyListNode.hidden = false;
          } else {
            emptyListNode.hidden = true;
          }

          if (callback && ('function' === typeof callback)) {
            callback();
          }
        });
      });
    }

    /**
     * {
     *   "simLots":0,
     *   "channelType":"custom",
     *   "channelId":"60",
     *   "channelName":"CB channel",
     *   "isGSM":true,
     *   "enabled":true,
     *   "disabled": false
     * }
    */
    function addChannelToCbList(type, oChannel, nChannel) {
      let cb_oChannel = repairCbChanel(oChannel);
      let cb_nChannel = repairCbChanel(nChannel);
      debug('addChannelToCbList::cb_oChannel -> ' +
        JSON.stringify(cb_oChannel));
      debug('addChannelToCbList::cb_nChannel -> ' +
        JSON.stringify(cb_nChannel));
      let CBS_KEY = cb_nChannel.channelType === CB_CHANNEL_TYPE_CUSTOM ?
        CBS_CUSTOM_KEY : CBS_PRESET_KEY;
      debug('addChannelToCbList::CBS_KEY -> ' + CBS_KEY);
      return setChannelsList(CBS_KEY, type, cb_oChannel, cb_nChannel);
    }

    function updateChannelToCbList(type, oChannel, nChannel) {
      debug('updateChannelToCbList::oChannel -> ' + JSON.stringify(oChannel));
      debug('updateChannelToCbList::nChannel -> ' + JSON.stringify(nChannel));
      let CBS_KEY = oChannel.channelType === CB_CHANNEL_TYPE_CUSTOM ?
        CBS_CUSTOM_KEY : CBS_PRESET_KEY;
      debug('updateChannelToCbList::CBS_KEY -> ' + CBS_KEY);
      return modifyChannelsList(CBS_KEY, type, oChannel, nChannel);
    }

    function delChannelFromCbList(channel) {
      debug('delChannelFromCbList::channel -> ' + JSON.stringify(channel));
      return new Promise((resolve) => {
        let CBS_KEY = channel.channelType === CB_CHANNEL_TYPE_CUSTOM ?
          CBS_CUSTOM_KEY : CBS_PRESET_KEY;
        debug('delChannelFromCbList::CBS_KEY -> ' + CBS_KEY);
        return getChannelsList(CBS_KEY).then((cbResult) => {
          Array.prototype.forEach.call((cbResult), (curChannel, index) => {
            if (curChannel.channelId === channel.channelId) {
              cbResult.splice(index, 1);
            }
          });
          debug('delChannelFromCbList::cbResult -> ' +
            JSON.stringify(cbResult));
          resolve(setData(CBS_KEY, cbResult));
        });
      });
    }

    function getCurChannelAttribute(element) {
      let idom = null, bdom = null;
      if (!element) {
        let curPanleId = Settings.currentPanel;
        idom = document.querySelector(curPanleId + ' li.focus input');
        bdom = document.querySelector(curPanleId + ' li.focus bdi');
      } else {
        idom = element.querySelector('input');
        bdom = element.querySelector('bdi');
      }

      let options = null;
      if (bdom && bdom.getAttribute('data-l10n-args')) {
        options = JSON.parse(bdom.getAttribute('data-l10n-args'));
      }

      if (idom) {
        options.enabled = idom.checked;
      }
      debug('getCurChannelAttribute  options -> ' + JSON.stringify(options));
      return options;
    }

    function clickCurFocusChannel() {
      debug('clickCurFocusChannel...');
      let curChannelAttr = getCurChannelAttribute();
      let oChannel = deepCopy(curChannelAttr);
      let nChannel = deepCopy(curChannelAttr);
      oChannel.enabled = !curChannelAttr.enabled;
      updateChannelToCbList('click', oChannel, nChannel);
    }

    function createChannelPanel(id, channel) {
      if (!channel) {
        return;
      }

      debug('createChannelPanel::id-currentPanel -> ' + id +
        ', '+ Settings.currentPanel);
      let root = document.querySelector(id + ' ul');
      let channelItem = genChannelTemplate(channel);
      let listFragment = document.createDocumentFragment();
      listFragment.appendChild(channelItem);
      root.appendChild(listFragment);
    }

    function clearChannelPanel(id) {
      debug('clearChannelPanel::id currentPanel -> ' + id +
        ', '+ Settings.currentPanel);
      let dom = document.querySelector(id + ' ul');
      let liNodes = document.querySelectorAll(id + ' ul li');
      Array.prototype.forEach.call((liNodes), (node) => {
        if (node) {
          dom.removeChild(node);
        }
      });
    }

    function editChannlePanel(oChannel, nChannel) {
      let parentNodeItem =
        document.querySelector(Settings.currentPanel + ' ul');
      let oldChildItem =
        document.querySelector(Settings.currentPanel +
        ' li[data-channel-id="' + oChannel.channelId + '"]');
      let newChildItem = genChannelTemplate(nChannel);
      newChildItem.querySelector('input').checked = oChannel.enabled;
      parentNodeItem.replaceChild(newChildItem, oldChildItem);
    }

    function saveChannelToDB(id, oChannel, nChannel) {
      return new Promise((resolve, reject) => {
        return addChannelToCbList('add', oChannel, nChannel).then((result) => {
          debug('saveChannelToDB allow -> ' + result.allow +
            ', update -> ' + result.update);
          if (result.allow) {
            if (!result.update) {

            }
            if (result.update) {
              dispatchEvent('update-channel-config', {
                detail: {
                  focusChannel: result.channel
                }
              });
              reject(result);
            } else {
              createChannelPanel(id, nChannel);
              resolve(result);
            }
          } else {
            showToast('add-channel-failure');
            resolve(result);
          }
        });
      });
    }

    function modifyChannelFromDB(oChannel, nChannel) {
      nChannel = repairCbChanel(nChannel);
      debug('modifyChannelFromDB::currentPanel -> ' + Settings.currentPanel +
        ', nChannel -> ' + JSON.stringify(nChannel));
      return new Promise((resolve, reject) => {
        updateChannelToCbList('edit', oChannel, nChannel).then((result) => {
          if (result.allow) {
            if (result.update) {
              dispatchEvent('update-channel-config', {
                detail: {
                  focusChannel: result.channel
                }
              });
              showToast('changessaved');
              reject(result);
            } else {
              editChannlePanel(oChannel, nChannel);
              showToast('changessaved');
              resolve(result);
            }
          } else {
            showToast('edit-channel-failure');
            resolve(result);
          }
        });
      });
    }

    function findNeedFocusNode(navId) {
      let curPanleId = Settings.currentPanel;
      let focusedNode = document.querySelector(curPanleId + ' li.focus');
      debug('findNeedFocusNode::focusedNode -> ' + focusedNode.style.cssText);
      if (!navId) {
        navId = focusedNode.style.getPropertyValue('--nav-down');
      }
      debug('findNeedFocusNode::navId -> ' + navId);

      let liNodes = document.querySelectorAll(curPanleId + ' ul > li');
      let needFocusNode = null;
      if (liNodes) {
        for (let i = 0, len = liNodes.length; i < len; i++) {
          debug('findNeedFocusNode::liNodes['+i+'] -> ' +
            liNodes[i].getAttribute('data-nav-id'));
          if (liNodes[i].getAttribute('data-nav-id') === navId) {
            needFocusNode = liNodes[i];
            break;
          }
        }
      }
      let curChannel = getCurChannelAttribute(needFocusNode);

      return curChannel;
    }

    function removeChannelFromDB() {
      return new Promise((resolve) => {
        let curChannel = getCurChannelAttribute();
        let curPanleId = Settings.currentPanel;
        let dom = document.querySelector(curPanleId + ' ul');
        let node = document.querySelector(curPanleId + ' li.focus');
        debug('removeChannelFromDB::curChannel -> ' +
          JSON.stringify(curChannel));
        let nextFocusNode = findNeedFocusNode();
        debug('removeChannelFromDB::nextFocusNode -> ' +
          JSON.stringify(nextFocusNode));

        if (node) {
          dom.removeChild(node);
        }

        return delChannelFromCbList(curChannel).then(() => {
          resolve(nextFocusNode);
        });
      });
    }

    return {
      get: getData,
      set: setData,
      copy: deepCopy,
      observe: observeDbState,
      curSimlot: getCurSimlot,
      getChannels: getChannelsList,
      setChannels: setChannelsList,
      showCbChannels: displayCbChannels,
      addEventListener: addFocusEventListener,
      removeEventListener: removeFocusEventListener,
      addChannel: createChannelPanel,
      clearChannel: clearChannelPanel,
      saveChannel: saveChannelToDB,
      editChannel: modifyChannelFromDB,
      deleteChannel: removeChannelFromDB,
      clickCurChannel: clickCurFocusChannel,
      getCurChannel: getCurChannelAttribute,
      setReadDone: setReadCellBroadcastListDone,
      getReadDone: getReadCellBroadcastListDone,
    };
  })();
  return CellBroadcastUtilsHelper;
});
