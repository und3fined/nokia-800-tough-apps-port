/* global SettingsSoftkey, DsdsSettings, ListFocusHelper, DsdsSettings */
/**
 * Used to show Emergency alert panel
 */
define(['require','modules/settings_panel','shared/settings_listener','panels/cell_broadcast_message/cell_broadcast_switch','panels/cell_broadcast_message/cell_broadcast_utils'],function(require) {
  

  let SettingsPanel = require('modules/settings_panel');
  let SettingsListener = require('shared/settings_listener');
  let CellBroadcastSwitchModeItem =
    require('panels/cell_broadcast_message/cell_broadcast_switch');
  let UtilsHelper =
    require('panels/cell_broadcast_message/cell_broadcast_utils');

  return function ctor_emergency_alert_panel() {
    let DEBUG = true;
    let FileName = '<!-- CBTAG:: cell_broadcast_message.js -->';
    let curPanleId = '#cell-broadcast-message';
    let cellBroadcastSwitchModeItem = null;
    let rilCbDisabled = 'ril.cellbroadcast.disabled';
    let settingElements = [rilCbDisabled];
    let listElements = document.querySelectorAll(curPanleId + ' li');

    function debug() {
      if (DEBUG) {
        console.log('[' + FileName + '] ' + Array.slice(arguments).concat());
      } else if (window.DUMP) {
        DUMP('[' + FileName + '] ' + Array.slice(arguments).concat());
      }
    }

    let deselectSoftkeyParams = {
      menuClassName: 'menu-button',
      header: {
        l10nId: 'message'
      },
      items: [{
        name: 'Deselect',
        l10nId: 'deselect',
        priority: 2,
        method: function() {}
      }]
    };
    let selectSoftkeyParams = {
      menuClassName: 'menu-button',
      header: {
        l10nId: 'message'
      },
      items: [{
        name: 'Select',
        l10nId: 'select',
        priority: 2
      }]
    };

    function updateSoftkey() {
      let focusedElement = document.querySelector(curPanleId + ' .focus');
      if (focusedElement && focusedElement.classList &&
          focusedElement.classList.contains('none-select')) {
        SettingsSoftkey.hide();
        return;
      }
      if (focusedElement && focusedElement.querySelector('input') &&
          focusedElement.querySelector('input').checked) {
        SettingsSoftkey.init(deselectSoftkeyParams);
      } else {
        SettingsSoftkey.init(selectSoftkeyParams);
      }
      SettingsSoftkey.show();
    }

    function initAlertSettingListener() {
      let i = settingElements.length - 1;
      for (i; i >= 0; i--) {
        SettingsListener.observe(settingElements[i], [], updateSoftkey);
      }
    }

    function removeAlertSettingListener() {
      let i = settingElements.length - 1;
      for (i; i >= 0; i--) {
        SettingsListener.unobserve(settingElements[i], updateSoftkey());
      }
    }

    function updateChannelsPanel(disable) {
      debug('updateChannelsPanel::disable -> ' + disable);
      let channelsPanel =
        document.querySelectorAll(curPanleId + ' ul:nth-child(2) > li');
      Array.prototype.forEach.call(channelsPanel, (element) => {
        let hrefItem = element.querySelector('a');
        if (disable) {
          hrefItem.removeAttribute('href');
          element.setAttribute('aria-disabled', true);
          element.classList.add('none-select');
        } else {
          hrefItem.setAttribute('href', hrefItem.dataset.hash);
          element.removeAttribute('aria-disabled');
          element.classList.remove('none-select');
        }
      });
    }

    function relatedCbSwitch() {
      SettingsListener.observe(rilCbDisabled, true, (cbResult) => {
        let serviceId = window.DsdsSettings.getIccCardIndexForCallSettings();
        debug('relatedCbSwitch::serviceId -> ' + serviceId +
          ' cbResult -> ' + JSON.stringify(cbResult));
        updateChannelsPanel(cbResult[serviceId]);
      });
    }

    function updateCbSwitch() {
      return new Promise((resolve) => {
        let req = SettingsListener.getSettingsLock().get(rilCbDisabled);
        req.onsuccess = () => {
          let cbResult = req.result[rilCbDisabled];
          let serviceId = window.DsdsSettings.getIccCardIndexForCallSettings();
          debug('updateCbSwitch::serviceId -> ' + serviceId +
            ' cbResult -> ' + JSON.stringify(cbResult));
          updateChannelsPanel(cbResult[serviceId]);

          let select = cellBroadcastSwitchModeItem._select;
          let cbEnabled = !cbResult[serviceId] ? 'true' : 'false';
          if (select.value !== cbEnabled) {
            select.value = cbEnabled;
          }
          select.hidden = false;
          resolve();
        };
      });
    }

    return SettingsPanel({
      onInit: function(panel) {
        debug('onInit');
        cellBroadcastSwitchModeItem = CellBroadcastSwitchModeItem({
          cellBroadcastSwitchModeItem:
            panel.querySelector('#cellBroadcast_mode_switch'),
          cellBroadcastSwitchModeSelect:
            panel.querySelector('#cellBroadcast-mode-select')
        });
        relatedCbSwitch();
      },

      onBeforeShow: function() {
        debug('onBeforeShow');
        updateCbSwitch().then(() => {
          updateSoftkey();
          ListFocusHelper.addEventListener(listElements, updateSoftkey);
          initAlertSettingListener();
        });
      },

      onBeforeHide: function() {
        debug('onBeforeHide');
        UtilsHelper.setReadDone(false);
        ListFocusHelper.removeEventListener(listElements, updateSoftkey);
        removeAlertSettingListener();
        SettingsSoftkey.hide();
      }
    });
  };
});
