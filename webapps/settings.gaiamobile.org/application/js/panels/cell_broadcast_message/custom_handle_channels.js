/* global SettingsSoftkey, Settings*/

define(['require','modules/dialog_panel'],function(require) {
  

  let DialogPanel = require('modules/dialog_panel');

  return function ctor_call_voice_mail_settings() {
    let elements = null;
    let DEBUG = true;
    let DELAYED_INIT_TIMEOUT = 500;
    let FileName = '<!-- CBTAG:: custom_handle_channels.js -->';
    let curPanleId = '#custom-handle-channels';

    function debug() {
      if (DEBUG) {
        console.log('[' + FileName + '] ' + Array.slice(arguments).concat());
      } else if (window.DUMP) {
        DUMP('[' + FileName + '] ' + Array.slice(arguments).concat());
      }
    }

    return DialogPanel({
      dispatchEvent: function(name, options) {
        let evt = new CustomEvent(name, options);
        window.dispatchEvent(evt);
      },
      initpanelready: function() {
        debug('initpanelready::current -> ' + elements.panel.id);
        let evt = new CustomEvent("panelready", {
          detail: {
            current: '#' + elements.panel.id,
            previous: "#cell-channels-config",
          }
        });
        window.dispatchEvent(evt);
      },
      dispatchDialogShowEvent: function() {
        debug('dispatchDialogShowEvent::current -> ' + elements.panel.id);
        this.dispatchEvent('dialogpanelshow', {
          detail: {
            dialogpanel: '#' + elements.panel.id
          }
        });
      },
      dispatchDialogHideEvent: function() {
        debug('dispatchDialogHideEvent::current -> ' + elements.panel.id);
        this.dispatchEvent('dialogpanelhide', {
          detail: {
            dialogpanel: '#' + elements.panel.id
          }
        });
      },
      _handleVisibiltychange: function() {
        debug('_handleVisibiltychange::settingsCurrentPanel -> ' + Settings.currentPanel);
        debug('_handleVisibiltychange::previousSection -> ' + NavigationMap.previousSection);
        debug('_handleVisibiltychange::currentSection -> ' + NavigationMap.currentSection);
        if (!document.hidden) {
          if (NavigationMap.currentSection !== curPanleId) {
            this.dispatchDialogShowEvent();
          } else {
            this.setCursorForElement(document.activeElement);
          }
        }
      },
      initSoftkey: function(submitable) {
        debug('initSoftkey::submitable -> ' + submitable);
        let softkeyParams = {
          menuClassName: 'menu-button',
          header: {
            l10nId: 'message'
          },
          items: [{
            name: 'Cancel',
            l10nId: 'cancel',
            priority: 1,
            method: function() {
              elements.resetButton.click();
            }
          }]
        };

        let saveItem = {
          name: 'Save',
          l10nId: 'save',
          priority: 3,
          method: function() {
            elements.submitButton.click();
          }
        };

        if (submitable) {
          softkeyParams.items.push(saveItem);
        }
        Array.prototype.sort.call(softkeyParams.items, (item1, item2) => {
          return item1.priority - item2.priority;
        });

        SettingsSoftkey.init(softkeyParams);
        SettingsSoftkey.show();
      },
      keydownHandler: function(evt) {
        debug('keydownHandler key -> ' + evt.key +
              ' currentPanel -> ' + Settings.currentPanel);
        switch (evt.key) {
          case 'ArrowUp':
          case 'ArrowDown':
          case 'Enter':
            let input = document.querySelector(curPanleId + ' li.focus input');
            if (input) {
              let cursorPos = input.value.length;
              input.focus();
              input.setSelectionRange(cursorPos, cursorPos);
            }
            break;
          case 'Backspace':
            elements.resetButton.click();
            break;
          default:
            break;
        }
      },
      setCursorForElement(element) {
        if ('INPUT' === element.nodeName) {
          let selectionStart = element.selectionStart;
          let selectionEnd = element.selectionEnd;
          debug('setCursorForElement::value -> ' + element.value);
          debug('setCursorForElement::selectionStart -> ' + selectionStart);
          debug('setCursorForElement::selectionEnd -> ' + selectionEnd);

          setTimeout(() => {
            element.focus();
            element.setSelectionRange(selectionStart, selectionEnd);
          }, DELAYED_INIT_TIMEOUT);
        }
      },
      onInit: function(panel) {
        debug('onInit');
        elements = {
          panel: panel,
          channelIndexInput: panel.querySelector('.channel-index'),
          channelTitleInput: panel.querySelector('.channel-title'),
          resetButton: panel.querySelector('button[type=reset]'),
          submitButton: panel.querySelector('button[type=submit]'),
        };
        this.submitable = true;
        this.handleVisibiltychange = this._handleVisibiltychange.bind(this);
        this.boundInputsChange = this.checkChannelInputs.bind(this);
      },
      onBeforeShow: function(panel, options) {
        debug('onBeforeShow::panel -> ' + panel.id);
        debug('onBeforeShow::options -> ' + JSON.stringify(options));
        this.initpanelready();
        this.dispatchDialogShowEvent();
        let header = document.querySelector(curPanleId + ' gaia-header h1');

        if ('add-channel' === options.type) {
          header.setAttribute('data-l10n-id', 'add-cell-channel');
          elements.channelTitleInput.value = 'CB channel';
          elements.channelIndexInput.value = '';
        }

        if ('edit-channel' === options.type) {
          header.setAttribute('data-l10n-id', 'edit-cell-channel');
          elements.channelTitleInput.value = options.channelName;
          elements.channelIndexInput.value = options.channelIndex;
        }

        let liNodes = document.querySelectorAll('#'+ panel.id + ' li.focus');
        Array.prototype.forEach.call(liNodes, (liNode) => {
          liNode.removeAttribute('class');
        });
        let cursorPos = elements.channelIndexInput.value.length;
        elements.channelIndexInput.focus();
        elements.channelIndexInput.parentNode.setAttribute('class', 'focus');
        elements.channelIndexInput.setSelectionRange(0, cursorPos);

        window.addEventListener('keydown', this.keydownHandler);
        document.addEventListener('visibilitychange',
          this.handleVisibiltychange);
        elements.channelIndexInput.addEventListener('input',
          this.boundInputsChange);
        elements.channelTitleInput.addEventListener('input',
          this.boundInputsChange);
        this.checkChannelInputs();
      },
      onShow: function() {
        debug('onShow');
        let cursorPos = elements.channelIndexInput.value.length;
        elements.channelIndexInput.focus();
        elements.channelIndexInput.setSelectionRange(0, cursorPos);
      },
      onBeforeHide: function() {
        debug('onBeforeHide');
        this.dispatchDialogHideEvent();
        SettingsSoftkey.hide();

        window.removeEventListener('keydown', this.keydownHandler);
        document.removeEventListener('visibilitychange',
          this.handleVisibiltychange);
        elements.channelIndexInput.removeEventListener('input',
          this.boundInputsChange);
        elements.channelTitleInput.removeEventListener('input',
          this.boundInputsChange);
      },
      onSubmit: function() {
        debug('onSubmit');
        if (this.submitable) {
          return Promise.resolve({
            channelName: elements.channelTitleInput.value,
            channelIndex: elements.channelIndexInput.value
          });
        } else {
          return Promise.reject();
        }
      },
      onCancel: function() {
        debug('onCancel');
        return Promise.reject();
      },
      checkChannelInputs: function() {
        let reg = /(^\d+[-]\d+$|^\d+$)/gi;
        let regExp = new RegExp(reg);
        let titleEle = elements.channelTitleInput;
        let inputEle = elements.channelIndexInput;
        let iParams = inputEle.value;
        let tParams = titleEle.value;
        let matchRegExp = regExp.test(iParams);

        if (iParams === '' && tParams === '') {
          this.submitable = false;
        } else {
          if (matchRegExp) {
            if (iParams.indexOf('-') > 0) {
              let limitRange = iParams.split('-');
              let minBound = parseInt(limitRange[0]);
              let maxBound = parseInt(limitRange[1]);
              if (minBound < maxBound) {
                let minMatch = (0 <= minBound && minBound < 65536);
                let maxMatch = (0 <= maxBound && maxBound < 65536);
                if (minMatch && maxMatch) {
                  this.submitable = true;
                } else {
                  this.submitable = false;
                }
              } else {
                this.submitable = false;
              }
            } else {
              let inputValue = parseInt(iParams);
              if (0 <= inputValue && inputValue < 65536) {
                this.submitable = true;
              } else {
                this.submitable = false;
              }
            }
          } else {
            this.submitable = false;
          }
        }

        if (!this.submitable) { // input:invalid:focus
          inputEle.style.boxShadow = 'inset 0 -0.1rem 0 #820000';
          inputEle.style.borderBottomColor = '#820000';
          inputEle.style.color = '#b90000';
        } else {
          inputEle.style.boxShadow = '';
          inputEle.style.borderBottomColor = '';
          inputEle.style.color = '';
        }

        this.initSoftkey(this.submitable);
      }
    });
  };
});
