/* global SettingsSoftkey, ConfirmDialogHelper, Settings, NavigationMap */

/**
 * Used to show CB message configuration panel
 */
define(['require','modules/settings_panel','modules/dialog_service','modules/settings_service','panels/cell_broadcast_message/cell_broadcast_utils'],function(require) {
  

  let SettingsPanel = require('modules/settings_panel');
  let DialogService = require('modules/dialog_service');
  let SettingsService = require('modules/settings_service');
  let UtilsHelper =
    require('panels/cell_broadcast_message/cell_broadcast_utils');

  return function ctor_emergency_alert_panel() {
    let DEBUG = true;
    let FileName = '<!-- CBTAG:: cell_channels_config.js -->';
    let curPanleId = '#cell-channels-config';
    let filtRules = '#cell-channels-config li';
    let filterEmptyListRule = '#cell-channels-config > #empty-list';

    function debug() {
      if (DEBUG) {
        console.log('[' + FileName + '] ' + Array.slice(arguments).concat());
      } else if (window.DUMP) {
        DUMP('[' + FileName + '] ' + Array.slice(arguments).concat());
      }
    }

    let aSoftkeyParams = {
      menuClassName: 'menu-button',
      header: { l10nId: 'options' },
      items: [
        {
          name: 'Add channel',
          l10nId: 'add-channel',
          priority: 1,
          method: function () {
            /*
            * DialogService.show('custom-handle-channels');
            * SettingsService.navigate('custom-handle-channels');
            * */

            dispalyEmptyContainer(false);
            let focusedEle = document.querySelector(curPanleId + ' li.focus');
            let curFocusedChannel = UtilsHelper.getCurChannel(focusedEle);
            DialogService.show('custom-handle-channels', {
              type: 'add-channel',
              simLots: 0,
              enabled: true,
              channelIndex: '50-60',
              channelName: 'CB channel',
            }).then((result) => {
              let type = result.type;
              let value = result.value;
              debug('Add channel::return value -> ' + JSON.stringify(value));
              if ('submit' === type) {
                saveAndRedrawChannel(curFocusedChannel, value);
              } else {
                reAddEventListener(curFocusedChannel);
              }
            }, (errResult) => {
                reAddEventListener(curFocusedChannel);
            });
          }
        }
      ]
    };

    let cSoftkeyParams = {
      menuClassName: 'menu-button',
      header: { l10nId: 'options' },
      items: [
        {
          name: 'Edit channel',
          l10nId: 'edit-channel',
          priority: 5,
          method: function () {
            let focusedEle = document.querySelector(curPanleId + ' li.focus');
            let curFocusedChannel = UtilsHelper.getCurChannel(focusedEle);
            DialogService.show('custom-handle-channels', {
              type: 'edit-channel',
              simLots:
                curFocusedChannel.simLots ? curFocusedChannel.simLots : 0,
              enabled:
                curFocusedChannel.enabled ? curFocusedChannel.enabled : true,
              channelIndex:
                curFocusedChannel.channelId ?
                  curFocusedChannel.channelId : '50-60',
              channelName: curFocusedChannel.channelName ?
                curFocusedChannel.channelName : 'CB channel',
            }).then((result) => {
              let type = result.type;
              let value = result.value;
              debug('Edit channel::result value -> ' + JSON.stringify(value));
              if ('submit' === type) {
                let oChannel = UtilsHelper.copy(curFocusedChannel);
                let nChannel = UtilsHelper.copy(curFocusedChannel);
                nChannel.channelId = value.channelIndex;
                nChannel.channelName = value.channelName;
                debug('Edit channel::oChannel -> ' + JSON.stringify(oChannel));
                debug('Edit channel::nChannel -> ' + JSON.stringify(nChannel));
                editAndRedrawChannel(oChannel, nChannel);
              } else {
                reAddEventListener(curFocusedChannel);
              }
            }, (errResult) => {
              reAddEventListener(curFocusedChannel);
            });
          }
        },
        {
          name: 'Delete channel',
          l10nId: 'delete-channel',
          priority: 6,
          method: function () {
            showConfirmDialog();
          }
        }
      ]
    };

    let deselectSoftkeyParams = {
      menuClassName: 'menu-button',
      header: {
        l10nId: 'options'
      },
      items: [{
        name: 'Deselect',
        l10nId: 'deselect',
        priority: 2,
        method: function() {
          setTimeout(UtilsHelper.clickCurChannel);
        }
      }]
    };
    let selectSoftkeyParams = {
      menuClassName: 'menu-button',
      header: {
        l10nId: 'options'
      },
      items: [{
        name: 'Select',
        l10nId: 'select',
        priority: 2,
        method: function () {
          setTimeout(UtilsHelper.clickCurChannel);
        }
      }]
    };

    function showConfirmDialog() {
      let dialogConfig = {
        title: {id: 'cell-channels-delete', args: {}},
        body: {id: 'cell-channels-confirm', args: {}},
        cancel: {
          name: 'Cancel',
          l10nId: 'cancel',
          priority: 1,
          callback: function() {
            dialog.destroy();
          }
        },
        confirm: {
          name: 'Delete',
          l10nId: 'delete',
          priority: 3,
          callback: function() {
            dialog.destroy();
            deleteAndRedrawChannel();
          }
        }
      };

      let dialog = new ConfirmDialogHelper(dialogConfig);
      dialog.show(document.getElementById('app-confirmation-dialog'));
    }

    function genNewSoftkeyParams(softkeyParams, newSoftkey) {
      let newSoftkeyParams = UtilsHelper.copy(softkeyParams);
      if (!newSoftkey.items) {
        return softkeyParams;
      }

      newSoftkeyParams.items = newSoftkeyParams.items.concat(newSoftkey.items);
      Array.prototype.sort.call(newSoftkeyParams.items, (item1, item2) => {
        return item1.priority - item2.priority;
      });

      return newSoftkeyParams;
    }

    function saveAndRedrawChannel(curFocusedChannel, channel) {
      if (channel && channel.channelIndex) {
        let simLots = UtilsHelper.curSimlot();
        let channelItem = {
          'simLots': simLots,
          'channelType': 'custom',
          'channelName': channel.channelName,
          'channelId': channel.channelIndex,
        };

        UtilsHelper.saveChannel(curPanleId, curFocusedChannel, channelItem)
          .then((result) => {
          if (result && result.allow) {
            debug('saveAndRedrawChannel::save channel success...');
          } else {
            debug('saveAndRedrawChannel::save channel failure...');
          }
          reAddEventListener(result.channel);
        });
      }
    }

    function editAndRedrawChannel(oChannel, nChannel) {
      UtilsHelper.editChannel(oChannel, nChannel).then((result) => {
        if (result && result.allow) {
          debug('editAndRedrawChannel::edit channel success...');
        } else {
          debug('editAndRedrawChannel::edit channel failure...');
        }
        reAddEventListener(result.channel);
      });
    }

    function setFocus(channel) {
      debug('setFocus::currentPanel -> ' + Settings.currentPanel);
      if ('#cell-channels-config' !== Settings.currentPanel) {
        return;
      }

      let node = null;
      if (channel && channel.channelId) {
        node = document.querySelector(curPanleId +
          ' li[data-channel-id="' + channel.channelId + '"]');
      }

      if (!node) {
        node = document.querySelector(curPanleId + ' ul > li:first-child');
      }
      debug('setFocus::node -> ' + node);

      if (node) {
        let focused = document.querySelectorAll('.focus');
        Array.prototype.forEach.call(focused, (focus) => {
          focus.classList.remove('focus');
        });
        debug('setFocus::focused -> ' + focused);

        node.setAttribute('tabindex', 1);
        node.classList.add('focus');
        node.focus();
        NavigationMap.scrollToElement(node);
      }
    }

    function deleteAndRedrawChannel() {
      UtilsHelper.deleteChannel().then((result) => {
        debug('deleteAndRedrawChannel::delete channel success...');
        reAddEventListener(result);
      });
    }

    function dispalyEmptyContainer(display) {
      let emptyListNode = document.querySelector(filterEmptyListRule);
      let cbChannelListElements = document.querySelectorAll(filtRules);
      debug('updateSoftkey::channelList -> ' + cbChannelListElements.length);
      if (display !== undefined) {
        emptyListNode.hidden = !display;
      } else {
        if (!cbChannelListElements.length) {
          emptyListNode.hidden = false;
        } else {
          emptyListNode.hidden = true;
        }
      }
    }

    function updateSoftkey(evt) {
      debug('updateSoftkey... ' + Settings.currentPanel);
      if (evt && evt.type) {
        debug('updateSoftkey type => ' + evt.type);
      }

      dispalyEmptyContainer();
      if (evt && '#cell-channels-config' !== Settings.currentPanel) {
        return;
      }

      let bdom = document.querySelector(curPanleId + ' li.focus bdi');
      let options = null;
      if (bdom && bdom.getAttribute('data-l10n-args')) {
        options = JSON.parse(bdom.getAttribute('data-l10n-args'));
      }

      if (evt && evt.target && 'change' === evt.type &&
        'INPUT' === evt.target.nodeName) {
        let idom = document.querySelector(curPanleId + ' li.focus input');
        idom.checked = evt.target.checked;
        idom.setAttribute('checked', evt.target.checked);
        options.enabled = evt.target.checked;
      }
      let focusedElement = document.querySelector('.focus');
      if (focusedElement && focusedElement.classList &&
        focusedElement.classList.contains('none-select')) {
        SettingsSoftkey.init(aSoftkeyParams);
        SettingsSoftkey.show();
        return;
      }

      debug('updateSoftkey::options -> ' + JSON.stringify(options));
      let softkeyParams = UtilsHelper.copy(aSoftkeyParams);
      switch (options && options.channelType) {
        case 'custom':
          softkeyParams.items = softkeyParams.items.concat(cSoftkeyParams.items);
          break;
        case 'preset':
          break;
        default:
          break;
      }

      if (options) {
        if (focusedElement && focusedElement.querySelector('input') &&
          focusedElement.querySelector('input').checked) {
          debug('updateSoftkey::deselectSoftkeyParams ');
          SettingsSoftkey.init(
            genNewSoftkeyParams(softkeyParams, deselectSoftkeyParams));
        } else {
          debug('updateSoftkey::selectSoftkeyParams ');
          SettingsSoftkey.init(
            genNewSoftkeyParams(softkeyParams, selectSoftkeyParams));
        }
      } else {
        SettingsSoftkey.init(softkeyParams);
      }
      SettingsSoftkey.show();
    }

    function reAddEventListener(channel) {
      return new Promise((resolve) => {
        debug('reAddEventListener...');
        if (channel) {
          debug('reAddEventListener::channel -> ' + JSON.stringify(channel));
        }

        try {
          NavigationMap.refresh();
        } catch (err) {
          console.error(err);
        } finally {
          reRemoveChannelListEventListener();
          reAddChannelListEventListener();
          setTimeout(() => {
            setFocus(channel);
            updateSoftkey();
            resolve();
          }, 200);
        }
      });
    }

    function reAddChannelListEventListener() {
      let listElements = document.querySelectorAll(filtRules);
      if (listElements) {
        UtilsHelper.addEventListener(listElements, updateSoftkey);
      }
    }

    function reRemoveChannelListEventListener() {
      let listElements = document.querySelectorAll(filtRules);
      if (listElements) {
        UtilsHelper.removeEventListener(listElements, updateSoftkey);
      }
    }

    return SettingsPanel({
      dialogPanelShow: false,

      _initAll: function(e) {
        debug('_initAll...');
        this.dialogPanelShow = false;
        let dialogpanel = (e && e.detail && e.detail.dialogpanel) ?
          e.detail.dialogpanel : '#custom-handle-channels';
        this._initpanelready(dialogpanel);
        window.addEventListener('keydown', this.handleKeydown);
      },

      _handleDialogShow: function() {
        debug('_handleDialogShow...');
        this.dialogPanelShow = true;
        window.removeEventListener('keydown', this.handleKeydown);
      },

      handleKeydown: function(e) {
        switch (e.key) {
          case 'BrowserBack':
          case 'Backspace':
            let openMenu = SettingsSoftkey.menuVisible();
            debug('handleKeydown::menuVisible -> ' + openMenu);
            debug('handleKeydown::currentPanel -> ' + Settings.currentPanel);
            debug('handleKeydown::previousSection -> ' +
              NavigationMap.previousSection);
            debug('handleKeydown::currentSection -> ' +
              NavigationMap.currentSection);
            if (!openMenu &&
              '#cell-channels-config' === Settings.currentPanel) {
              Settings.currentPanel = '#cell-broadcast-message';
            }
            break;
          default:
            break;
        }
      },

      _initpanelready: function(dialogpanel) {
        let evt = new CustomEvent('panelready', {
          detail: {
            current: Settings.currentPanel,
            previous: dialogpanel
          }
        });
        window.dispatchEvent(evt);
      },

      onInit: function() {
        UtilsHelper.observe().then(() => {
          this.initAll = this._initAll.bind(this);
          this.handleDialogShow = this._handleDialogShow.bind(this);
          this.dialogPanelShow = false;
          window.addEventListener('update-channel-config', (evt) => {
            let cbChannel = evt.detail.focusChannel;
            UtilsHelper.showCbChannels(curPanleId, () => {
              reAddEventListener(cbChannel);
            });
          });
        });
      },

      onShow: function() {
        if ('#cell-channels-config' === NavigationMap.currentSection) {
          debug('onShow::dialogPanelShow -> ' + this.dialogPanelShow);
          let readDone = UtilsHelper.getReadDone();
          debug('onShow::readCellBroadcastListDone -> ' + readDone);
          if (!readDone) {
            UtilsHelper.showCbChannels(curPanleId, () => {
              UtilsHelper.setReadDone(true);
              reAddEventListener();
            });
          } else {
            reAddChannelListEventListener();
            updateSoftkey();
          }

          if (!this.dialogPanelShow) {
            window.addEventListener('keydown', this.handleKeydown);
            window.addEventListener('dialogpanelshow', this.handleDialogShow);
            window.addEventListener('dialogpanelhide', this.initAll);
          }
        }
      },

      onBeforeHide: function() {
        if ('#cell-channels-config' === NavigationMap.currentSection) {
          debug('onBeforeHide::dialogPanelShow -> ' + this.dialogPanelShow);
          reRemoveChannelListEventListener();
          if (!this.dialogPanelShow) {
            SettingsSoftkey.hide();
            window.removeEventListener('keydown', this.handleKeydown);
            window.removeEventListener('dialogpanelshow', this.handleDialogShow);
            window.removeEventListener('dialogpanelhide', this.initAll);
          }
        }
      }
    });
  };
});
