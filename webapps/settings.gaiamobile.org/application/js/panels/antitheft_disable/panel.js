define(['require','modules/settings_panel','modules/settings_service'],function(require) {
  
  var SettingsPanel = require('modules/settings_panel');
  var SettingsService = require('modules/settings_service');

  return function antitheft_disable_settings_panel() {
    var _isReset = false;
    var ANTITHEFT_KEY = 'antitheft.enabled';
    var _settings = window.navigator.mozSettings;
    var _currentEmail = '';
    var _passwordInput,
      _showPwdCheckbox;

    function _initSoftKey(enabledForgot) {
      var softkeyParams = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Next',
          l10nId: 'next',
          priority: 3,
          method: function() {
            _verifyPassword();
          }
        }]
      };

      if (enabledForgot) {
        softkeyParams.items.unshift({
          name: 'Forgot PWD',
          l10nId: 'antitheft-forgot-password',
          priority: 1,
          method: function() {
            _ForgotPassword();
          }
        });
      }

      SettingsSoftkey.init(softkeyParams);
      SettingsSoftkey.show();
    }

    function _factoryReset() {
      var power = navigator.mozPower;
      if (!power) {
        console.error('Cannot get mozPower');
        return;
      }

      if (!power.factoryReset) {
        console.error('Cannot invoke mozPower.factoryReset()');
        return;
      }

      power.factoryReset();
    }

    function _ForgotPassword() {
      var lock = _settings.createLock();
      var REST_PASSWORD_KEY = 'identity.fxaccounts.reset-password.url';
      var req = lock.get(REST_PASSWORD_KEY);

      req.onsuccess = () => {
        try {
          new MozActivity({
            name: 'view',
            data: {
              type: 'url',
              url: req.result[REST_PASSWORD_KEY]
            }
          });
        } catch (e) {
          console.error('Failed to create an reset password activity: ' + e);
        }
      };
    }

    function _addFocus() {
      _passwordInput.focus();
    }

    function _verifyPassword() {
      if (!navigator.onLine) {
        showToast('disable-antitheft-network-error');
        return;
      }

      FxAccountsIACHelper.signIn(_currentEmail, _passwordInput.value,
        _onVerifySuccess, _onVerifyFailed);
    }

    function _onVerifySuccess(e) {
      if (_isReset) {
        _factoryReset();
      }

      _setSwitch(false);
      SettingsService.navigate('root');
    }

    function _onVerifyFailed(e) {
      _showErrorResponse(e);
    }

    function _showErrorResponse(response) {
      var l10nId = null;

      if (response.error == 'INVALID_PASSWORD') {
        l10nId = 'disable-antitheft-password-error';
      } else {
        l10nId = 'disable-antitheft-server-error';
      }
      showToast(l10nId);
    }

    function _setShowPwd(evt) {
      if (evt.target.checked) {
        _passwordInput.setAttribute('type', 'text');
      } else {
        _passwordInput.setAttribute('type', 'password');
      }
    }

    function _setSwitch(enabled) {
      var option = {};
      var lock = _settings.createLock();
      option[ANTITHEFT_KEY] = enabled;
      lock.set(option);
    }

    function _initAccount() {
      FxAccountsIACHelper.getAccounts(e => {
        var item = document.getElementById('current-account');
        if (e && e.verified) {
          _currentEmail = (e.email || e.accountId);
          item.innerHTML = _currentEmail;
        }
      });
    }

    function _initUI(panel, options) {
      _isReset = options.enabled || false;
      var header = document.getElementById('antitheft-disable-header');
      header.setAttribute('data-href', options.dataHref || '#root');
      var description = panel.querySelector('.description p');
      var l10nId = options.descriptions || 'disable-antitheft-warning';
      description.setAttribute('data-l10n-id', l10nId);
      _initSoftKey(_isReset);
    }

    return SettingsPanel({
      onInit: function(panel, options) {
        _passwordInput =
          document.getElementById('disable-antitheft-password-input');
        _showPwdCheckbox = panel.querySelector('input[type="checkbox"]');
        _initAccount();
      },

      onBeforeShow: function(panel, options) {
        _initUI(panel, options);
        _showPwdCheckbox.addEventListener('click', _setShowPwd);
        _passwordInput.parentNode.addEventListener('focus', _addFocus);
      },

      onBeforeHide: function() {
        SettingsSoftkey.hide();
        _showPwdCheckbox.removeEventListener('click', _setShowPwd);
        _passwordInput.parentNode.removeEventListener('focus', _addFocus);
      }
    });
  };
});
