define(['require','modules/settings_panel','modules/settings_service'],function(require){
  

  var SettingsPanel=require('modules/settings_panel');
  var SettingsService=require('modules/settings_service');


  return function ctor_iceinformation_edit_option(){
    var elements={};
    function _initSoftKey(){
      var softkeyParams={
        menuClassName:'menu-button',
        header:{
          l10nId:'message'
        },
        items:[{
          name:'Select',
          l10nId:'select',
          priority:2,
          method:function(){
            if(elements.panel.querySelector('.focus.create-icecontacts')){
              let activity = new MozActivity({ name: 'ice', data: { type: 'webcontacts/contact' } });
              activity.onsuccess = () =>{
                console.log("Call Create ICE completed");
              }
              activity.onerror = () => {
                console.warn('Activity error', activity.error.name);
              };
            }
          }
        }]
      };
      SettingsSoftkey.init(softkeyParams);
      SettingsSoftkey.show();
    }

    return SettingsPanel({
      onInit:function(panel){
        elements={
          panel:panel,
          //createIceContacts:panel.querySelector('.create-icecontacts')
        };
      },

      onBeforeShow:function(){
        _initSoftKey();
      },

      onBeforeHide:function(){
        SettingsSoftkey.hide();
      }
    });
  };
});
