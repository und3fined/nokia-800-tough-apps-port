define(['require','modules/settings_panel','shared/settings_listener','shared/airplane_mode_helper'],function(require) {
  

  var SettingsPanel = require('modules/settings_panel');
  var SettingsListener = require('shared/settings_listener');
  var AirplaneModeHelper = require('shared/airplane_mode_helper');
  var _currentSettingsValue = false;

  return function ctor_connectivity_settings() {
    var elements = {};
    var airplaneStatus, airplaneEnabled;

    function _initSoftKey() {
      var params = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };

      SettingsSoftkey.init(params);
      SettingsSoftkey.show();
    }

    function _keyEventHandler(evt) {
      switch (evt.key) {
        case 'Enter':
          var select = document.querySelector('li.focus select');
          select && select.focus();
          break;
        default:
      }
    }

    function _updateItemState(airplaneEnabled) {
      var validCardCount = 0;
      var _iccManager = window.navigator.mozIccManager;
      var _mobileConnections = window.navigator.mozMobileConnections;
      if (_mobileConnections) {
        [].forEach.call(_mobileConnections, (simcard, cardIndex) => {
          iccId = simcard.iccId;
          var icc = _iccManager.getIccById(iccId);
          if (icc !== null) {
            validCardCount++;
          }
        });
      }

      if (_mobileConnections) {
        var disabled = (validCardCount === 0 || false);
        elements.networkContainer.setAttribute('aria-disabled', disabled);
        if (disabled) {
          elements.networkHrefItem.removeAttribute('href');
          elements.networkContainer.classList.add('none-select');
        } else {
          elements.networkContainer.setAttribute('aria-disabled',
            airplaneEnabled);
          if (airplaneEnabled) {
            elements.networkHrefItem.removeAttribute('href');
            elements.networkContainer.classList.add('none-select');
          } else {
            elements.networkHrefItem.setAttribute('href', '#carrier');
            elements.networkContainer.classList.remove('none-select');
          }
        }
      }
    }

    function _onAPMStateChange(status) {
      if (status === 'enabled' || status === 'disabled') {
        elements.airplaneMenuItem.classList.remove('disabled');
      } else {
        elements.airplaneMenuItem.classList.add('disabled');
      }
    }

    function _onChangeAirplaneModeMenu(status) {
      var enabled =
        (status === 'enabled' || status === 'enabling') ? true : false;
      elements.airplaneMenuItem.setAttribute('aria-disabled', enabled);
      _updateItemState(enabled);

      if (status === 'enabled' || status === 'disabled') {
        elements.airplaneMenuItem.removeAttribute('aria-disabled');
      } else {
        elements.airplaneMenuItem.setAttribute('aria-disabled', true);
      }
    }

    function _setAPMStatus(evt) {
      var enabled = (evt.target.value === 'true') || false;
      if (_currentSettingsValue === enabled) {
        return;
      }
      showToast('changessaved');
      window.dispatchEvent(new CustomEvent('airplaneModeChange', {
        detail: {
          status: enabled ? 'enabling': 'disabling'
        }
      }));
    }

    return SettingsPanel({
      onInit: function(panel) {
        elements = {
          WifiSmallItem: panel.querySelector('#wifi-desc'),
          airplaneMenuItem: panel.querySelector('.airplane_mode_switch'),
          airplaneItem: panel.querySelector('.airplaneMode-select'),
          networkContainer: panel.querySelector('#data-connectivity'),
          networkHrefItem: panel.querySelector('#menuItem-mobileNetworkAndData')
        };

        elements.airplaneItem.hidden = true;
        AirplaneModeHelper.addEventListener('statechange', _onAPMStateChange);

        window.addEventListener('panelready', e => {
          switch (e.detail.current) {
            case '#wifi':
            case '#carrier':
              // If other APP enter "Settings configure activity" page,
              // we should back to "connectivity-settings" instead of "root" page.
              if (NavigationMap._optionsShow === false) {
                var header = document.querySelectorAll('.current [data-href]');
                header[0].setAttribute('data-href',
                  '#connectivity-settings');
              }
              break;
            default:
          }
        });

        DeviceFeature.ready(() => {
          document.getElementById('menuItem-wifi').parentNode.hidden =
            (DeviceFeature.getValue('wifi') !== 'true');
        });

        elements.airplaneMenuItem.hidden = false;
        elements.networkContainer.hidden = false;

        SettingsListener.observe('wifi.enabled', true, enabled => {
          var value = enabled ? 'on' : 'off';
          elements.WifiSmallItem.setAttribute('data-l10n-id', value);
        });

        SettingsListener.observe('airplaneMode.enabled', false, value => {
          elements.airplaneItem.hidden = false;
          _currentSettingsValue = value;
        });

        SettingsListener.observe('airplaneMode.status', false,
          _onChangeAirplaneModeMenu.bind(this));

        window.addEventListener('airplaneModeChange', e => {
          _onChangeAirplaneModeMenu(e.detail.status);
        });

        elements.airplaneItem.addEventListener('blur', _setAPMStatus);
      },

      onBeforeShow: function() {
        _initSoftKey();
        window.addEventListener('keydown', _keyEventHandler);
      },

      onBeforeHide: function() {
        SettingsSoftkey.hide();
      }
    });
  };
});
