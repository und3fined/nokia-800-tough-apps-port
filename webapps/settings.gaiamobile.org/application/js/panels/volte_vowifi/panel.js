 /* global DsdsSettings */
 /* global SettingsSoftkey */
define(['require','modules/settings_panel','modules/settings_service','modules/dialog_service'],function(require) {
  
  var SettingsPanel = require('modules/settings_panel');
  var SettingsService = require('modules/settings_service');
  //[BTS-126] BDC yuxin add for VoWiFi: Emergency Call Notifications should show to end useer when device is registered for WiFi Calling.begin
  var DialogService = require('modules/dialog_service');
  //[BTS-126] BDC yuxin add for VoWiFi: Emergency Call Notifications should show to end useer when device is registered for WiFi Calling.end

  //BDC zhangwp 20190416 add for IMS default configurations. begin
  const KEY_FIH_VOLTE_DEFAULT_ENABLE_BOOL = 'fih.volte.default.enable.bool';
  const KEY_FIH_VOLTE_EDITABLE_BOOL = 'fih.volte.editable.bool';
  const KEY_FIH_VOWIFI_DEFAULT_ENABLE_BOOL = 'fih.vowifi.default.enable.bool';
  const KEY_FIH_VOWIFI_EDITABLE_BOOL = 'fih.vowifi.editable.bool';
  //BDC zhangwp 20190416 add for IMS default configurations. end

  return function volte_settings_panel() {
    var elements = {};
    var _settings = navigator.mozSettings;
    var telephony = navigator.mozTelephony;
    var vowifiStatusItem = document.querySelector('#vowifi-status-desc');

    function _initSoftKey() {
      var softkeyParams = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };
      SettingsSoftkey.init(softkeyParams);
      SettingsSoftkey.show();
    }

    function _switchChange() {
      var obj = {};
      if (elements.volteSwitch.value === 'true' &&
        elements.vowifiSwitch.value === 'true') {
        obj['ril.ims.enabled'] = true;
        //[BTS-2396] BDC zhangwp 20190930 add for VF AU default call mode. begin
/*
        obj['ril.ims.preferredProfile'] = 'wifi-preferred';
*/
        if(_isCellularPreferred()) {
          obj['ril.ims.preferredProfile'] = 'cellular-preferred';
        } else {
          obj['ril.ims.preferredProfile'] = 'wifi-preferred';
        }
        //[BTS-2396] BDC zhangwp 20190930 add for VF AU default call mode. end
      } else if (elements.volteSwitch.value === 'true' &&
        elements.vowifiSwitch.value === 'false') {
        obj['ril.ims.enabled'] = true;
        obj['ril.ims.preferredProfile'] = 'cellular-only';
      } else if (elements.volteSwitch.value === 'false' &&
        elements.vowifiSwitch.value === 'true') {
        obj['ril.ims.enabled'] = true;
        obj['ril.ims.preferredProfile'] = 'wifi-only';
      } else {
        obj['ril.ims.enabled'] = false;
      }

      var result = _settings.createLock().set(obj);
      result.onsuccess = function () {
        showToast('changessaved');
        _updateDesc();
      };
      result.onerror = function () {
        console.error("Error: An error occure, can't set ims switch");
        _updateDesc();
        _updateUI();
      };
    }

    //[BTS-2396] BDC zhangwp 20190930 add for VF AU default call mode. begin
    var defaultDataMccMnc = '';
    function _isCellularPreferred() {
      console.log('volte_vowifi isCellularPreferred : defaultDataMccMnc: ' + defaultDataMccMnc);

      if(defaultDataMccMnc === '50503') {
        return true;
      }

      return false;
    }
    //[BTS-2396] BDC zhangwp 20190930 add for VF AU default call mode. end

    //[BTS-126] BDC yuxin add for VoWiFi: Emergency Call Notifications should show to end useer when device is registered for WiFi Calling.begin
    function _vowifiSwitchChange() {
      if (elements.vowifiSwitch.value === 'true') {
        getSetting('ril.data.defaultServiceId').then(function(result) {
          var serviceId = result;
          var missEccOpt = isWifiCallMissEcc(serviceId);
          if (missEccOpt) {
            var dialogConfig = {
              title: {id: 'wifiCallMissEccTitle', args: {operator: missEccOpt}},
              body: {id: 'wifiCallMissEccMsg', args: {}},
              cancel: {
                name: 'notNow',
                l10nId: 'notNow',
                priority: 1,
                callback: function() {
                  dialog.destroy();
                  elements.vowifiSwitch.value = 'false';
                }
              },
              confirm: {
                name: 'turnOn',
                l10nId: 'turnOn',
                priority: 3,
                callback: function() {
                  dialog.destroy();
                  _switchChange();
                }
              }
            };
            var dialog = new ConfirmDialogHelper(dialogConfig);
            dialog.show(document.getElementById('app-confirmation-dialog'));
          } else {
	          _switchChange();
          }
        });
      } else {
        _switchChange();
      }
    }

    function isWifiCallMissEcc(defaultDataId) {
      var missEccOpt = null;
      var mccmnc = null;
      if (defaultDataId !== false && defaultDataId >= 0) {
        var conn = navigator.mozMobileConnections[defaultDataId];
        if (conn && conn.voice && conn.voice.network && conn.voice.network.mcc) {
          mccmnc = conn.voice.network.mcc + conn.voice.network.mnc;
        }
        if (!mccmnc && conn.iccId){
          var icc = navigator.mozIccManager.getIccById(conn.iccId);
          mccmnc = icc && icc.iccInfo ? icc.iccInfo.mcc + icc.iccInfo.mnc : null;
        }
      }

      if (mccmnc) {
        if (mccmnc === '23003' || mccmnc === '23415' || mccmnc === '26202' || mccmnc === '26209') {
          missEccOpt = 'Vodafone';
        }
      }

      //[BTS-2396] BDC zhangwp 20190930 add for VF AU default call mode. begin
      if (conn.iccId){
        var icc = navigator.mozIccManager.getIccById(conn.iccId);
        defaultDataMccMnc = icc && icc.iccInfo ? icc.iccInfo.mcc + icc.iccInfo.mnc : null;
      }
      //[BTS-2396] BDC zhangwp 20190930 add for VF AU default call mode. end

      return missEccOpt;
    }
    //[BTS-126] BDC yuxin add for VoWiFi: Emergency Call Notifications should show to end useer when device is registered for WiFi Calling.end

    function getSetting(settingKey) {
      return new Promise(function (resolve, reject) {
        var transaction = _settings.createLock();
        var req = transaction.get(settingKey);
        req.onsuccess = function () {
          resolve(req.result[settingKey]);
        };
        req.onerror = function () {
          resolve(false);
        };
      });
    }

    function _updateDesc() {
      vowifiStatusItem.textContent = '';
      var imsEnabled = null;
      var imsProfile = null;

      var p1 = getSetting('ril.data.defaultServiceId');
      var p2 = getSetting('ril.ims.enabled');
      var p3 = getSetting('ril.ims.preferredProfile');
      Promise.all([p1, p2, p3]).then(function(values) {
        var serviceId = values[0];
        imsEnabled = values[1];
        imsProfile = values[2];
        var imsHandler = navigator.mozMobileConnections[serviceId].imsHandler;
        if (imsHandler) {
          AirplaneModeHelper.ready(function() {
            var status = AirplaneModeHelper.getStatus();
            if ((status === 'enabled')) {
              if (imsEnabled &&
                (imsProfile === 'wifi-preferred' ||
                imsProfile === 'wifi-only')) {
                if (imsHandler.unregisteredReason) {
                  //[BTS-2526] DBC yuxin modify for VHA/OA Issue VF034 - Itermittently device fails to register to VoWIFI in Airplane mode ON and give the error message as " Registration error".begin
                  //original code
                  //_updateVowifiDesc('errorMessage');
                  //vowifiStatusItem.hidden = false;
                  vowifiStatusItem.hidden = true;
                  //[BTS-2526] DBC yuxin modify for VHA/OA Issue VF034 - Itermittently device fails to register to VoWIFI in Airplane mode ON and give the error message as " Registration error".end
                } else {
                  _updateVowifiDesc('airplaneMode');
                  vowifiStatusItem.hidden = false;
                }
              } else {
                vowifiStatusItem.hidden = true;
              }
            } else {
              vowifiStatusItem.hidden = true;
              if (imsEnabled &&
                (imsProfile === 'wifi-preferred' ||
                imsProfile === 'wifi-only')) {
                if (imsHandler.unregisteredReason) {
                  //[BTS-2526] DBC yuxin modify for VHA/OA Issue VF034 - Itermittently device fails to register to VoWIFI in Airplane mode ON and give the error message as " Registration error".begin
                  //original code
                  //_updateVowifiDesc('errorMessage');
                  //vowifiStatusItem.hidden = false;
                  vowifiStatusItem.hidden = true;
                  //[BTS-2526] DBC yuxin modify for VHA/OA Issue VF034 - Itermittently device fails to register to VoWIFI in Airplane mode ON and give the error message as " Registration error".end
                }
              }
            }
          });
          if (telephony) {
            telephony.ontelephonycoveragelosing = (evt) => {
              _updateVowifiDesc('poorSignal');
            };
          }
        }
      });
    }

    function _updateVowifiDesc(status) {
      var l10n = 'volte-status-' + status;
      vowifiStatusItem.textContent = ' - ' + navigator.mozL10n.get(l10n);
    }

    function _updateUI() {
      let p1 = getSetting('ril.data.defaultServiceId');
      let p2 = getSetting('ril.ims.enabled');
      let p3 = getSetting('ril.ims.preferredProfile');
      Promise.all([p1, p2, p3]).then(function(values) {
        let serviceId = values[0];
        let imsEnabled = values[1];
        let imsProfile = values[2];

        //BDC zhangwp 20190416 add for IMS default configurations. begin
        let matchInfo = {
          "clientId": "0",
        };
        matchInfo.clientId = serviceId;

        console.log('volte_vowifi updateUI');

        let p4 = navigator.customization.getValueForCarrier(matchInfo, KEY_FIH_VOLTE_EDITABLE_BOOL);
        let p5 = navigator.customization.getValueForCarrier(matchInfo, KEY_FIH_VOWIFI_EDITABLE_BOOL);
        let p6 = navigator.customization.getValueForCarrier(matchInfo, KEY_FIH_VOLTE_DEFAULT_ENABLE_BOOL);
        let p7 = navigator.customization.getValueForCarrier(matchInfo, KEY_FIH_VOWIFI_DEFAULT_ENABLE_BOOL);

        Promise.all([p4, p5, p6, p7]).then(function(values) {
          let volteEditable = JSON.stringify(values[0]) === 'true' ? true : false;
          let vowifiEditable = JSON.stringify(values[1]) === 'true' ? true : false;
          let volteDefaultOn = JSON.stringify(values[2]) === 'true' ? true : false;
          let vowifiDefaultOn = JSON.stringify(values[3]) === 'true' ? true : false;
          let carrierVolteSupport = volteEditable || volteDefaultOn;
          let carrierVowifiSupport = vowifiEditable || vowifiDefaultOn;

          console.log('volte_vowifi updateUI : volteEditable: ' + volteEditable + " vowifiEditable: " + vowifiEditable);
          console.log('volte_vowifi updateUI : serviceId: ' + serviceId + ' imsEnabled: ' + imsEnabled + ' imsProfile: ' + imsProfile);
        //BDC zhangwp 20190416 add for IMS default configurations. end

        DeviceFeature.ready(() => {
          let isSupportWifi = DeviceFeature.getValue('wifi');
          let isSupportVowifi = DeviceFeature.getValue('voWifi');
          let isSupportVolte = DeviceFeature.getValue('voLte');
          let vowifiElement = elements.vowifiSwitch.parentNode.parentNode;
          let volteElement = elements.volteSwitch.parentNode.parentNode;
          let volteWifiH1 = document.getElementById('volte-vowifi-h1');
          let mobileConnection = navigator.mozMobileConnections[serviceId];
          let supportedBearers = mobileConnection.imsHandler &&
            mobileConnection.imsHandler.deviceConfig.supportedBearers;

          //[BTS-1960] BDC zhangwp 20190617 add for remove VoWiFi settings for SIM2. begin
          if(serviceId === 1) {
            console.log('volte_vowifi updateUI : remove VoWiFi settings for SIM2');
            vowifiEditable = false;
            vowifiElement.classList.add('hidden');
          }
          //[BTS-1960] BDC zhangwp 20190617 add for remove VoWiFi settings for SIM2. end

          if (supportedBearers) {
            //[BTS-1983] BDC zhangwp 20190618 modify for update IMS menu. begin
/*
            let supportWifi = (isSupportWifi === 'true') &&
              (isSupportVowifi === 'true') &&
              (supportedBearers.indexOf('wifi') >= 0);
            let supportLte = (isSupportVolte === 'true') &&
              supportedBearers.indexOf('cellular') >= 0;
*/
            let supportWifi = (isSupportWifi === 'true') &&
              (isSupportVowifi === 'true') &&
              carrierVowifiSupport &&
              (supportedBearers.indexOf('wifi') >= 0);
            let supportLte = (isSupportVolte === 'true') &&
              carrierVolteSupport &&
              supportedBearers.indexOf('cellular') >= 0;
            //[BTS-1983] BDC zhangwp 20190618 modify for update IMS menu. end

            if (supportWifi && supportLte) {
              //BDC zhangwp 20190416 add for IMS default configurations. begin
/*
              volteElement.classList.remove('hidden');
              vowifiElement.classList.remove('hidden');
              volteWifiH1.setAttribute('data-l10n-id', 'volte-header');
*/
              if(volteEditable) {
                volteElement.classList.remove('hidden');
              }
              if(vowifiEditable) {
                vowifiElement.classList.remove('hidden');
              }

              if(volteEditable && !vowifiEditable) {
                volteWifiH1.setAttribute('data-l10n-id', 'fih-volte-header');
              } else if(!volteEditable && vowifiEditable) {
                volteWifiH1.setAttribute('data-l10n-id', 'fih-vowifi-header');
              } else {
                volteWifiH1.setAttribute('data-l10n-id', 'volte-header');
              }
              //BDC zhangwp 20190416 add for IMS default configurations. end
              volteWifiH1.hidden = false;
            } else {
              if (supportWifi) {
                //BDC zhangwp 20190416 add for IMS default configurations. begin
/*
                vowifiElement.classList.remove('hidden');
*/
                if(vowifiEditable) {
                  vowifiElement.classList.remove('hidden');
                }
                //BDC zhangwp 20190416 add for IMS default configurations. end
              } else {
                vowifiElement.classList.add('hidden');
                //[BTS-2365] BDC zhangwp 20190723 modify for Dutch translation. begin
/*
                volteWifiH1.setAttribute('data-l10n-id', 'volte');
*/
                volteWifiH1.setAttribute('data-l10n-id', 'fih-volte-header');
                //[BTS-2365] BDC zhangwp 20190723 modify for Dutch translation. end

                volteWifiH1.hidden = false;
              };
              if (supportLte) {
                //BDC zhangwp 20190416 add for IMS default configurations. begin
/*
                volteElement.classList.remove('hidden');
*/
                if(volteEditable) {
                  volteElement.classList.remove('hidden');
                }
                //BDC zhangwp 20190416 add for IMS default configurations. end
              } else {
                volteElement.classList.add('hidden');
                //[BTS-2365] BDC zhangwp 20190723 modify for Dutch translation. begin
/*
                volteWifiH1.setAttribute('data-l10n-id', 'vowifi');
*/
                volteWifiH1.setAttribute('data-l10n-id', 'fih-vowifi-header');
                //[BTS-2365] BDC zhangwp 20190723 modify for Dutch translation. end

                volteWifiH1.hidden = false;
              };
            }
          }

          if (imsEnabled && (imsProfile === 'cellular-preferred' ||
            imsProfile === 'wifi-preferred')) {
            elements.volteSwitch.value = 'true';
            elements.vowifiSwitch.value = 'true';
          } else if (imsEnabled && imsProfile === 'cellular-only') {
            elements.volteSwitch.value = 'true';
            elements.vowifiSwitch.value = 'false';
          } else if (imsEnabled && imsProfile === 'wifi-only') {
            elements.volteSwitch.value = 'false';
            elements.vowifiSwitch.value = 'true';
          } else if (!imsEnabled) {
            //BDC zhangwp 20190416 add for IMS default configurations. begin
/*
            elements.volteSwitch.value = 'false';
            elements.vowifiSwitch.value = 'false';
*/
            if(!volteEditable && volteDefaultOn) {
              elements.volteSwitch.value = 'true';
            } else {
              elements.volteSwitch.value = 'false';
            }

            if(!vowifiEditable && vowifiDefaultOn) {
              elements.vowifiSwitch.value = 'true';
            } else {
              elements.vowifiSwitch.value = 'false';
            }
            //BDC zhangwp 20190416 add for IMS default configurations. end
          }
          window.dispatchEvent(new CustomEvent('refresh'));
        });

        //BDC zhangwp 20190416 add for IMS default configurations. begin
        });
        //BDC zhangwp 20190416 add for IMS default configurations. end

      });
    }

    return SettingsPanel({
      onInit: function(panel) {
        elements = {
          volteSwitch: document.getElementById('volte-switch'),
          vowifiSwitch: document.getElementById('vowifi-switch')
        };
      },

      onBeforeShow: function() {
        _updateDesc();
        _updateUI();
        elements.volteSwitch.addEventListener('change', _switchChange);
        //[BTS-126] BDC yuxin add for VoWiFi: Emergency Call Notifications should show to end useer when device is registered for WiFi Calling.begin
        //original code
        //elements.vowifiSwitch.addEventListener('change', _switchChange);
        elements.vowifiSwitch.addEventListener('change', _vowifiSwitchChange);
        //[BTS-126] BDC yuxin add for VoWiFi: Emergency Call Notifications should show to end useer when device is registered for WiFi Calling.end
      },

      onBeforeHide: function() {
        elements.volteSwitch.addEventListener('change', _switchChange);
        //[BTS-126] BDC yuxin add for VoWiFi: Emergency Call Notifications should show to end useer when device is registered for WiFi Calling.begin
        //original code
        //elements.vowifiSwitch.addEventListener('change', _switchChange);
        elements.vowifiSwitch.addEventListener('change', _vowifiSwitchChange);
        //[BTS-126] BDC yuxin add for VoWiFi: Emergency Call Notifications should show to end useer when device is registered for WiFi Calling.end
      },

      onShow: function() {
        _initSoftKey();
      },

      onHide: function() {
        SettingsSoftkey.hide();
      }
    });
  };
});
