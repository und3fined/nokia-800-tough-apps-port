define(['require','modules/settings_panel','modules/settings_service'],function(require){
  

  var SettingsPanel=require('modules/settings_panel');
  var SettingsService=require('modules/settings_service');


  return function ctor_iceinformation_user(){
    var elements={};
    function _initSoftKey(){
      var softkeyParams={
        menuClassName:'menu-button',
        header:{
          l10nId:'message'
        },
        items:[{
          name:'Select',
          l10nId:'select',
          priority:2,
          method:function(){
            if(elements.panel.querySelector('.focus.delete')){
              _deleteConfirmDialog();
            }
            if(elements.panel.querySelector('.focus.create-icecontacts')){
              let activity = new MozActivity({ name: 'ice', data: { type: 'webcontacts/contact' } });
              activity.onsuccess = () =>{
                console.log("Call Create ICE completed");
              }
              activity.onerror = () => {
                console.warn('Activity error', activity.error.name);
              };
            }

            if(elements.panel.querySelector('.focus.share-via-sms')){
               var settings = {};
               settings['iceinformation.share']=true;
               navigator.mozSettings.createLock().set(settings);
               new MozActivity({
                   name: 'new',
                   data: {
                   type: 'websms/sms'
                   }
               });
            }
          }
        }]
      };
      SettingsSoftkey.init(softkeyParams);
      SettingsSoftkey.show();
    }

    function _hideElms(panel) {
      // Hide some elements if it is in ftu process
      // JWJ 2019-05-02 Bug fixing BTS-1256 --Start
      // dataset will convert the value to string that means dataset.ftu will be "true" if it is set in activity_handler
      // and it will be "false" after set in iceinformation panel. Otherwise it is undefined.
      if (panel.dataset.ftu == 'true') {
      // JWJ 2019-05-02 Bug fixing BTS-1256 --End
        elements.edit.hidden = true;
        elements.shareViaSms.hidden = true;
        elements.delete.hidden = true;
      }
    }

    function _deleteConfirmDialog(){
      var dialogConfig={
        title:{
          id:'confirmation',
          args:{}
        },
        body:{
          id:'qtn_sm_dial_delete_ICE_confirm',
          args:{}
        },
        cancel:{
          name:'Cancel',
          l10nId:'cancel',
          priority:1,
          callback:function(){
            dialog.destroy();
          }
        },
        confirm:{
          name:'Yes',
          l10nId:'yes',
          priority:3,
          callback:function(){
            localStorage.firstname='';
            localStorage.lastname='';
            localStorage.condition='';
            localStorage.allergies='';
        localStorage.vaccination='';
        localStorage.medication='';
        localStorage.birthday='';
        localStorage.height='';
        localStorage.weight='';
        localStorage.address='';
        var settings = {};
        settings['iceinformation.user']= '';
        navigator.mozSettings.createLock().set(settings);
            dialog.destroy();
          }
        },
      };

      var dialog=new ConfirmDialogHelper(dialogConfig);
      dialog.show(document.getElementById('app-confirmation-dialog'));
    }

    return SettingsPanel({
      onInit:function(panel){
        elements={
          panel:panel,
          edit:panel.querySelector('.edit'),
          shareViaSms:panel.querySelector('.share-via-sms'),
          delete:panel.querySelector('.delete'),
          createIceContacts:panel.querySelector('.create-icecontacts')
        };
      },

      onBeforeShow:function(panel){
        _initSoftKey();
        _hideElms(panel);
        //MockNavigatormozSetMessageHandler.mSetup();
        elements.edit.addEventListener('click',(evt)=>{
          SettingsService.navigate('iceinformation');
        });
      },

      onBeforeHide:function(panel){
        elements.edit.removeEventListener('click',(evt)=>{
          SettingsService.navigate('iceinformation');
        });
        SettingsSoftkey.hide();
      }
    });
  };
});
