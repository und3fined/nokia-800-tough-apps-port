define(['require','modules/settings_cache','modules/dialog_service','dsds_settings','shared/settings_helper','shared/simslot_manager'],function(require) {
  

  /**
   * Singleton object that handles some call settings.
   */
  var SettingsCache = require('modules/settings_cache');
  var DialogService = require('modules/dialog_service');
  var DsdsSettings = require('dsds_settings');
  var SettingsHelper = require('shared/settings_helper');

  //[CNT-660] BDC zhangwp add for hide DTMF menu. begin
  var SIMSlotManager = require('shared/simslot_manager');
  //[CNT-660] BDC zhangwp add for hide DTMF menu. end

  var CallSettings = function cs_callsettings() {
    var _networkTypeCategory = {
      'gprs': 'gsm',
      'edge': 'gsm',
      'umts': 'gsm',
      'hsdpa': 'gsm',
      'hsupa': 'gsm',
      'hspa': 'gsm',
      'hspa+': 'gsm',
      'lte': 'gsm',
      'gsm': 'gsm',
      'is95a': 'cdma',
      'is95b': 'cdma',
      '1xrtt': 'cdma',
      'evdo0': 'cdma',
      'evdoa': 'cdma',
      'evdob': 'cdma',
      'ehrpd': 'cdma'
    };

    var _clirConstantsMapping = {
      'CLIR_DEFAULT': 0,
      'CLIR_INVOCATION': 1,
      'CLIR_SUPPRESSION': 2
    };

    var _settings = window.navigator.mozSettings;
    var _mobileConnections = window.navigator.mozMobileConnections;
    var _voiceTypes = Array.prototype.map.call(_mobileConnections,
      function() { return null; });

    /** mozMobileConnection instance the panel settings rely on */
    var _mobileConnection = null;
    var _imsHandler = null;
    /**
      * To prevent current panle frequently refresh
      *   while the voice type is changing.
      *
      * @param {String} 'cdma' or 'gsm'
      */
    var _currentType = null;
    /** Task scheduler */
    var _taskScheduler = null;

    /** @HACK If devices is querying Call Waiting status,
      * should disable the select option.
      */
    var _callWaitingQueryStatus = false;

    /**
     * Init function.
     */
    function cs_init() {
      // Get the mozMobileConnection instace for this ICC card.
      _mobileConnection = _mobileConnections[
        DsdsSettings.getIccCardIndexForCallSettings()
      ];
      if (!_mobileConnection) {
        return;
      }

      _taskScheduler = TaskScheduler();

      cs_checkNetworkType();
      // Init call setting stuff.
      cs_initVoicePrivacyMode();
      cs_initCallWaiting();
      cs_initCallerId();
      cs_initFdnItem();

      // Update items in the call settings panel.
      window.addEventListener('panelready', function(e) {
        // Get the mozMobileConnection instace for this ICC card.
        _mobileConnection = _mobileConnections[
          DsdsSettings.getIccCardIndexForCallSettings()
        ];
        if (!_mobileConnection) {
          return;
        }

        switch (e.detail.current) {
          case '#call':
            // No need to refresh the call settings items if navigated from
            // panels not manipulating call settings.
            if (e.detail.previous === '#call-cfSettings' ||
                e.detail.previous === '#call-cbSettings') {
              return;
            }

            cs_updateNetworkTypeLimitedItemsVisibility(_currentType);
            cs_refreshCallSettingItems();
            break;
        }
      });

      // We need to refresh call setting items as they can be changed in dialer.
      document.addEventListener('visibilitychange', function() {
        if (document.hidden) {
          return;
        }

        switch (Settings.currentPanel) {
          case '#call':
            cs_updateNetworkTypeLimitedItemsVisibility(_currentType);
            cs_refreshCallSettingItems();
            break;
        }
      });
    }

    function cs_initFdnItem() {
      var iccObj = getIccByIndex();
      var fdnMenuItem = document.getElementById('menuItem-callFdn');
      iccObj.getServiceState('fdn').then(function(hasFdn) {
        fdnMenuItem.hidden = !hasFdn;
      });
      /*[BTS-206] change the FDN display start*/
      navigator.customization.getValue('fih.key.hide.fdn').then((result) => {
          if (result) {
              fdnMenuItem.hidden = true;
          }
      });
      /*[BTS-206] change the FDN display end*/
    }

    function cs_checkNetworkType() {
      var voice = _mobileConnection.voice;
      var data = _mobileConnection.data;
      _imsHandler = _mobileConnection.imsHandler;
      if (_imsHandler &&
        (_imsHandler.capability === 'voice-over-wifi' ||
        _imsHandler.capability === 'video-over-wifi')) {
        _currentType = _imsHandler.capability;
        cs_updateNetworkTypeLimitedItemsVisibility(_currentType);
      } else if (voice && voice.state === 'registered' &&
        voice.connected === true) {
        _currentType = _networkTypeCategory[voice.type];
        cs_updateNetworkTypeLimitedItemsVisibility(_currentType);
      } else if (data && data.state === 'registered' &&
        data.connected === true) {
        _currentType = _networkTypeCategory[data.type];
        cs_updateNetworkTypeLimitedItemsVisibility(_currentType);
      }
    }

    function cs_addNetworkTypeCheckListener() {
      if (_mobileConnection.voice) {
        _mobileConnection.addEventListener('voicechange', cs_onTypeChange);
      }
      if (_mobileConnection.data) {
        _mobileConnection.addEventListener('datachange', cs_onTypeChange);
      }
      if (_imsHandler) {
        _imsHandler.addEventListener('capabilitychange',
          cs_onCapabilityChange);
      }
    }

    function cs_onTypeChange(evt) {
      var voiceType = _mobileConnection.voice && _mobileConnection.voice.type;
      var dataType = _mobileConnection.data && _mobileConnection.data.type;
      if (!voiceType && !dataType) {
        return;
      }
      var newType = _networkTypeCategory[voiceType || dataType];
      if (newType === _currentType) {
        return;
      } else {
        _currentType = newType;
      }
      cs_updateNetworkTypeLimitedItemsVisibility(newType);
      cs_refreshCallSettingItems();
    }

    function cs_onCapabilityChange() {
      if (_imsHandler.capability === 'voice-over-wifi' ||
        _imsHandler.capability === 'video-over-wifi') {
        if (_imsHandler.capability === _currentType) {
          return;
        } else {
          _currentType = newType;
        }
        cs_updateNetworkTypeLimitedItemsVisibility(_imsHandler.capability);
        cs_refreshCallSettingItems();
      }
    }
    //[BTS-2491] BDC matf remove call barring item for 50503. begin
    function cs_updateCallBarringItemsVisibility(mcc,mnc) {
     if (!mcc || !mnc) {
      return false;
    }
    console.log("cs_updateCallBarringItemsVisibility mcc = "+mcc+" ; mnc ="+mnc);
    if (mcc == '505' && mnc == '03') {
      return true;
    }else {
      return false;
    }
  }
    //[BTS-2491] BDC matf remove call barring item for 50503. end
    /**
     * Update the network type limited items' visibility based on the
     * voice type or data type.
     */
    function cs_updateNetworkTypeLimitedItemsVisibility(newType) {
      // The following features are limited to GSM types.
      let callForwardingItem =
        document.getElementById('menuItem-callForwarding');
      let callBarringItem =
        document.getElementById('menuItem-callBarring');

      let callWaitingItem = document.getElementById('menuItem-callWaiting');
      let callerIdItem = document.getElementById('menuItem-callerId');
      let dtmfItem = document.getElementById('menuItem-dtmf');
      // The following feature is limited to CDMA types.
      let voicePrivacyItem =
        document.getElementById('menuItem-voicePrivacyMode');
      //[CNT-660] BDC zhangwp add for hide DTMF menu. begin
      
      let currentSimIndex = DsdsSettings.getIccCardIndexForCallSettings();
      let sim = SIMSlotManager.getSlots()[currentSimIndex];
      //[CNT-660] BDC zhangwp add for hide DTMF menu. end
       console.log(" cs_updateNetworkTypeLimitedItemsVisibility currentSimIndex = "+currentSimIndex+" ; sim ="+sim);
      let enabled = (newType !== 'gsm' &&
        newType !== 'voice-over-wifi' && newType !== 'video-over-wifi');
      callBarringItem.hidden =
      callWaitingItem.hidden =
      callerIdItem.hidden = enabled;
      //[BTS-2491] BDC matf remove call barring item for 50503. begin
      if(sim && sim.simCard != undefined && sim.simCard.iccInfo != undefined) {
       var simmcc = sim.simCard.iccInfo.mcc;
       var simmnc = sim.simCard.iccInfo.mnc;
        console.log(" cs_updateNetworkTypeLimitedItemsVisibility simmcc = "+simmcc+" ; simmnc ="+simmnc);
       var hideCallBarring = cs_updateCallBarringItemsVisibility(simmcc,simmnc);
           console.log(" cs_updateNetworkTypeLimitedItemsVisibility hideCallBarring = "+hideCallBarring);
        if (hideCallBarring){
        callBarringItem.hidden = true;
        }
      }
        console.log("cs_updateNetworkTypeLimitedItemsVisibility callBarringItem.hidden = "+callBarringItem.hidden);
      //[BTS-2491] BDC matf remove call barring item for 50503 . end
      getSetting('callforward.settings.ui').then((value) => {
        if (value === HIDE) {
          callForwardingItem.hidden = true;
        } else if (value === GRAYOUT) {
          callForwardingItem.hidden = enabled;
          callForwardingItem.classList.add('none-select');
          callForwardingItem.setAttribute('aria-disabled', true);
          var hrefItem = callForwardingItem.querySelector('a');
          hrefItem.removeAttribute('href');
        } else {
          callForwardingItem.hidden = enabled;
          callForwardingItem.classList.remove('none-select');
          callForwardingItem.removeAttribute('aria-disabled');
          var hrefItem = callForwardingItem.querySelector('a');
          hrefItem.setAttribute('href', '#call-cfsettings-list');
        }
      });

      voicePrivacyItem.hidden =
        (newType !== 'cdma');

      //[CNT-660] BDC zhangwp modify for hide DTMF menu. begin
/*
      if (newType === 'gsm') {
        dtmfItem.classList.add('none-select');
        dtmfItem.setAttribute('aria-disabled', true);
        _settings.createLock().set({'phone.dtmf.type' : 'long'});
      } else {
        dtmfItem.classList.remove('none-select');
        dtmfItem.removeAttribute('aria-disabled');
        window.navigator.mozSettings.removeObserver('phone.dtmf.type', cs_showToast);
        window.navigator.mozSettings.addObserver('phone.dtmf.type', cs_showToast);
      }
*/
      //[CNT-660] BDC zhangwp add for hide DTMF menu. begin
      if(sim && sim.simCard != undefined && sim.simCard.iccInfo != undefined) {
        if(sim.simCard.iccInfo.iccType === 'ruim' || sim.simCard.iccInfo.iccType === 'csim') {
          dtmfItem.classList.remove('none-select');
          dtmfItem.removeAttribute('aria-disabled');
          window.navigator.mozSettings.removeObserver('phone.dtmf.type', cs_showToast);
          window.navigator.mozSettings.addObserver('phone.dtmf.type', cs_showToast);
        } else {
          dtmfItem.hidden = true;
          dtmfItem.classList.add('none-select');
          dtmfItem.setAttribute('aria-disabled', true);
          _settings.createLock().set({'phone.dtmf.type' : 'long'});
        }
      } else {
        dtmfItem.hidden = true;
      }
      //[CNT-660] BDC zhangwp modify for hide DTMF menu. end

      window.dispatchEvent(new CustomEvent('refresh'));
    }

    function cs_showToast() {
      showToast('changessaved');
    }

    function cs_removeNetworkTypeCheckListener() {
      if (_mobileConnection.voice) {
        _mobileConnection.removeEventListener('voicechange', cs_onTypeChange);
      }
      if (_mobileConnection.data) {
        _mobileConnection.removeEventListener('datachange', cs_onTypeChange);
      }
      if (_imsHandler) {
        _imsHandler.removeEventListener('capabilitychange', cs_onCapabilityChange);
      }
    }

    /**
     * Refresh the items in the call setting panel.
     */
    function cs_refreshCallSettingItems() {
      return new Promise((resolve) => {
        cs_updateFdnStatus();
        cs_updateVoicePrivacyItemState();

        cs_updateCallerIdPreference();
        cs_updateCallerIdItemState();
        cs_updateCallWaitingItemState();
        resolve();
      });
    }

    /**
     *
     */
    function cs_enableTapOnCallerIdItem(enable) {
      var element = document.getElementById('menuItem-callerId');
      var select = element.querySelector('select');

      select.disabled = !enable;
      if (enable) {
        element.removeAttribute('aria-disabled');
        element.classList.remove('none-select');
      } else {
        element.setAttribute('aria-disabled', true);
        element.classList.add('none-select');
      }
    }

    function cs_updateCallerIdPreference(callback) {
      _taskScheduler.enqueue('CALLER_ID_PREF', function(done) {
        if (typeof callback !== 'function') {
          callback = function() {
            done();
          };
        } else {
          var originalCallback = callback;
          callback = function() {
            originalCallback();
            done();
          };
        }

        cs_enableTapOnCallerIdItem(false);

        var req = _mobileConnection.getCallingLineIdRestriction();
        req.onsuccess = function() {
          var value = 0; //CLIR_DEFAULT

          // In some legitimates error cases (FdnCheckFailure), the req.result
          // is undefined. This is fine, we want this, and in this case we will
          // just display an error message for all the matching requests.
          if (req.result) {
            switch (req.result['m']) {
              case 1: // Permanently provisioned
              case 3: // Temporary presentation disallowed
              case 4: // Temporary presentation allowed
                switch (req.result['n']) {
                  case 1: // CLIR invoked, CLIR_INVOCATION
                  case 2: // CLIR suppressed, CLIR_SUPPRESSION
                  case 0: // Network default, CLIR_DEFAULT
                    value = req.result['n']; //'CLIR_INVOCATION'
                    break;
                  default:
                    value = 0; //CLIR_DEFAULT
                    break;
                }
                cs_enableTapOnCallerIdItem(true);
                break;
              case 0: // Not Provisioned
              case 2: // Unknown (network error, etc)
              default:
                value = 0; //CLIR_DEFAULT
                cs_enableTapOnCallerIdItem(false);
                break;
            }

            // Set the Call ID status,
            //   first item value for SIM1 and second item value for SIM2
            SettingsCache.getSettings(function(results) {
              var preferences = results['ril.clirMode'] || [0, 0];
              var targetIndex = DsdsSettings.getIccCardIndexForCallSettings();
              preferences[targetIndex] = value;
              var setReq = _settings.createLock().set({
                'ril.clirMode': preferences
              });
              setReq.onsuccess = callback;
              setReq.onerror = callback;
            });
          } else {
            callback();
          }
        };
        req.onerror = callback;
      });
    }

    /**
     *
     */
    function cs_updateCallerIdItemState(callback) {
      var element = document.getElementById('menuItem-callerId');
      if (!element || element.hidden) {
        if (typeof callback === 'function') {
          callback(null);
        }
        return;
      }

      _taskScheduler.enqueue('CALLER_ID', function(done) {
        SettingsCache.getSettings(function(results) {
          var targetIndex = DsdsSettings.getIccCardIndexForCallSettings();
          var preferences = results['ril.clirMode'];
          var preference = (preferences && preferences[targetIndex]) || 0;
          var input = document.getElementById('ril-callerId');

          var value;
          switch (preference) {
            case 1: // CLIR invoked
              value = 'CLIR_INVOCATION';
              break;
            case 2: // CLIR suppressed
              value = 'CLIR_SUPPRESSION';
              break;
            case 0: // Network default
            default:
              value = 'CLIR_DEFAULT';
              break;
          }

          input.value = value;

          if (typeof callback === 'function') {
            callback();
          }
          done();
        });
      });
    }

    function cs_checkCallerId(clirMode) {
      var targetIndex = DsdsSettings.getIccCardIndexForCallSettings();
      var getReq = _settings.createLock().get('ril.clirMode');
      getReq.onsuccess = function () {
        var preferences = getReq.result['ril.clirMode'];
        var preference = (preferences && preferences[targetIndex]) || 0;
        if (clirMode === preference) {
          showToast('changessaved');
        }
      };
    }

    /**
     *
     */
    function cs_initCallerId() {
      var element = document.getElementById('ril-callerId');

      var updateItem = function() {
        cs_updateCallerIdItemState(function() {
          cs_enableTapOnCallerIdItem(true);
        });
      };

      var updatePreferenceAndItem =
        cs_updateCallerIdPreference.bind(null, updateItem);

      // We listen for change events so that way we set the CLIR mode once the
      // user change the option value.
      element.addEventListener('change', (event) => {
        var clirMode = _clirConstantsMapping[element.value];
        var setReq = _mobileConnection.setCallingLineIdRestriction(clirMode);
        // If the setting success, system app will sync the value.
        // If the setting fails, we force sync the value here and update the UI.
        setReq.onerror = updatePreferenceAndItem;
        setReq.onsuccess = () => {
          cs_updateCallerIdPreference();
          cs_checkCallerId(clirMode);
        }
      });
    }

    /**
     * Enable/Disable call waiting settings page
     */
    function cs_enableTapOnCallWaitingItem(enable) {
      var menuItem = document.getElementById('menuItem-callWaiting');
      var select = menuItem.querySelector('select');
      var descText = menuItem.querySelector('small');
      // update call waiting query status
      _callWaitingQueryStatus = enable;

      // update the description
      function getSelectValue() {
        var enabled = select.value === 'true';
        var status = '';
        if (select) {
          status = enabled ? 'enabled' : 'disabled';
        }
        return status;
      }

      if (descText && !enable) {
        descText.setAttribute('data-l10n-id', 'callSettingsQuery');
      } else {
        // Clear the data-l10n-id information
        descText.innerHTML = '';
        descText.setAttribute('data-l10n-id', getSelectValue());
      }

      if (enable) {
        menuItem.removeAttribute('aria-disabled');
        menuItem.classList.remove('none-select');
      } else {
        menuItem.setAttribute('aria-disabled', true);
        menuItem.classList.add('none-select');
      }
    }

    /**
     *
     */
    function cs_updateCallWaitingItemState(callback) {
      var menuItem = document.getElementById('menuItem-callWaiting');
      if (!menuItem || menuItem.hidden) {
        if (typeof callback === 'function') {
          callback(null);
        }
        return;
      }

      cs_enableTapOnCallWaitingItem(false);

      _taskScheduler.enqueue('CALL_WAITING', function(done) {
        var select = menuItem.querySelector('select');

        var getCWEnabled = _mobileConnection.getCallWaitingOption();
        getCWEnabled.onsuccess = function cs_getCWEnabledSuccess() {
          var enabled = getCWEnabled.result;
          select.value = enabled;
          menuItem.dataset.state = enabled ? 'on' : 'off';
          cs_enableTapOnCallWaitingItem(true);

          if (callback) {
            callback(null);
          }
          done();
        };
        getCWEnabled.onerror = function cs_getCWEnabledError() {
          menuItem.dataset.state = 'unknown';
          if (callback) {
            callback(null);
          }
          done();
        };
      });
    }

    /**
     * @HACK To Enable the "Call Waiting" select option
     */
    function _enableCallWaitingSelect(evt) {
      if (evt.key === 'Enter') {
        var select = document.querySelector('li.focus select');
        if (select && _callWaitingQueryStatus) {
          select.hidden = false;
          select.focus();
        }
        select.hidden = true;
      }
    }

    /**
     *
     */
    function cs_initCallWaiting() {
      var callWaitingItem = document.getElementById('menuItem-callWaiting');
      callWaitingItem.addEventListener('keydown', _enableCallWaitingSelect);

      // Bind call waiting setting to the input
      var select =
        document.querySelector('#menuItem-callWaiting select');
      select.addEventListener('change', function cs_cwInputChanged(event) {
        var handleSetCallWaiting = function cs_handleSetCallWaiting() {
          cs_updateCallWaitingItemState(function() {
            cs_enableTapOnCallWaitingItem(true);
          });
        };
        cs_enableTapOnCallWaitingItem(false);
        var enabled = (select.value === 'true') || false;
        var req = _mobileConnection.setCallWaitingOption(enabled);
        req.onerror = handleSetCallWaiting;
        req.onsuccess = () => {
          cs_updateCallWaitingItemState(function() {
            cs_enableTapOnCallWaitingItem(true);
            showToast('changessaved');
          });
        };
      });
    }

    /**
     *
     */
    function cs_updateVoiceMailItemState() {
      var voiceMailMenuItem = document.getElementById('voiceMail-desc');
      var targetIndex = DsdsSettings.getIccCardIndexForCallSettings();

      voiceMailMenuItem.textContent = '';
      SettingsCache.getSettings(function(results) {
        var numbers = results['ril.iccInfo.mbdn'];
        var number = numbers[targetIndex];
        if (number) {
          voiceMailMenuItem.removeAttribute('data-l10n-id');
          voiceMailMenuItem.textContent = number;
        } else {
          voiceMailMenuItem.setAttribute('data-l10n-id',
                                         'voiceMail-number-notSet');
        }
      });
    }

    function cs_initVoiceMailClickEvent() {
      document.querySelector('.menuItem-voicemail').onclick = function() {
        DialogService.show('call-voiceMailSettings');
      };
    }

    /**
     *
     */
    function cs_initVoiceMailSettings() {
      // update all voice numbers if necessary
      SettingsCache.getSettings(function(results) {
        var settings = navigator.mozSettings;
        var voicemail = navigator.mozVoicemail;
        var updateVMNumber = false;
        var numbers = results['ril.iccInfo.mbdn'] || [];

        Array.prototype.forEach.call(_mobileConnections, function(conn, index) {
          var number = numbers[index];
          // If the voicemail number has not been stored into the database yet
          // we check whether the number is provided by the mozVoicemail API. In
          // that case we store it into the setting database.
          if (!number && voicemail) {
            number = voicemail.getNumber(index);
            if (number) {
              updateVMNumber = true;
              numbers[index] = number;
            }
          }
        });

        if (updateVMNumber) {
          var req = settings.createLock().set({
            'ril.iccInfo.mbdn': numbers
          });
          req.onsuccess = function() {
            cs_updateVoiceMailItemState();
            settings.addObserver('ril.iccInfo.mbdn', function() {
              cs_updateVoiceMailItemState();
            });
          };
        } else {
          cs_updateVoiceMailItemState();
          settings.addObserver('ril.iccInfo.mbdn', function() {
            cs_updateVoiceMailItemState();
          });
        }
      });
    }

    function cs_updateVoicePrivacyItemState() {
      var menuItem = document.getElementById('menuItem-voicePrivacyMode');
      if (!menuItem || menuItem.hidden) {
        return;
      }

      var privacyModeSelect = menuItem.querySelector('select');
      var getReq = _mobileConnection.getVoicePrivacyMode();
      getReq.onsuccess = function get_vpm_success() {
        privacyModeSelect.value = getReq.result;
      };
      getReq.onerror = function get_vpm_error() {
        console.warn('get voice privacy mode: ' + getReq.error.name);
      };
    }

    /**
     * Init voice privacy mode.
     */
    function cs_initVoicePrivacyMode() {
      var defaultVoicePrivacySettings =
      Array.prototype.map.call(_mobileConnections,
        function() { return [true, true]; });
      var voicePrivacyHelper =
        SettingsHelper('ril.voicePrivacy.enabled', defaultVoicePrivacySettings);
      var privacyModeItem =
        document.getElementById('menuItem-voicePrivacyMode');
      var privacyModeSelect =
        privacyModeItem.querySelector('select');

      privacyModeSelect.addEventListener('change',
        function vpm_inputChanged() {
          var checked = (privacyModeSelect.value === 'true' || false);
          voicePrivacyHelper.get(function gotVP(values) {
            var originalValue = !checked;
            var setReq = _mobileConnection.setVoicePrivacyMode(checked);
            setReq.onsuccess = function set_vpm_success() {
              var targetIndex = DsdsSettings.getIccCardIndexForCallSettings();
              values[targetIndex] = !originalValue;
              voicePrivacyHelper.set(values);
            };
            setReq.onerror = function get_vpm_error() {
              // restore the value if failed.
              privacyModeSelect.value = originalValue;
            };
          });
      });
    }

    /**
     *
     */
    function cs_updateFdnStatus() {
      var iccObj = getIccByIndex();
      if (!iccObj) {
        return;
      }

      var req = iccObj.getCardLock('fdn');
      req.onsuccess = function spl_checkSuccess() {
        var enabled = req.result.enabled;

        var simFdnDesc = document.querySelector('#fdnSettings-desc');
        simFdnDesc.setAttribute('data-l10n-id', enabled ? 'on' : 'off');
        _settings.createLock().set({'ril.fdn.enabled' : enabled});

        var fdnSettingsBlocked = document.querySelector('#fdnSettingsBlocked');
        if (fdnSettingsBlocked) {
          fdnSettingsBlocked.hidden = !enabled;
        }
        var callForwardingItem =
          document.getElementById('menuItem-callForwarding');

        getSetting('callforward.settings.ui').then((value) => {
          if (value === HIDE) {
            callForwardingItem.hidden = true;
          } else if (value === GRAYOUT) {
            callForwardingItem.hidden = enabled;
            callForwardingItem.classList.add('none-select');
            callForwardingItem.setAttribute('aria-disabled', true);
            var hrefItem = callForwardingItem.querySelector('a');
            hrefItem.removeAttribute('href');
          } else {
            callForwardingItem.hidden = enabled;
            callForwardingItem.classList.remove('none-select');
            callForwardingItem.removeAttribute('aria-disabled');
            var hrefItem = callForwardingItem.querySelector('a');
            if (DeviceFeature.getValue('vilte') === 'true') {
              hrefItem.setAttribute('href', '#call-cfsettings-list');
            } else {
              hrefItem.setAttribute('href', '#call-cfSettings');
            }
          }
        });

        var voice = _mobileConnection.voice;
        var data = _mobileConnection.data;
        if ((_imsHandler &&
          (_imsHandler.capability === 'voice-over-wifi' ||
          _imsHandler.capability === 'video-over-wifi')) ||
          (voice && voice.state === 'registered' &&
          voice.connected === true) ||
          (data && data.state === 'registered' &&
          data.connected === true)) {
          callForwardingItem.hidden = enabled;
          window.dispatchEvent(new CustomEvent('refresh'));
        }
        window.dispatchEvent(new CustomEvent('refresh'));
      };
    }
    return {
      init:  function(panel) {
        cs_init();
      },
      onBeforeShow: function() {
        cs_addNetworkTypeCheckListener();
      },
      onBeforeHide: function() {
        cs_removeNetworkTypeCheckListener();
      }
    };
  };
  return CallSettings;
});

/**
 * TaskScheduler helps manage tasks and ensures they are executed in
 * sequential order. When a task of a certain type is enqueued, all pending
 * tasks of the same type in the queue are removed. This avoids redundant
 * queries and improves user perceived performance.
 */
var TaskScheduler = function() {
  return {
    _isLocked: false,
    _tasks: [],
    _lock: function() {
      this._isLocked = true;
    },
    _unlock: function() {
      this._isLocked = false;
      this._executeNextTask();
    },
    _removeRedundantTasks: function(type) {
      return this._tasks.filter(function(task) {
        return task.type !== type;
      });
    },
    _executeNextTask: function() {
      if (this._isLocked) {
        return;
      }
      var nextTask = this._tasks.shift();
      if (nextTask) {
        this._lock();
        nextTask.func(function() {
          this._unlock();
        }.bind(this));
      }
    },
    enqueue: function(type, func) {
      this._tasks = this._removeRedundantTasks(type);
      this._tasks.push({
        type: type,
        func: func
      });
      this._executeNextTask();
    }
  };
};
