function enumerateAll(e,t,n){function i(t){if(c.result=t.target.result,!c.result&&(a++,a<e.length))return r(),void 0;if(c.onsuccess)try{c.onsuccess(t)}catch(n){console.warn("enumerateAll onsuccess threw",n)}}function o(t){if(c.error=t.target.error,"NotFoundError"===c.error.name&&a!==e.length-1)return a++,r(),void 0;if(c.onerror)try{c.onerror(t)}catch(n){console.warn("enumerateAll onerror threw",n)}}function r(){s=e[a].enumerate(t,n||{}),s.onsuccess=i,s.onerror=o}var a=0,s=null,c={"continue":function(){s.continue()}};return r(),c}