'use strict';var CallsHandler=(function callsHandler(){var CALLS_LIMIT=8;var CDMA_CALLS_LIMIT=2;var handledCalls=[];var exitCallScreenTimeout=null;const CALL_TYPE_VOICE_N_VIDEO=1;const CALL_TYPE_VT=4;var oldBrightnessValue=0.2;var brightnessTimer=null;var isBrightnessDimMode=false;const BRIGHTNESS_TIMEOUT=5000;var callInfoTimer=null;const CALLINFO_TIMEOUT=5000;const DIM_VALUE=0.115;var _=navigator.mozL10n.get;var toneInterval=null;var ringbackToneInterval=null;var callHeldByUser=null;var telephony=window.navigator.mozTelephony;var enableSpeaker=false;var isBTReceiverUsed=false;var showInfo=true;var lastSuppserviceCode='';var operateState={answer:false,hangUp:false,merge:false};var _flipManager=null;var fo=false;telephony.oncallschanged=onCallsChanged;telephony.onringbacktone=onRingbackTone;telephony.onsuppservicenotification=onsuppservicenotification;window.addEventListener('keydown',handleKeyEvent,true);window.addEventListener('largetextenabledchanged',(event)=>{document.body.classList.toggle('large-text',navigator.largeTextEnabled);});document.body.classList.toggle('large-text',navigator.largeTextEnabled);navigator.getFlipManager().then((flipManager)=>{_flipManager=flipManager;flipManager.addEventListener('flipchange',onFlipChange);});var btHelper=new BluetoothHelper();var screenLock;var btConnectedState;var imsGroupRecords=[];function getImsRecord(){return imsGroupRecords;}
function insertImsRecord(number,name){var imsRecord={};imsRecord.number=number;imsRecord.name=name;imsGroupRecords.push(imsRecord);}
function setup(){if(telephony){telephony.muted=false;}
btHelper.getConnectedDevicesByProfile(btHelper.profiles.HFP,function(result){btConnectedState=!!(result&&result.length);isBTReceiverUsed=btConnectedState;CallScreen.setBTReceiverIcon(!!(result&&result.length));});btHelper.onhfpstatuschanged=function(evt){btConnectedState=evt.status;isBTReceiverUsed=btConnectedState;CallScreen.setBTReceiverIcon(evt.status);};var acm=navigator.mozAudioChannelManager;if(acm){acm.addEventListener('headphoneschange',function onheadphoneschange(){if(!CallsHandler.isAnyOpenCalls){return;}
if(acm.headphones){CallScreen.switchToDefaultOut();}else if(CallsHandler.enableSpeaker){CallScreen.switchToSpeaker();}});}
btHelper.onscostatuschanged=function onscostatuschanged(evt){btConnectedState=evt.status;if(evt.status){CallScreen.switchToDefaultOut();}else if(CallsHandler.enableSpeaker){CallScreen.switchToSpeaker();}};navigator.mozSetMessageHandler('bluetooth-dialer-command',handleBTCommand);navigator.mozSetMessageHandler('cdma-info-rec-received',cdmaCallConnected);navigator.mozSettings.addObserver('lockscreen.remote-lock',(evt)=>{if(!(evt.settingValue[0]===''&&evt.settingValue[1]==='')){hangUpAllCalls();}});if(telephony.calls.length||telephony.conferenceGroup.state!==''){onCallsChanged();}}
function hangUpAllCalls(){telephony.calls.forEach(function callIterator(call){call.hangUp();});if(telephony.conferenceGroup.calls.length||telephony.conferenceGroup.state!==''){endConferenceCall();}}
function onFlipChange(evt){var flipOpen=evt.currentTarget.flipOpened;var answerModeEnable=false;if(flipOpen){var lock=navigator.mozSettings.createLock();var answerModeSettings=lock.get('phone.answer.flipopen.enabled');answerModeSettings.onsuccess=function(){answerModeEnable=answerModeSettings.result['phone.answer.flipopen.enabled'];if(answerModeEnable){answer();}};if(telephony.active){console.log("call_handler","onFlipChange, setMicMode 0");SetMicMode(0);}
answerModeSettings.onerror=function(){console.error("An error occure when get the answermode");};}else{if(btConnectedState){if(telephony.active){console.log("call_handler","onFlipChange, setMicMode 1");SetMicMode(1);}
return;}
var acm=navigator.mozAudioChannelManager;if(acm&&acm.headphones){if(telephony.active){console.log("call_handler","onFlipChange, setMicMode 1");SetMicMode(1);}
return;}
hangUpAllCalls();}}
function SetMicMode(aMicMode){console.log("call_handler","SetMicMode, aMicMode is "+aMicMode);telephony.micMode=aMicMode;}
function isFlipOpend(){console.log('calls_handler isFlipOpend ');if(_flipManager){console.log(' calls_handler isFlipOpend _flipManager.flipOpened: '+_flipManager.flipOpened);return _flipManager.flipOpened;}else{console.log(' calls_handler isFlipOpend navigator.flipOpened: '+navigator.flipOpened);return navigator.flipOpened;}}
function initMicMode(){if(!isFlipOpend()){console.log(' initMicMode SetMicMode(1) filpstatus= '+isFlipOpend());SetMicMode(1);}else{console.log(' initMicMode SetMicMode(0) filpstatus= '+isFlipOpend());SetMicMode(0);}}
function cdmaCallConnected(message){if(message.lineControl){handledCalls[0].allowConnected();}}
var highPriorityWakeLock=null;function onCallsChanged(){CallScreen.showMainCallscreen();if(!highPriorityWakeLock&&telephony.calls.length>0){highPriorityWakeLock=navigator.requestWakeLock('high-priority');}
if(highPriorityWakeLock&&telephony.calls.length===0){highPriorityWakeLock.unlock();highPriorityWakeLock=null;}
if(telephony.active){telephony.active.addEventListener('error',handleBusyErrorAndPlayTone);}
telephony.calls.forEach(function callIterator(call){var alreadyAdded=handledCalls.some(function hcIterator(hc){return(hc.call==call);});if(!alreadyAdded){addCall(call);}});function hcIterator(call){return(call==hc.call);}
for(var index=(handledCalls.length-1);index>=0;index--){if(handledCalls[index].call.state==='disconnected'){removeCall(index);}}
if(telephony.calls.length===1&&telephony.conferenceGroup.calls.length===0){stopWaitingTone();}
if(cdmaCallWaiting()){CallScreenHelper.setState("connected_waiting");handleCallWaiting(telephony.calls[0]);}else{CallScreenHelper.setState();}
exitCallScreenIfNoCalls(CallScreen.callEndPromptTime);CallScreenHelper.render();}
function onRingbackTone(evt){if(evt.playRingbackTone){telephony.calls.some((call)=>{if(call.state==='dialing'||call.state==='alerting'){playRingbackTone(call);return true;}});}else{stopRingbackTone();}}
function playRingbackTone(call){var sequence=[[425,425,1000],[0,0,4000]];ringbackToneInterval=window.setInterval(()=>{TonePlayer.playSequence(sequence);},5200);TonePlayer.playSequence(sequence);call.addEventListener('statechange',function stateChange(){if(call.state==='dialing'||call.state==='alerting'){return;}
stopRingbackTone();call.removeEventListener('stateChange',stateChange);})}
function stopRingbackTone(){window.clearInterval(ringbackToneInterval);}
function onsuppservicenotification(evt){if(!evt||!evt.code){return;}
switch(evt.code){case'mt-call_on_hold':CallScreen.showToast('mt-call-on-hold');break;case'mt-call_retrieved':if(lastSuppserviceCode!=='mt-multi_party_call'){CallScreen.showToast('mt-call_retrieved');}
break;case'mt-multi_party_call':CallScreen.showToast('mt-multi_party_call');break;default:return;}
lastSuppserviceCode=evt.code;}
function unmute(){if(!telephony.muted){return;}
CallsHandler.toggleMute();OptionHelper.swapParams('inCallParams','Unmute','Mute',true);CallScreen.updateCallsMuteState();}
function offSpeaker(){if(telephony.speakerEnabled){CallsHandler.toggleSpeaker();OptionHelper.swapParams('inCallParams','Speaker-off','Speaker',true);}}
function getOldBrightnessValue(){return new Promise(resolve=>{var lock=navigator.mozSettings.createLock();var brightnessSetting=lock.get('screen.brightness');brightnessSetting.onsuccess=function(){oldBrightnessValue=brightnessSetting.result['screen.brightness'];resolve();};});}
function handleBrightnessTimer(isSetTimer){if(brightnessTimer){clearTimeout(brightnessTimer);}
if(isSetTimer){isSetTimer=!telephony.calls.some(function(call){return call.state!=='connected'&&call.state!=='held';});}
if(isSetTimer){brightnessTimer=setTimeout(()=>{handleBrightnessDim();},BRIGHTNESS_TIMEOUT);}}
function handleCallInfoTimer(isSetTimer){var videoCallState;if(telephony.calls.length===1){videoCallState=telephony.calls[0].videoCallState;}
if('TxEnabled'===videoCallState||!CallsHandler.isVideoCall()){return;}
if(callInfoTimer){clearTimeout(callInfoTimer);}
if(isSetTimer){callInfoTimer=setTimeout(()=>{if('connected'===telephony.calls[0].state&&showInfo&&CallsHandler.isVideoCall()){toggleShowInfo();handleCallInfoTimer(false);}},CALLINFO_TIMEOUT);}}
function handleBrightnessDim(){if(navigator.mozPower.screenEnabled){navigator.mozPower.screenBrightness=DIM_VALUE;isBrightnessDimMode=true;}}
function handleKeyEvent(event){if(isBrightnessDimMode){handleBrightnessRecover(true);}else{handleBrightnessTimer(true);}}
function handleBrightnessRecover(isSetTimer){var settingObject={};settingObject['screen.brightness']=oldBrightnessValue;navigator.mozSettings.createLock().set(settingObject);if(isSetTimer){handleBrightnessTimer(true);}else{handleBrightnessTimer(false);}
isBrightnessDimMode=false;}
function addCall(call){var cdmaTypes=['evdo0','evdoa','evdob','1xrtt','is95a','is95b'];var type=window.navigator.mozMobileConnections[call.serviceId].voice.type;var cdmaOutingCalls=(handledCalls.length==0&&cdmaTypes.indexOf(type)!==-1&&call.state==='dialing');if(telephony.calls.length>CALLS_LIMIT){HandledCall(call);call.hangUp();return;}
if(telephony.calls.length===1&&!telephony.conferenceGroup.calls.length){resetToDefault();CallScreen.switchToDefaultOut(true);if(CallsHandler.isVideoCall()){CallsHandler.enableSpeakerForVT();}
getOldBrightnessValue().then(()=>{handleBrightnessRecover(true);});}
var hc=new HandledCall(call);if(cdmaOutingCalls){hc.allowedConnected=false;}
handledCalls.push(hc);CallScreen.insertCall(hc.node);if(call.state==='incoming'||call.state==='dialing'||call.state==='alerting'){if(call.state==='incoming'){handleBrightnessRecover(true);turnScreenOn(call);}else{handleBrightnessTimer(true);}
call.addEventListener('statechange',function callStateChange(){if(telephony.calls.length&&(call.state==='incoming'||call.state==='dialing'||call.state==='alerting')){return;}
if((telephony.calls.length>=2||telephony.conferenceGroup.state)&&call.state==='connected'&&telephony.muted){unmute();}
call.removeEventListener('statechange',callStateChange);handleBrightnessTimer(true);});}
if((handledCalls.length>1)||((telephony.calls.length>=1)&&(telephony.conferenceGroup.calls.length))){if(call.state==='incoming'){if(CallsHandler.isVideoCall()){end();}else{hc.hide();handleCallWaiting(call);}}else{hc.show();}}else{if(telephony.ownAudioChannel){telephony.ownAudioChannel();}}}
function conferenceCallEnd(){if(handledCalls.length!==1){return;}
CallScreen.incomingInfo.classList.add('ended');CallScreen.incomingNumberAdditionalTelType.textContent='';CallScreen.incomingNumberAdditionalTel.textContent=_('callEnded');CallScreen.hideIncoming();var remainingCall=handledCalls[0];if(remainingCall.call.state==='incoming'){remainingCall.show();setTimeout(function nextTick(){if(remainingCall.call.state=='incoming'){CallScreen.render('incoming');}});}}
function removeCall(index){handledCalls.splice(index,1);var length=handledCalls.length;var incoming=false;if(length){incoming=handledCalls.some((handleCall)=>{return handleCall.call.state==='incoming';});}
if(incoming&&(length>1||telephony.conferenceGroup.calls.length||telephony.conferenceGroup.state)){return;}
CallScreen.hideIncoming();if(!incoming){CallScreen.incomingInfo.classList.add('ended');CallScreen.incomingNumberAdditionalTelType.textContent='';CallScreen.incomingNumberAdditionalTel.textContent=_('callEnded');return;}
var remainingCall=null;handledCalls.forEach(function(handleCall){if(handleCall.call.state=='incoming'){remainingCall=handleCall;remainingCall.show();return;}});}
function turnScreenOn(call){CallScreen.lockScreenWake();call.addEventListener('statechange',function callStateChange(){if(telephony.calls.length&&telephony.calls[0].state==='incoming'){return;}
call.removeEventListener('statechange',callStateChange);CallScreen.unlockScreenWake();});}
function handleBusyErrorAndPlayTone(evt){if(evt.call!==undefined&&evt.call.error.name==='BusyError'){var sequence=[[425,0,500],[0,0,500],[425,0,500],[0,0,500],[425,0,500],[0,0,500],[425,0,500],[0,0,500],[425,0,500],[0,0,500]];var sequenceDuration=sequence.reduce(function(prev,curr){return prev+curr[2];},0);TonePlayer.playSequence(sequence);TonePlayer.setWaitingTime(sequenceDuration);}}
function handleCallWaiting(call){var number=call.secondId?call.secondId.number:call.id.number;var options={filterBy:['number'],filterValue:number,filterOp:'fuzzyMatch'};var request=navigator.mozContacts.findBlockedNumbers(options);request.onsuccess=()=>{if(!request.result||!request.result.length){displayCallWaiting(call);}};request.onerror=()=>{displayCallWaiting(call);};}
function displayCallWaiting(call){var simNum=CallScreen.getSimNum(call);var statusIcon=CallScreen.incomingInfo.querySelector('.status-icon');if(simNum!==''){statusIcon.dataset.index=simNum;}else{delete statusIcon.dataset.index;}
if(CallsHandler.isVideoCall()){statusIcon.dataset.video='video-';}else{delete statusIcon.dataset.video;}
CallScreen.incomingHdIcon.classList.toggle('hide',true);CallScreen.incomingInfo.classList.remove('ended');CallScreen.showIncoming();call.addEventListener('statechange',function callStateChange(){if(CallScreen.incomingContainer.classList.contains('displayed')&&call.state==='incoming'){CallScreen.incomingHdIcon.classList.toggle('hide',true);}else{call.removeEventListener('statechange',callStateChange);}});playWaitingTone(call);var number=call.secondId?call.secondId.number:call.id.number;if(!number){CallScreen.incomingNumber.textContent=_('unknown');CallScreen.incomingNumberAdditionalTel.textContent='';return;}
Contacts.findByNumber(number,function lookupContact(contact,matchingTel){var contactName=number;var contactNameExist=false;if(contact){if(contact.name&&contact.name.length&&contact.name[0]!==''){contactName=contact.name;contactNameExist=true;}else if(contact.org&&contact.org.length&&contact.org[0]!==''){contactName=contact.org;contactNameExist=true;}}
if(CallsHandler.setCNAPText(call,CallScreen.incomingNumberAdditionalTel,CallScreen.incomingNumber)){if(contactNameExist){CallScreen.incomingNumber.textContent=contactName;}}else if(contact&&contact.name){CallScreen.incomingInfo.classList.add('additionalInfo');CallScreen.incomingNumber.textContent=contactName;CallScreen.incomingNumberAdditionalTelType.textContent=Utils.getPhoneNumberAdditionalInfo(matchingTel);CallScreen.incomingNumberAdditionalTel.textContent=number;}else{CallScreen.incomingNumber.textContent=number;CallScreen.incomingNumberAdditionalTelType.textContent='';CallScreen.incomingNumberAdditionalTel.textContent=_('unknown');}});}
function exitCallScreenIfNoCalls(timeout){if(handledCalls.length===0&&!telephony.conferenceGroup.state){document.body.classList.toggle('no-handled-calls',true);CallScreenHelper.finalCallEndOption();CallsHandler.enableSpeaker=false;lastSuppserviceCode='';operateState={answer:false,hangUp:false,merge:false};if(exitCallScreenTimeout!==null){clearTimeout(exitCallScreenTimeout);exitCallScreenTimeout=null;}
exitCallScreenTimeout=setTimeout(function(evt){OptionHelper.softkeyPanel.hideMenu();CallRecording.showSpaceNoticeIfNeeded();Rtt.resetRtt();exitCallScreenTimeout=null;if(handledCalls.length===0){handleBrightnessRecover(false);var imsGroupRecords=getImsRecord();imsGroupRecords.splice(0,imsGroupRecords.length);CallScreen.resetWfcRemind();unmute();}else{document.body.classList.toggle('no-handled-calls',false);}},timeout);}}
function openLines(){return telephony.calls.length+
(telephony.conferenceGroup.calls.length?1:0);}
function handleBTCommand(message){var command=message.command;switch(command){case'CHUP':end();break;case'ATA':answer();break;case'CHLD=0':hangupWaitingCalls();break;case'CHLD=1':if((handledCalls.length===1)&&!cdmaCallWaiting()||telephony.conferenceGroup.state&&!telephony.calls.length){end();}else{endAndAnswer();}
break;case'CHLD=2':if((openLines()===1)&&!cdmaCallWaiting()){holdOrResumeCallByUser();}else if(CallScreenHelper.getCall('incoming')){holdAndAnswer();}else{toggleCalls();}
break;case'CHLD=3':mergeCalls();break;default:var partialCommand=command.substring(0,3);if(partialCommand==='VTS'){DtmfHelper.press(command.substring(4));}
break;}}
function answer(option){if(!handledCalls.length){return;}
if((handledCalls.length>1)||cdmaCallWaiting()||telephony.conferenceGroup.calls.length){holdAndAnswer();return;}
handleCall(handledCalls[0].call,'answer',option);}
function holdAndAnswer(){if(((handledCalls.length<2)&&(telephony.calls.length<2)&&!telephony.conferenceGroup.calls.length)&&!cdmaCallWaiting()){return;}
if(telephony.calls.length===3||(telephony.calls.length>=2&&telephony.conferenceGroup.calls.length)){endAndAnswer();return;}
if(telephony.active||handledCalls.length>=2||telephony.calls.length===1&&telephony.conferenceGroup.calls.length){var lastCall=handledCalls[handledCalls.length-1].call;handleCall(lastCall,'answer','voice');handleRttWaitingCallStateChange(lastCall);CallScreen.hideIncoming();}else{handledCalls[0].call.hold().then(()=>{CallScreen.hideIncoming();});}
if(cdmaCallWaiting()){btHelper.answerWaitingCall();CallScreenHelper.setState('connected_hold');CallScreenHelper.render();handledCalls[0].updateCallNumber();stopWaitingTone();}}
function endAndAnswer(){if((handledCalls.length<2)&&!cdmaCallWaiting()){return;}
if(telephony.active==telephony.conferenceGroup){endConferenceCall().then(function(){CallScreen.hideIncoming();},function(){});return;}
if(cdmaCallWaiting()){handledCalls[0].call.hold();stopWaitingTone();btHelper.answerWaitingCall();}else{var callToEnd=telephony.active||handledCalls[handledCalls.length-2].call;var callToAnswer;handledCalls.some(function(handledCall){if(handledCall.call.state=='incoming'){callToAnswer=handledCall.call;return true;}});if(callToEnd&&callToAnswer){callToEnd.addEventListener('disconnected',function ondisconnected(){callToEnd.removeEventListener('disconnected',ondisconnected);handleCall(callToAnswer,'answer');});handleCall(callToEnd,'hangUp');}else if(callToEnd){handleCall(callToEnd,'hangUp');}else if(callToAnswer){handleCall(callToAnswer,'answer');}}
CallScreen.hideIncoming();if(cdmaCallWaiting()){handledCalls[0].updateCallNumber();}}
function toggleCalls(){if(incomingCall()&&!cdmaCallWaiting()){return;}
if(openLines()<2&&!cdmaCallWaiting()){if(!telephony.active){holdOrResumeSingleCall();}
return;}
telephony.active.hold();btHelper.toggleCalls();callHeldByUser=null;}
function holdOrResumeCallByUser(){if(telephony.active){callHeldByUser=telephony.active;}
holdOrResumeSingleCall();}
function holdOrResumeSingleCall(){console.log('holdOrResumeSingleCall '+openLines()+' '
+telephony.calls.length);if(openLines()!==1||(telephony.calls.length&&(telephony.calls[0].state==='incoming'||!telephony.calls[0].switchable))){return;}
if(telephony.active){telephony.active.hold();}else{var line=telephony.calls.length?telephony.calls[0]:telephony.conferenceGroup;console.log('holdOrResumeSingleCall resume');line.resume();callHeldByUser=null;}}
function hangupWaitingCalls(){handledCalls.forEach(function(handledCall){var callState=handledCall.call.state;if(callState==='held'||(callState==='incoming'&&handledCalls.length>1)){handleCall(handledCall.call,'hangUp');}});}
function ignore(){if(cdmaCallWaiting()){CallScreenHelper.setState();stopWaitingTone();btHelper.ignoreWaitingCall();}else{var ignoreIndex=handledCalls.length-1;var call=handledCalls[ignoreIndex].call;window.performance.mark('endIncoming-start');call.ondisconnected=()=>{window.performance.mark('endIncoming-end');window.performance.measure('performance-endIncoming','endIncoming-start','endIncoming-end');window.performance.clearMarks('endIncoming-start');window.performance.clearMarks('endIncoming-end');window.performance.clearMeasures('performance-endIncoming');};handleCall(call,'hangUp');}
CallScreen.incomingInfo.classList.add('ended');CallScreen.incomingNumberAdditionalTelType.textContent='';CallScreen.incomingNumberAdditionalTel.textContent=_('callEnded');CallScreen.hideIncoming();}
function endConferenceCall(){return telephony.conferenceGroup.hangUp().then(function(){ConferenceGroupHandler.signalConferenceEnded();},function(){console.error('Failed to hangup Conference Call');});}
function end(){if(incomingCall()){ignore();return;}
var callToEnd=null;if(telephony.active){callToEnd=telephony.active;}else{if(telephony.conferenceGroup.calls.length||telephony.conferenceGroup.state){callToEnd=telephony.conferenceGroup;}else{callToEnd=telephony.calls[0];}}
if(callToEnd.calls){endConferenceCall();return;}else{handleCall(callToEnd,'hangUp');}
callToEnd.addEventListener('statechange',function callStateChange(){var state=callToEnd.state;if((state==='disconnected'||state==='')&&!telephony.active){holdOrResumeSingleCall();}
callToEnd.removeEventListener('statechange',callStateChange);});}
function switchToSpeaker(){btHelper.disconnectSco();if(!telephony.speakerEnabled){telephony.speakerEnabled=true;}}
function enableSpeakerForVT(){CallsHandler.enableSpeaker=true;if(!isBTReceiverUsed&&!navigator.mozAudioChannelManager.headphones){switchToSpeaker();}}
function switchToDefaultOut(doNotConnect){if(telephony.speakerEnabled){telephony.speakerEnabled=false;}
if(!doNotConnect){btHelper.connectSco();}}
function switchToReceiver(){btHelper.disconnectSco();if(telephony.speakerEnabled){telephony.speakerEnabled=false;}}
function toggleMute(){telephony.muted=!telephony.muted;}
function toggleSpeaker(){if(telephony.speakerEnabled){CallsHandler.switchToDefaultOut();}else{CallsHandler.switchToSpeaker();}}
function playWaitingTone(call){var sequence=[[440,440,100],[0,0,100],[440,440,100]];toneInterval=window.setInterval(function playTone(){TonePlayer.playSequence(sequence);},3000);TonePlayer.playSequence(sequence);call.addEventListener('statechange',function callStateChange(){if(call.state==='incoming'){return;}
call.removeEventListener('statechange',callStateChange);window.clearInterval(toneInterval);});}
function stopWaitingTone(){window.clearInterval(toneInterval);}
function activeCall(){var telephonyActiveCall=telephony.active;var active=null;for(var i=0;i<handledCalls.length;i++){var handledCall=handledCalls[i];if(telephonyActiveCall===handledCall.call){active=handledCall;break;}}
return active;}
function incomingCall(){var incoming=null;if(cdmaCallWaiting()){incoming=telephony.calls[0];}else{for(var i=0;i<handledCalls.length;i++){var handledCall=handledCalls[i];if('incoming'===handledCall.call.state){incoming=handledCall;break;}}}
return incoming;}
function canShowDtmfScreen(){if(CallScreenHelper.cdmaState==='connected_waiting'||CallScreenHelper.getCall('incoming')||CallsHandler.isVideoCall()||Rtt.isRttViewShown()){return false;}else{return telephony.active?true:false;}}
function activeCallForContactImage(){if(handledCalls.length===1){return handledCalls[0];}
return[activeCall()].concat(handledCalls).find(function(elem){return!elem||!elem.call.group;});}
function cdmaCallWaiting(){return((telephony.calls.length==1)&&(telephony.calls[0].state=='connected')&&(telephony.calls[0].secondId));}
function isFirstCallOnCdmaNetwork(){var cdmaTypes=['evdo0','evdoa','evdob','1xrtt','is95a','is95b'];if(handledCalls.length!==0){var ci=handledCalls[0].call.serviceId;var type=window.navigator.mozMobileConnections[ci].voice.type;return(cdmaTypes.indexOf(type)!==-1);}else{return false;}}
function isCdma3WayCall(){return isFirstCallOnCdmaNetwork()&&((telephony.calls.length===CDMA_CALLS_LIMIT)||(telephony.conferenceGroup.calls.length>0));}
function mergeCalls(){if(operateState.merge){return;}else{operateState.merge=true;}
if(ConferenceGroupUI.isImsconferenceGroup()){getConferenceStartTime();}
if(!telephony.conferenceGroup.calls.length&&telephony.calls.length===2){telephony.conferenceGroup.add(telephony.calls[0],telephony.calls[1]);}else if(telephony.conferenceGroup.state&&telephony.calls.length===1){telephony.conferenceGroup.add(telephony.calls[0]);}
callHeldByUser=null;}
function isEstablishingCall(){return telephony.calls.some(function(call){return call.state=='dialing'||call.state=='alerting';});}
function isAnyCallOnHold(){return telephony.calls.some(call=>call.state==='held')||(telephony.conferenceGroup&&telephony.conferenceGroup.state==='held');}
function isAnyCallSwitchable(){return telephony.calls.some(call=>call.switchable)||((telephony.conferenceGroup.calls.length>0)&&telephony.conferenceGroup.calls.every(call=>call.switchable));}
function isEveryCallMergeable(){return telephony.calls.every(call=>call.mergeable);}
function updatePlaceNewCall(){if(isEstablishingCall()){CallScreen.disablePlaceNewCallButton();}else{CallScreen.enablePlaceNewCallButton();}}
function getGroupDetailsText(){console.log('FIH CallsHandler Run getGroupDetailsText');var text='';handledCalls.forEach(function(handleCall){if(handleCall.call.group){text+=handleCall.info;text+=', ';}});text=text.substring(0,text.length-2);console.log('FIH CallsHandler getGroupDetailsText (text.split(, )).length: '+(text.split(', ')).length);if((text.split(', ')).length===telephony.conferenceGroup.calls.length){return text;}else{return getGroupDetailsTextForSrvcc();}}
function getGroupDetailsTextForSrvcc(){console.log('FIH CallsHandler Run getGroupDetailsTextForSrvcc');var calls=telephony.conferenceGroup.calls;var text='';calls.forEach(function(call){var name=matchNumber(call.id.number);console.log('FIH CallsHandler getGroupDetailsTextForSrvcc name  ='+name);if(name===null){return;}
text+=(name?name:call.id.number)+', ';});text=text.substring(0,text.length-2);return text;}
function getImsGroupDetailsText(){console.log('FIH CallsHandler Run getImsGroupDetailsText');var text='';var calls=telephony.conferenceGroup.calls;var imsCallsItem=ConferenceGroupHandler.imsCallsItem;imsCallsItem.forEach((item,imsCall)=>{var exist=calls.some((call)=>{return(call===imsCall);});if(!exist){ConferenceGroupHandler.removeFromGroupDetails(item);imsCallsItem.delete(imsCall);}});calls.forEach(function(call){console.log('FIH CallsHandler getImsGroupDetailsText calls.forEach imsCallsItem.has(call)  ='+imsCallsItem.has(call));if(imsCallsItem.has(call)){text+=imsCallsItem.get(call).textContent+', ';return;}
var name=matchNumber(call.id.number);console.log('FIH CallsHandler getImsGroupDetailsText calls.forEach name  ='+name);if(name===null){return;}
var item;text+=(name?name:call.id.number)+', ';item=ConferenceGroupHandler.addToImsGroupDetails(call.id.number,name);console.log('FIH CallsHandler getImsGroupDetailsText calls.forEach imsCallsItem.set(call, item)  call ='+call+',item ='+item);imsCallsItem.set(call,item);call.ondisconnected=()=>{if(!item.dataset.groupHangup){CallScreen.showToast('call-left-conference-toast',item.textContent);}
ConferenceGroupHandler.removeFromGroupDetails(item);imsCallsItem.delete(call);};});text=text.substring(0,text.length-2);console.log('FIH CallsHandler getImsGroupDetailsText return text  ='+text);return text;}
function matchNumber(number){console.log('FIH CallsHandler Run matchNumber number ='+number);var findMatchNumber=false;var name='';var fuzzyName;console.log('FIH CallsHandler  matchNumber imsGroupRecords.length ='+imsGroupRecords.length);for(var i=0;i<imsGroupRecords.length;i++){var record=imsGroupRecords[i];console.log('FIH CallsHandler  matchNumber record.number ='+record.number);if(record.number===number){findMatchNumber=true;if(record.name!==''){console.log('FIH CallsHandler  matchNumber name = record.name ='+record.name);name=record.name;break;}}else if(record.number.indexOf(number)!==-1||number.indexOf(record.number)!==-1){console.log('FIH CallsHandler matchNumber fuzzyName = record.name ='+record.name);fuzzyName=record.name;}else{var recordNumberLength=record.number.length;var callNumberLength=number.length;if(recordNumberLength>=7&&callNumberLength>=7&&record.number.substr(recordNumberLength-7)===number.substr(callNumberLength-7)){console.log('FIH CallsHandler matchNumber for case such as 0289xxxxxx -> 886289xxxxxx  fuzzyName = record.name ='+record.name);fuzzyName=record.name;}}}
if(!findMatchNumber&&fuzzyName===undefined&&(number==='unavailable'||number.indexOf('anonymous')!==-1)){console.log('FIH CallsHandler matchNumber Only for Qualcomm  fuzzyName = unknown');fuzzyName=_('unknown');}
console.log('FIH CallsHandler matchNumber findMatchNumber ='+findMatchNumber+", fuzzyName ="+fuzzyName);if(!findMatchNumber&&fuzzyName===undefined){return null;}
if(!findMatchNumber&&fuzzyName!==undefined){name=fuzzyName;}
return name;}
function getConferenceStartTime(){var groupDuration=document.querySelector('#group-call > .duration');var conferenceStartTime=0;if(groupDuration.dataset.startTime===undefined){handledCalls.forEach(function(handleCall){var startTime=handleCall.getStartTime();if(conferenceStartTime===0||conferenceStartTime>startTime){conferenceStartTime=startTime;}});}else{conferenceStartTime=groupDuration.dataset.startTime;}
return conferenceStartTime;}
function clearConferenceStartTime(){var groupDuration=document.querySelector('#group-call > .duration');delete groupDuration.dataset.startTime;}
function setCNAPText(call,numberItem,nameItem){var name=call.id.name;if(name===''){return false;}
var number=call.id.number;var numberPresentation=call.id.numberPresentation;nameItem.textContent=name;switch(numberPresentation){case('allowed'):numberItem.textContent=number;break;case('restricted'):case('unknown'):numberItem.textContent=_('unknown');break;case('payphone'):numberItem.textContent=_('payphone');break;}
return true;}
function resetToDefault(){offSpeaker();CallScreen.hideDtmfNumber();btHelper.getConnectedDevicesByProfile(btHelper.profiles.HFP,function(result){btConnectedState=!!(result&&result.length);CallScreen.setBTReceiverIcon(btConnectedState);});}
function isInConferenceCall(number){var calls=telephony.conferenceGroup.calls;if(!calls.length){return false;}else{for(var i=0;i<calls.length;i++){if(calls[i].id.number===number){return true;}}
return false;}}
function changeCamera(){var videoCallProvider=telephony.calls[0].videoCallProvider;VT._hideStreams('local');videoCallProvider.setCamera();var type=VT.isCameraFront()?'rear':'front';VT._prepareLocalStreams(type).then((surface)=>{VT.localView.mozSrcObject=surface;VT.localAnchor.hidden=true;VT._showStreams('local');});}
function toggleCamera(){var videoCallProvider=telephony.calls[0].videoCallProvider;if(VT.isCameraOn()){VT.setCameraOn(false);videoCallProvider.setCamera();VT._hideStreams('local');}else{var type=VT.isCameraFront()?'front':'rear';VT._prepareLocalStreams(type).then((surface)=>{VT.localView.mozSrcObject=surface;VT.localAnchor.hidden=true;VT._showStreams('local');});}}
function changeCallType(){if(CallsHandler.isVideoCall()){changeVideoCallState('bidirectional','audio-only');VT.requestVoice();}else{CallRecording.stopRec();var handledCallNode=document.getElementById('list-call');handledCallNode.querySelector('.local-video-view').classList.add('full');VT._showLocalAnchor(true);VT._prepareRemoteStreams().then((surface)=>{VT.remoteView.mozSrcObject=surface;VT.remoteAnchor.hidden=true;});changeVideoCallState('audio-only','bidirectional');CallScreenHelper.render();}}
function changeVideoCallState(origin,dest){var originState={};var desState={};var videoCallProvider=telephony.calls[0].videoCallProvider;originState.state=origin;originState.quality='default';desState.state=dest;desState.quality='default';videoCallProvider.sendSessionModifyRequest(originState,desState);}
function isVideoCall(){if(telephony.conferenceGroup.calls.length){return false;}
var videoCallState=telephony.calls[0].videoCallState;if(videoCallState&&(videoCallState==='TxEnabled'||videoCallState==='RxEnabled'||videoCallState==='Bidirectional'||videoCallState==='Paused')){console.log("calls_handler isVideoCall() = true");return true;}else{return false;}}
function isSupportedVT(){if(telephony.conferenceGroup.calls.length){return false;}
var capabilities=telephony.calls[0].capabilities;if(capabilities.vtLocalBidirectional&&capabilities.vtRemoteBidirectional){return true;}else{return false;}}
function toggleShowInfo(){var handledCallNode=document.getElementById('list-call');if(showInfo){handledCallNode.querySelector('#callsinfo').classList.add('hide');handledCallNode.querySelector('.status-icon').classList.add('hide');handledCallNode.querySelector('.duration').classList.add('hide');handledCallNode.querySelector('.local-video-view').classList.add('large');showInfo=false;}else{handledCallNode.querySelector('#callsinfo').classList.remove('hide');handledCallNode.querySelector('.status-icon').classList.remove('hide');handledCallNode.querySelector('.duration').classList.remove('hide');handledCallNode.querySelector('.local-video-view').classList.remove('large');showInfo=true;}}
function handleCall(call,event,option){if(operateState[event]!==false){return;}
operateState[event]=true;if(event==='answer'){answerCall(call,option);}else{call[event]();}
call.addEventListener('statechange',handleStateChangeOrError);call.addEventListener('error',handleStateChangeOrError);}
function answerCall(call,option){if(option==='voice'){call.answer(CALL_TYPE_VOICE_N_VIDEO,false);}else if(call.videoCallState==='Bidirectional'){call.answer(CALL_TYPE_VT,false);}else if(call.rttMode==='full'){call.answer(CALL_TYPE_VOICE_N_VIDEO,true);}else{call.answer(CALL_TYPE_VOICE_N_VIDEO,false);}}
function handleStateChangeOrError(evt){operateState.answer=false;operateState.hangUp=false;evt.call.removeEventListener('statechange',handleStateChangeOrError);evt.call.removeEventListener('error',handleStateChangeOrError);}
function getConnectedCall(){var connectedcall=telephony.calls.find((call)=>{if(call.state==='connected'){return call;}});return connectedcall;}
function isConnectedCallInRttMode(){var rttMode=false;var call=getConnectedCall();if(call&&call.rttMode==='full'){rttMode=true;}
return rttMode;}
function anyCallInRttMode(){return telephony.calls.some((call)=>{return call.rttMode==='full';});}
function isIncomingSupportedRtt(){var length=telephony.calls.length;if(length>0&&telephony.calls[length-1].state==='incoming'&&telephony.calls[length-1].capabilities&&telephony.calls[length-1].capabilities.supportRtt){return true;}else{return false;}}
function activeCallSupportRtt(){if(telephony.active&&telephony.active.capabilities&&telephony.active.capabilities.supportRtt){return true;}else{return false;}}
function enableScreenDimFeature(enable){handleBrightnessTimer(enable);}
function handleRttWaitingCallStateChange(call){if(!call||call.rttMode!=='full'){return;}
call.addEventListener('statechange',function showToast(){if(call.state==='incoming'&&call.rttMode==='full'){return;}
if(call.state==='connected'&&call.rttMode==='off'){CallScreen.showToast('voice-only-call');}
call.removeEventListener('statechange',showToast);});}
return{setup:setup,answer:answer,holdAndAnswer:holdAndAnswer,endAndAnswer:endAndAnswer,toggleCalls:toggleCalls,ignore:ignore,end:end,toggleMute:toggleMute,toggleSpeaker:toggleSpeaker,switchToReceiver:switchToReceiver,switchToSpeaker:switchToSpeaker,switchToDefaultOut:switchToDefaultOut,holdOrResumeCallByUser:holdOrResumeCallByUser,checkCalls:onCallsChanged,mergeCalls:mergeCalls,SetMicMode:SetMicMode,isFlipOpend:isFlipOpend,initMicMode:initMicMode,exitCallScreenIfNoCalls:exitCallScreenIfNoCalls,getGroupDetailsText:getGroupDetailsText,conferenceCallEnd:conferenceCallEnd,getConferenceStartTime:getConferenceStartTime,clearConferenceStartTime:clearConferenceStartTime,setCNAPText:setCNAPText,getImsGroupDetailsText:getImsGroupDetailsText,insertImsRecord:insertImsRecord,getImsRecord:getImsRecord,isInConferenceCall:isInConferenceCall,changeCamera:changeCamera,toggleCamera:toggleCamera,changeVideoCallState:changeVideoCallState,isVideoCall:isVideoCall,isSupportedVT:isSupportedVT,changeCallType:changeCallType,toggleShowInfo:toggleShowInfo,handleCallInfoTimer:handleCallInfoTimer,enableSpeakerForVT:enableSpeakerForVT,getConnectedCall:getConnectedCall,enableScreenDimFeature:enableScreenDimFeature,anyCallInRttMode:anyCallInRttMode,isIncomingSupportedRtt:isIncomingSupportedRtt,activeCallSupportRtt:activeCallSupportRtt,get activeCall(){return activeCall();},get incomingCall(){return incomingCall();},get canShowDtmfScreen(){return canShowDtmfScreen();},get activeCallForContactImage(){return activeCallForContactImage();},get isAnyOpenCalls(){return handledCalls.length>0||telephony.conferenceGroup.state!=='';},get isAnyConnectedCall(){return!!(telephony.active&&telephony.active.state==='connected');},get serviceId(){var activeCallServiceId=0;if(telephony.calls.length){activeCallServiceId=telephony.calls[0].serviceId;}else if(telephony.conferenceGroup.calls.length){activeCallServiceId=telephony.conferenceGroup.calls[0].serviceId;}
return activeCallServiceId;},get imsRegState(){var imsHandler=navigator.mozMobileConnections[CallsHandler.serviceId].imsHandler;if(imsHandler)
return imsHandler.capability;else
return null;},get isBtConnected(){return btConnectedState;},get isMute(){return telephony.muted;},get isSpeakerEnabled(){return telephony.speakerEnabled;},get isBTReceiverUsed(){return isBTReceiverUsed;},get isConferenceConnected(){return telephony.conferenceGroup.state==='connected';},get isConnectedCallInRttMode(){return isConnectedCallInRttMode();},set isMergeRequsted(value){operateState.merge=value;},get isShowInfo(){return showInfo;},set isShowInfo(status){showInfo=status;},get handledCalls(){return handledCalls;},isFirstCallOnCdmaNetwork:isFirstCallOnCdmaNetwork};})();