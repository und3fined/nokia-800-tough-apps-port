'use strict';var ConferenceGroupUI=(function(){var groupCalls=document.getElementById('group-call-details'),groupCallsList=document.getElementById('group-call-details-list'),bdiGroupCallsCountElt=document.createElement('bdi'),initialized=false;function _init(callback){if(initialized){callback();}
groupCallsList=document.getElementById('group-call-details-list');if(!initialized){bdiGroupCallsCountElt=document.createElement('bdi');}
initialized=true;callback();}
function hideGroupDetails(evt){if(evt){evt.preventDefault();}
if(!navigator.mozTelephony.conferenceGroup.calls.length){_removeAllCalls();}
groupCalls.classList.remove('display');}
function _removeAllCalls(){var callNodes=groupCalls.querySelectorAll('.handled-call');for(var i=0;i<callNodes.length;i++){removeCall(callNodes[i]);}}
function createItem(info){var listItem=document.createElement('li');listItem.classList.add('conference_group_list');listItem.classList.add('p-pri');var infoItem=document.createElement('div');infoItem.textContent=info;listItem.appendChild(infoItem);return listItem;}
function addCall(info){var item=createItem(info);groupCallsList.appendChild(item);return item;}
function addImsCall(number,name){var item=createImsItem(number,name);groupCallsList.appendChild(item);return item;}
function createImsItem(number,name){var listItem=document.createElement('li');listItem.classList.add('conference_group_list');listItem.classList.add('p-pri');listItem.dataset.telenumber=number;var infoItem=document.createElement('div');if(name===''){infoItem.textContent=number;}else{infoItem.textContent=name;}
listItem.appendChild(infoItem);return listItem;}
function removeCall(node){groupCallsList.removeChild(node);}
function showGroupDetails(){NavigationManager.reset('.conference_group_list');groupCalls.classList.add('display');}
function removeDetails(){groupCallsList.innerHTML='';}
function getCallFromCallmap(listItem){var imsCallsItem=ConferenceGroupHandler.imsCallsItem;var call=null;imsCallsItem.forEach((item,imsCall)=>{if(item===listItem){call=imsCall;}});return call;}
function endCall(){hideGroupDetails();var listItem=groupCalls.querySelector('.focus');if(listItem.endCall){listItem.endCall();}else{var call=getCallFromCallmap(listItem);if(call){call.hangUp();}}}
function isImsconferenceGroup(){if(CallsHandler.imsRegState!=='voice-over-cellular'&&CallsHandler.imsRegState!=='voice-over-wifi'){return false;}
var firstListItem=groupCallsList.childNodes[0];if(groupCallsList.childElementCount==0||firstListItem.endCall==undefined){return true;}else{return false;}}
function separateCall(){hideGroupDetails();var listItem=groupCalls.querySelector('.focus');if(listItem.separateCall){listItem.separateCall();}else{var call=getCallFromCallmap(listItem);if(call){window.navigator.mozTelephony.conferenceGroup.remove(call);removeCall(listItem);ConferenceGroupHandler.imsCallsItem.delete(call);}}}
function isGroupDetailsShown(){return groupCalls.classList.contains('display');}
function markCallsAsEnded(){_init(function(){var callElems=groupCallsList.getElementsByTagName('li');for(var i=0;i<callElems.length;i++){callElems[i].dataset.groupHangup='groupHangup';}});}
function setGroupDetailsHeader(text){bdiGroupCallsCountElt.textContent=text;}
return{addCall:addCall,addImsCall:addImsCall,removeCall:removeCall,showGroupDetails:showGroupDetails,hideGroupDetails:hideGroupDetails,isGroupDetailsShown:isGroupDetailsShown,markCallsAsEnded:markCallsAsEnded,endCall:endCall,separateCall:separateCall,removeDetails:removeDetails,isImsconferenceGroup:isImsconferenceGroup,setGroupDetailsHeader:setGroupDetailsHeader};})();