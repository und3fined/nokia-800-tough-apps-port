'use strict'

const Redirect = (function Redirect () {
  const init = function init () {
    const url = window.location.href
    const result = {
      oauth2LoginCompleted: {}
    }
    /*
      An error response:
      http://localhost/auth?error=access_denied
      An authorization code response:
      http://localhost/auth?code=4/P7q7W91a-oMsCeLvIaQm6bTrgtp7
    */
    const search = url.split('?')[1] || ''

    if (search.length > 0) {
      const elements = search.split('&')

      elements.forEach(p => {
        const values = p.split('=')
        result.oauth2LoginCompleted[decodeURIComponent(values[0])] =
          decodeURIComponent(values[1])
      })
      var COMMS_APP_ORIGIN = document.location.protocol + '//' +
        document.location.host
      window.opener.postMessage(result, COMMS_APP_ORIGIN)
    }

    window.close()
  }

  return {
    'init': init
  }
}())

Redirect.init()
